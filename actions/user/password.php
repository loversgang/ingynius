<?php

/**
 * Action to reset a password, send success email, and log the user in.
 *
 * @package Elgg
 * @subpackage Core
 */
$password = get_input('password1');
$password_repeat = get_input('password2');
$user = get_entity(elgg_get_logged_in_user_guid());

try {
    validate_password($password);
} catch (RegistrationException $e) {
    register_error($e->getMessage());
    forward(REFERER);
}

if ($password != $password_repeat) {
    register_error(elgg_echo('RegistrationException:PasswordMismatch'));
    forward(REFERER);
}
if (($user instanceof ElggUser) && ($user->canEdit())) {
    if (force_user_password_reset($user->guid, $password)) {
        system_message(elgg_echo('user:password:success'));
        forward(elgg_get_site_url() . 'password/');
    } else {
        register_error(elgg_echo('user:password:fail'));
        forward(elgg_get_site_url() . 'password');
    }
} else {
    register_error(elgg_echo('admin:user:resetpassword:no'));
    forward(elgg_get_site_url() . 'password');
}