<?php

/**
 * Page for changing password
 *
 * @package Elgg.Core
 * @subpackage Registration
 */
$title = elgg_echo('Change Password');
if (elgg_is_logged_in()) {
    $logged_user = elgg_get_logged_in_user_entity();
    $user_guid = $logged_user->guid;
    $content = elgg_view_form('user/password', array('class' => 'form from-horizontal'));
    $body = elgg_view_layout('one_column', array(
        'content' => $content
    ));
    echo elgg_view_page($title, $body);
} else {
    forward();
}

