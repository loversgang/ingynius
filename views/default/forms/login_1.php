<?php
/**
 * Elgg login form
 *
 * @package Elgg
 * @subpackage Core
 */
?>
<div class="form-group">
    <label><?php echo elgg_echo('loginusername'); ?></label>
    <?php
    echo elgg_view('input/text', array(
        'name' => 'username',
        'autofocus' => true,
        'class' => 'form-control'
    ));
    ?>
</div>
<div class="form-group">
    <label><?php echo elgg_echo('password'); ?></label>
    <?php
    echo elgg_view('input/password', array(
        'name' => 'password',
        'class' => 'form-control'
    ));
    ?>
</div>
<?php echo elgg_view('login/extend', $vars); ?>
<div class="checkbox">
    <label>
        <input type="checkbox" name="persistent" value="true" />
        <?php echo elgg_echo('user:persistent'); ?>
    </label>
</div>
<?php
echo elgg_view('input/submit', array(
    'value' => elgg_echo('login'),
    'class' => 'btn btn-success col-md-12'
));
?>

<?php
if (isset($vars['returntoreferer'])) {
    echo elgg_view('input/hidden', array('name' => 'returntoreferer', 'value' => 'true'));
}
?>
<?php
echo elgg_view_menu('login', array(
    'sort_by' => 'priority',
));
?>

