<?php
/**
 * Elgg login form
 *
 * @package Elgg
 * @subpackage Core
 */
?>
<div class="form-group">
    <label><?php echo elgg_echo('loginusername'); ?></label>
    <?php
    echo elgg_view('input/text', array(
        'name' => 'username',
        'autofocus' => true,
        'class' => 'form-control'
    ));
    ?>
</div>
<div class="form-group">
    <label><?php echo elgg_echo('password'); ?></label>
    <?php
    echo elgg_view('input/password', array(
        'name' => 'password',
        'class' => 'form-control'
    ));
    ?>
</div>
<div class="col-md-12" style="margin-bottom: 10px">
<?php
echo elgg_view('input/submit', array(
    'value' => elgg_echo('login'),
    'class' => 'btn btn-primary col-md-12'
));
?>
<?php
if (isset($vars['returntoreferer'])) {
    echo elgg_view('input/hidden', array('name' => 'returntoreferer', 'value' => 'true'));
}
?>
<?php
echo elgg_view_menu('login', array(
    'sort_by' => 'priority',
));
?>
</div>
