<div class="col-md-12" style="margin-top: 10px">
    <div class="panel panel-info panel-login" style="border-radius: 5px;">
        <div class="panel-heading">
            <h3 class="panel-title">Change password</h3>
        </div>
        <div class="panel-body" style="border: none !important;margin: 0px 20px;">
            <div class="form-group">
                <label class="control-label">New password</label>
                <input type="password" name="password1" class="form-control"/>
            </div>
            <div class="form-group">
                <label class="control-label">New password again</label>
                <input type="password" name="password2" class="form-control"/>
            </div>
            <input type="submit" value="Change password" class="btn btn-success"/>
        </div>
    </div>
</div>