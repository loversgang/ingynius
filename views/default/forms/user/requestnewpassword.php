<?php
/**
 * Elgg forgotten password.
 *
 * @package Elgg
 * @subpackage Core
 */
?>
<div class="col-md-12" style="margin-top: 10px">
    <div class="panel panel-info panel-login" style="border-radius: 5px;">
        <div class="panel-heading">
            <h3 class="panel-title">Forgot Password</h3>
        </div>
        <div class="panel-body" style="border: none !important;margin: 0px 10px;">
            <div class="alert alert-success">
                <?php echo elgg_echo('user:password:text'); ?>
            </div>
            <div class="form-group">
                <label><?php echo elgg_echo('loginusername'); ?></label><br />
                <?php
                echo elgg_view('input/text', array(
                    'name' => 'username',
                    'autofocus' => true,
                    'class' => 'form-control'
                ));
                ?>
            </div>
            <?php echo elgg_view('input/captcha'); ?>
            <div class="col-md-12" style="margin-bottom: 10px;">
                <?php
                echo elgg_view('input/submit', array(
                    'value' => elgg_echo('request'),
                    'class' => 'btn btn-primary col-md-12'
                ));
                ?>
            </div>
            <div class="col-md-12" style="text-align:center">
                <div class="col-md-6 links_tabs">
                    <a href="<?php echo elgg_get_site_url() ?>login/">Login</a>
                </div>
                <div class="col-md-6 links_tabs">
                    <a href="<?php echo elgg_get_site_url() ?>register/">Sign Up</a>
                </div>
            </div>
        </div>
    </div>
</div>
