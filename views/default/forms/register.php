<?php
/**
 * Elgg register form
 *
 * @package Elgg
 * @subpackage Core
 */
if (elgg_is_sticky_form('register')) {
    $values = elgg_get_sticky_values('register');
    elgg_clear_sticky_form('register');
} else {
    $values = array();
}

$password = $password2 = '';
$username = elgg_extract('username', $values, get_input('u'));
$email = elgg_extract('email', $values, get_input('e'));
$name = elgg_extract('name', $values, get_input('n'));
?>
<div class="col-md-12" style="margin-top: 10px">
    <div class="panel panel-info panel-login" style="border-radius: 5px;">
        <div class="panel-heading">
            <h3 class="panel-title">Sign Up</h3>
        </div>
        <div class="panel-body register_form" style="border: none !important;margin: 0px 10px;">
            <div class="form-group">
                <label>Full Name</label><br />
                <?php
                echo elgg_view('input/text', array(
                    'name' => 'name',
                    'value' => $name,
                    'autofocus' => true,
                    'class' => 'form-control'
                ));
                ?>
            </div>
            <div class="form-group">
                <label><?php echo elgg_echo('email'); ?></label><br />
                <?php
                echo elgg_view('input/text', array(
                    'name' => 'email',
                    'value' => $email,
                    'class' => 'form-control'
                ));
                ?>
            </div>
            <div class="form-group">
                <label><?php echo elgg_echo('username'); ?></label><br />
                <?php
                echo elgg_view('input/text', array(
                    'name' => 'username',
                    'value' => $username,
                    'class' => 'form-control'
                ));
                ?>
            </div>
            <div class="form-group">
                <label><?php echo elgg_echo('password'); ?></label><br />
                <?php
                echo elgg_view('input/password', array(
                    'name' => 'password',
                    'value' => $password,
                    'class' => 'form-control'
                ));
                ?>
            </div>
            <div class="form-group">
                <label><?php echo elgg_echo('passwordagain'); ?></label><br />
                <?php
                echo elgg_view('input/password', array(
                    'name' => 'password2',
                    'value' => $password2,
                    'class' => 'form-control'
                ));
                ?>
            </div>
            <?php
            // Add captcha hook
            echo elgg_view('input/captcha', $vars);

            echo '<div class="col-md-12" style="margin-top: -10px;margin-bottom: 10px;">';
            echo elgg_view('input/hidden', array('name' => 'friend_guid', 'value' => $vars['friend_guid']));
            echo elgg_view('input/hidden', array('name' => 'invitecode', 'value' => $vars['invitecode']));
            echo "<br/>";
            echo elgg_view('input/submit', array(
                'name' => 'submit',
                'value' => elgg_echo('register'),
                'class' => 'btn btn-primary col-md-12'
            ));
            echo '</div>';
            ?>
            <div class="col-md-12" style="text-align:center">
                <div class="col-md-6 links_tabs">
                    <a href="<?php echo elgg_get_site_url() ?>login/">Login</a>
                </div>
                <div class="col-md-6 links_tabs">
                    <a href="<?php echo elgg_get_site_url() ?>forgotpassword/">Forgot Password</a>
                </div>
            </div>
        </div>
    </div>
</div>
