<?php
/**
 * Elgg login box
 *
 * @package Elgg
 * @subpackage Core
 *
 * @uses $vars['module'] The module name. Default: aside
 */
$module = elgg_extract('module', $vars, 'aside');

$login_url = elgg_get_site_url();
if (elgg_get_config('https_login')) {
    $login_url = str_replace("http:", "https:", $login_url);
}

//$title = elgg_extract('title', $vars, elgg_echo('login'));

$body = elgg_view_form('login', array(
    'action' => "{$login_url}action/login",
    'class' => 'form form-horizontal',
    'style' => 'text-align: left'
        ));
?>
<div class="col-md-12" style="margin-top: 10px">
    <div class="panel panel-info panel-login" style="border-radius: 5px;">
        <div class="panel-heading">
            <h3 class="panel-title">Login</h3>
        </div>
        <div class="panel-body" style="border: none !important;margin: 0px 20px;">
            <?php
            echo elgg_view_module($module, $title, $body);
            ?>
            <div class="col-md-12">
                <div class="col-md-6 links_tabs">
                    <a href="<?php echo elgg_get_site_url() ?>register/">Register</a>
                </div>
                <div class="col-md-6 links_tabs">
                    <a href="<?php echo elgg_get_site_url() ?>forgotpassword/">Forgot Password</a>
                </div>
            </div>
        </div>
    </div>
</div>

