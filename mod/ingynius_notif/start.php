<?php

elgg_register_event_handler('init', 'system', 'ingynius_notif_init');

function ingynius_notif_init() {
    elgg_register_plugin_hook_handler('entity:url', 'object', 'pages_set_url');
    elgg_register_page_handler('ingynius_notif', 'ingynius_notif_page_handler');
}

function ingynius_notif_page_handler($segments) {
    if (!$segments[0]) {
        include elgg_get_plugins_path() . 'ingynius_notif/pages/ingynius_notif/list.php';
        return true;
    }
    if ($segments[0] == 'list') {
        include elgg_get_plugins_path() . 'ingynius_notif/pages/ingynius_notif/list.php';
        return true;
    }
    if ($segments[0] == 'create') {
        include elgg_get_plugins_path() . 'ingynius_notif/pages/ingynius_notif/create.php';
        return true;
    }
    if ($segments[0] == 'view') {
        include elgg_get_plugins_path() . 'ingynius_notif/pages/ingynius_notif/view.php';
        return true;
    }
    return false;
}
