<?php
include_once elgg_get_plugins_path() . 'time_theme/functions.php';
$logged_user = elgg_get_logged_in_user_entity();
$guid = $_GET['page_id'];
$entity = get_entity($guid);
if ($entity->title == 'page_content') {
    $page = maybe_unserialize($entity->description);
    $likes_value = 'post_likes_' . $entity->guid;
    $comments_value = 'post_comments_' . $entity->guid;
    $ia = elgg_set_ignore_access(true);
    $post_likes = ($entity->$likes_value) ? maybe_unserialize($entity->$likes_value) : array();
    $post_comments = ($entity->$comments_value) ? maybe_unserialize($entity->$comments_value) : array();
    elgg_set_ignore_access($ia);
    $entities = elgg_get_entities(array(
        'type' => 'object',
        'subtypes' => array('text_content_' . $guid, 'photo_content_' . $guid, 'audio_content_' . $guid, 'link_content_' . $guid, 'video_content_' . $guid, 'event_content_' . $guid),
        'limit' => FALSE,
    ));
    ?>
    <div class="col-md-12" style="margin-top: 1px;padding: 0px">
        <div class="fb-profile">
            <img align="left" class="fb-image-lg" src="<?php echo elgg_get_site_url() ?>mod/time_theme/files/<?php echo $page['img_url']; ?>" alt="Profile image example">
            <span id="page_photo"><img class="fb-image-profile thumbnail" align="left" src="<?php echo elgg_get_site_url() ?>mod/time_theme/files/pages/<?php echo $page['img_url']; ?>"></span>
            <div class="fb-profile-text" style="background-color: #fff;">
                <div class="add_to_circles_button">
                    <button type="button" class="btn btn-default" data-user_id="">
                        <span class="col-md-3 like_post">
                            <span>
                                <i class="fa <?php echo in_array($logged_user->guid, $post_likes) ? 'fa-thumbs-up' : 'fa-thumbs-o-up'; ?>" data-owner_guid="<?php echo $entity->owner_guid; ?>" data-guid="<?php echo $entity->guid; ?>" data-post_like_count="<?php echo count($post_likes); ?>"></i>
                            </span>
                            <span class="post_like_count"><?php echo count($post_likes); ?></span>
                        </span>
                    </button>
                </div>
                <h1 style="margin-top: -15px;padding-bottom: 5px">
                    <span class="page_name">
                        <?php echo $page['page_name']; ?>
                    </span>
                </h1>
            </div>
        </div>
    </div>
    <div class="col-md-12" style="background-color: #fff;padding: 5px 0 0 0;border-top: 1px solid #ddd;">
        <div class="col-md-4 post_box">
            <div class="row">
                <div>
                    <div style="text-align: center;">
                        <div class="col-md-4">
                            <i class="fa fa-refresh fa-2x colors-color-hover" style="color:#444"></i><br>
                            <span>Latest</span>
                        </div>
                        <div class="col-md-4">
                            <a href="<?php echo elgg_get_site_url() ?>company_pages/">
                                <i class="fa fa-pagelines fa-2x colors-color-hover" style="color:#3b5998"></i><br>
                                <span>Pages</span>
                            </a>
                        </div>
                        <div class="col-md-4">
                            <i class="fa fa-pencil fa-2x colors-color-hover" style="color:#00ff00"></i><br>
                            <span><a href="">Edit</a></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8 post_box">
            <div class="row">
                <div>
                    <div style="text-align: center;">
                        <div class="col-md-2" id="save_page_text" data-page_id="<?php echo $guid; ?>">
                            <i class="fa fa-font fa-2x colors-color-hover" style="color:#444"></i><br>
                            <span>Text</span>
                        </div>
                        <div class="col-md-2" id="save_page_photo" data-page_id="<?php echo $guid; ?>">
                            <i class="fa fa-camera fa-2x colors-color-hover" style="color:#d95e40"></i><br>
                            <span>Photo</span>
                        </div>
                        <div class="col-md-2" id="save_page_link" data-page_id="<?php echo $guid; ?>">
                            <i class="fa fa-link fa-2x colors-color-hover" style="color:#56bc8a"></i><br>
                            <span>Link</span>
                        </div>
                        <div class="col-md-2" id="save_page_audio" data-page_id="<?php echo $guid; ?>">
                            <i class="fa fa-volume-up fa-2x colors-color-hover" style="color:#529ecc"></i><br>
                            <span>Audio</span>
                        </div>
                        <div class="col-md-2" id="save_page_video" data-page_id="<?php echo $guid; ?>">
                            <i class="fa fa-film fa-2x colors-color-hover" style="color:#748089"></i><br>
                            <span>Video</span>
                        </div>
                        <div class="col-md-2" id="save_page_event" data-page_id="<?php echo $guid; ?>">
                            <i class="fa fa-calendar fa-2x colors-color-hover" style="color:#f2992e"></i><br>
                            <span>Event</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12" style="padding: 0px">
        <div id="masonry-container" class="js-masonry" data-masonry-options='{ "columnWidth": ".item", "itemSelector": ".item", "transitionDuration": "1.2s" }' style="padding-right: 0px !important;">
            <?php
            foreach ($entities as $entity) {
                $subtype = get_subtype_from_id($entity->subtype);
                $post_owner = get_entity($entity->owner_guid);
                $user_circles_list = array();
                $user_circles_value = 'circles_' . $post_owner->guid;
                if ($post_owner->$user_circles_value) {
                    $user_circles_list = maybe_unserialize($post_owner->$user_circles_value);
                }
                $post = maybe_unserialize($entity->description);
                $collection = get_access_collection($entity->access_id);
                $post_location = $collection->name;
                $owner_dp = 'user_dp_' . $post_owner->guid;
                $likes_value = 'post_likes_' . $entity->guid;
                $comments_value = 'post_comments_' . $entity->guid;
                $dp_src = ($post_owner->$owner_dp) ? elgg_get_site_url() . 'mod/time_theme/' . $post_owner->$owner_dp : elgg_get_site_url() . '_graphics/icons/user/defaultlarge.gif';
                $ia = elgg_set_ignore_access(true);
                $post_likes = ($entity->$likes_value) ? maybe_unserialize($entity->$likes_value) : array();
                $post_comments = ($entity->$comments_value) ? maybe_unserialize($entity->$comments_value) : array();
                elgg_set_ignore_access($ia);
                if (!user_can_view($user_circles_list, $post['content_privacy'], $post_owner->guid)) {
                    continue;
                }
                if ($subtype == 'photo_content_' . $guid) {
                    $line = 'added new photo';
                    include elgg_get_plugins_path() . 'time_theme/views/default/page/subtypes/photo_content.php';
                } elseif ($subtype == 'text_content_' . $guid) {
                    $line = 'updated status';
                    include elgg_get_plugins_path() . 'time_theme/views/default/page/subtypes/text_content.php';
                } elseif ($subtype == 'audio_content_' . $guid) {
                    $line = 'shared audio track';
                    include elgg_get_plugins_path() . 'time_theme/views/default/page/subtypes/audio_content.php';
                } elseif ($subtype == 'video_content_' . $guid) {
                    $line = 'shared video';
                    include elgg_get_plugins_path() . 'time_theme/views/default/page/subtypes/video_content.php';
                } elseif ($subtype == 'link_content_' . $guid) {
                    $line = 'shared link';
                    include elgg_get_plugins_path() . 'time_theme/views/default/page/subtypes/link_content.php';
                } elseif ($subtype == 'event_content_' . $guid) {
                    $line = 'created event';
                    include elgg_get_plugins_path() . 'time_theme/views/default/page/subtypes/event_content.php';
                } else {
                    $line = 'Text';
                }
            }
            ?>
        </div>
    </div>
    <?php
} else {
    forward(elgg_get_site_url() . 'company_pages/');
}
