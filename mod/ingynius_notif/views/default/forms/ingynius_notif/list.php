<?php
include_once elgg_get_plugins_path() . 'time_theme/functions.php';
$logged_user = elgg_get_logged_in_user_entity();
$notif_value = 'notifications_' . $logged_user->guid;
$notifications = ($logged_user->$notif_value) ? maybe_unserialize($logged_user->$notif_value) : array();
?>
<div class="row-fluid" style="background-color: #fff; margin-top: 10px;min-height: 780px;">
    <div class="col-md-12">
        <h2 class="page-header" style="border:none;margin: 10px 0 20px 0">
            Notifications
        </h2>
        <hr/>
    </div>
    <table class="table" style="margin-bottom: 0px">
        <?php

        function cmp($a, $b) {
            return $b["time"] - $a["time"];
        }

        usort($notifications, "cmp");
        foreach ($notifications as $key => $notification) {
            $entity = get_entity($notification['post_id']);
            $post = maybe_unserialize($entity->description);
            $user = get_entity($notification['from_id']);
            $type = $notification['type'];
            $on_time = $notification['time'];
            $seen = $notification['seen'];
            if ($entity) {
                ?>
                <tr class="notif_tr" style="border-bottom: 1px solid #ddd; <?php echo $seen == 1 ? 'background-color: #fff;' : 'background-color: #ebe4bc;' ?>">
                    <td style="width: 50px;border:none">
                        <img class="img" style="height: 35px" src="<?php echo getUserDp($user->guid) ?>"/>
                    </td>
                    <td style="border:none;padding:8px 5px">
                        <a href="<?php echo elgg_get_site_url() ?>profile/<?php echo $user->username; ?>"><?php echo $user->name ?></a>
                        <?php if ($type == 'like') { ?> 
                            <a class="post_details_notif" data-post="<?php echo $entity->guid; ?>" data-from="<?php echo $user->guid; ?>" data-type="<?php echo $type; ?>" data-time="<?php echo $on_time; ?>" href="<?php echo elgg_get_site_url() ?>post/<?php echo $entity->guid; ?>/">liked your post <?php echo substr($post['text_content'], 0, 80); ?>..</a>
                        <?php } else { ?>
                            <a class="post_details_notif" data-post="<?php echo $entity->guid; ?>" data-from="<?php echo $user->guid; ?>" data-type="<?php echo $type; ?>" data-time="<?php echo $on_time; ?>" href="<?php echo elgg_get_site_url() ?>post/<?php echo $entity->guid; ?>/">commented on your post <?php echo substr($post['text_content'], 0, 80); ?>..</a>
                        <?php } ?> 
                        <br/>
                        <span style="color:#999"><?php echo time_stamp($on_time); ?></span>
                    </td>
                    <td style="width: 50px;border:none">
                        <span class="notif_loader"></span>
                    </td>
                </tr>
                <?php
            }
        }
        ?>
    </table>
</div>