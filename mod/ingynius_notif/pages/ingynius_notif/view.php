<?php

// make sure only logged in users can see this page
gatekeeper();
include_once elgg_get_plugins_path() . 'time_theme/functions.php';
$guid = $_GET['page_id'];
$entity = get_entity($guid);
$page = maybe_unserialize($entity->description);
$title = $page['page_name'];
$content .= elgg_view_form("ingynius_pages/view");
$body = elgg_view_layout('one_sidebar', array(
    'content' => $content,
        ));
echo elgg_view_page($title, $body);
