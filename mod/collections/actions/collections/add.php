<?php

// Get Logged In User Details
$logged_user = elgg_get_logged_in_user_entity();

// Create Collection
$name = get_input('name');
$save = create_access_collection($name);

if ($save) {
    system_message("Collection Created Successfully!");
    forward('collections');
} else {
    register_error("Something Went Wrong!");
    forward('collections');
}