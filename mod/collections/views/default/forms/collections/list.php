<?php
$logged_user = elgg_get_logged_in_user_entity();
$collections = get_user_access_collections($logged_user->guid);
?>
<div class="row-fluid">
    <div class="col-md-12">
        <ul class="breadcrumb">
            <li><a href="<?php echo elgg_get_site_url(); ?>">Home</a></li>
            <li><a href="<?php echo elgg_get_site_url(); ?>collections/">Collections</a></li>
            <li class="active">Collections</li>
        </ul>
        <div class="breadcrumb_menu">
            <div class="pull-right">
                <a href="<?php echo elgg_get_site_url(); ?>collections/add/" class="btn btn-xs btn-default">
                    <i class="fa fa-plus-square"></i> New Collection
                </a>
            </div>
            <span>Collections</span>
        </div>
        <div class="clearfix"></div>
        <div class="panel panel-primary">
            <div class="panel-heading">Collections</div>
            <div class="panel-body">
                <table class="table">
                    <tbody>
                        <?php
                        foreach ($collections as $collection) {
                            if (is_object($collection)) {
                                ?>
                                <tr>
                                    <td style="width: 80%"><?php echo $collection->name; ?></td>
                                    <td style="width: 20%">
                                        <a href="<?php echo elgg_get_site_url(); ?>collections/edit/?id=<?php echo $collection->id; ?>"class="btn btn-primary btn-xs"><i class="fa fa-2x fa-pencil"></i></a>
                                        <a href="<?php echo elgg_get_site_url(); ?>collections/list/?id=<?php echo $collection->id; ?>"class="btn btn-danger btn-xs"><i class="fa fa-2x fa-trash-o"></i></a>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
