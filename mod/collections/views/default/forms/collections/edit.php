<?php
$collection_id = $_GET['id'];
$collection = get_access_collection($collection_id);
?>
<div class="row-fluid">
    <div class="col-md-12">
        <ul class="breadcrumb">
            <li><a href="<?php echo elgg_get_site_url(); ?>">Home</a></li>
            <li><a href="<?php echo elgg_get_site_url(); ?>collections/">Collections</a></li>
            <li class="active">Edit Collection</li>
        </ul>
        <div class="breadcrumb_menu">
            <div class="pull-right">
                <a href="<?php echo elgg_get_site_url(); ?>collections/add/" class="btn btn-xs btn-default">
                    <i class="fa fa-plus-square"></i> New Collection
                </a>
            </div>
            <span>Edit Collection</span>
        </div>
        <div class="clearfix"></div>
        <div class="panel panel-primary">
            <div class="panel-heading">Collections</div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="control-label"><?php echo elgg_echo("Collection Name"); ?></label><br />
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-group"></i></div>
                        <?php
                        echo elgg_view('input/text', array(
                            'name' => 'name',
                            'class' => 'form-control',
                            'id' => 'name',
                            'value' => $collection->name
                        ));
                        ?>
                        <?php
                        echo elgg_view('input/text', array(
                            'type' => 'hidden',
                            'name' => 'id',
                            'class' => 'form-control',
                            'id' => 'name',
                            'value' => $collection->id
                        ));
                        ?>
                    </div>
                </div>
                <div class="form-group">
                    <?php echo elgg_view('input/submit', array('value' => elgg_echo('Update'))); ?>
                </div>
            </div>
        </div>
    </div>
</div>