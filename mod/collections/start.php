<?php

elgg_register_event_handler('init', 'system', 'collections_init');

function collections_init() {
    elgg_register_page_handler('collections', 'collections_page_handler');
}

function collections_page_handler($segments) {
    if (!$segments[0]) {
        include elgg_get_plugins_path() . 'collections/pages/collections/list.php';
        return true;
    }
    if ($segments[0] == 'list') {
        include elgg_get_plugins_path() . 'collections/pages/collections/list.php';
        return true;
    }
    if ($segments[0] == 'add') {
        include elgg_get_plugins_path() . 'collections/pages/collections/add.php';
        return true;
    }
    if ($segments[0] == 'edit') {
        include elgg_get_plugins_path() . 'collections/pages/collections/edit.php';
        return true;
    }
    return false;
}

elgg_register_action("collections/add", elgg_get_plugins_path() . "collections/actions/collections/add.php");
elgg_register_action("collections/edit", elgg_get_plugins_path() . "collections/actions/collections/edit.php");