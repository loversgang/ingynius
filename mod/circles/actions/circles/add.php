<?php

// Get Logged In User Details
$logged_user = elgg_get_logged_in_user_entity();

// Create Circle
$data = new ElggObject();
$data->subtype = "circle";
$data->title = get_input('name');
$data->description = '';
$data->access_id = ACCESS_PUBLIC;
$data->owner_guid = elgg_get_logged_in_user_guid();
$saved = $data->save();

if ($saved) {
    system_message("Circle Created Successfully!");
    forward('circles');
} else {
    register_error("Something Went Wrong!");
    forward('circles');
}