<?php

// Get Logged In User Details
$logged_user = elgg_get_logged_in_user_entity();
$collection_id = get_input('id');
$name = get_input('name');

// Update Collection
$save = update_access_collection($collection_id, $name);

if ($save) {
    system_message("Collection Updated Successfully!");
    forward('circles');
} else {
    register_error("Something Went Wrong!");
    forward('circles');
}