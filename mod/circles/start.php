<?php

elgg_register_event_handler('init', 'system', 'circles_init');

function circles_init() {
    elgg_register_page_handler('circles', 'circles_page_handler');
}

function circles_page_handler($segments) {
    if (!$segments[0]) {
        include elgg_get_plugins_path() . 'circles/pages/circles/list.php';
        return true;
    }
    if ($segments[0] == 'list') {
        include elgg_get_plugins_path() . 'circles/pages/circles/list.php';
        return true;
    }
    if ($segments[0] == 'add') {
        include elgg_get_plugins_path() . 'circles/pages/circles/add.php';
        return true;
    }
    if ($segments[0] == 'edit') {
        include elgg_get_plugins_path() . 'circles/pages/circles/edit.php';
        return true;
    }
    return false;
}

elgg_register_action("circles/add", elgg_get_plugins_path() . "circles/actions/circles/add.php");
elgg_register_action("circles/edit", elgg_get_plugins_path() . "circles/actions/circles/edit.php");
