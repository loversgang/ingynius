<?php
$logged_user = elgg_get_logged_in_user_entity();
$circles = elgg_get_entities(array(
    'type' => 'object',
    'subtype' => 'circle',
    'limit' => FALSE,
        ));
?>
<div class="row-fluid">
    <div class="col-md-12">
        <ul class="breadcrumb">
            <li><a href="<?php echo elgg_get_site_url(); ?>">Home</a></li>
            <li><a href="<?php echo elgg_get_site_url(); ?>circles/">Circles</a></li>
            <li class="active">Circles</li>
        </ul>
        <div class="breadcrumb_menu">
            <div class="pull-right">
                <a href="<?php echo elgg_get_site_url(); ?>circles/add/" class="btn btn-xs btn-default">
                    <i class="fa fa-plus-square"></i> New Circle
                </a>
            </div>
            <span>Circles</span>
        </div>
        <div class="clearfix"></div>
        <div class="panel panel-primary">
            <div class="panel-heading">Circles</div>
            <div class="panel-body">
                <table class="table">
                    <tbody>
                        <?php
                        foreach ($circles as $circle) {
                            if (is_object($circle)) {
                                ?>
                                <tr>
                                    <td style="width: 80%"><?php echo $circle->title; ?></td>
                                    <td style="width: 20%">
                                        <a href="<?php echo elgg_get_site_url(); ?>circles/edit/?id=<?php echo $circle->guid; ?>"class="btn btn-primary btn-xs"><i class="fa fa-2x fa-pencil"></i></a>
                                        <a href="<?php echo elgg_get_site_url(); ?>circles/list/?id=<?php echo $circle->guid; ?>"class="btn btn-danger btn-xs"><i class="fa fa-2x fa-trash-o"></i></a>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
