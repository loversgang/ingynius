<div class="row-fluid">
    <div class="col-md-12">
        <ul class="breadcrumb">
            <li><a href="<?php echo elgg_get_site_url(); ?>">Home</a></li>
            <li><a href="<?php echo elgg_get_site_url(); ?>circles/">Circles</a></li>
            <li class="active">Add Circle</li>
        </ul>
        <div class="breadcrumb_menu">
            <span>Add Circle</span>
        </div>
        <div class="clearfix"></div>
        <div class="panel panel-primary">
            <div class="panel-heading">Circles</div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="control-label"><?php echo elgg_echo("Circle Name"); ?></label><br />
                    <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-group"></i></div>
                        <?php
                        echo elgg_view('input/text', array(
                            'name' => 'name',
                            'class' => 'form-control',
                            'id' => 'name',
                        ));
                        ?>
                    </div>
                </div>
                <div class="form-group">
                    <?php echo elgg_view('input/submit', array('value' => elgg_echo('save'))); ?>
                </div>
            </div>
        </div>
    </div>
</div>