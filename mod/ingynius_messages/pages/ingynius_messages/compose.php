<?php

// make sure only logged in users can see this page
gatekeeper();
$title = "Compose Message";
$content .= elgg_view_form("ingynius_messages/compose", array('id' => 'page_profile_pic_upload_form'));
$body = elgg_view_layout('one_sidebar', array(
    'content' => $content,
        ));
echo elgg_view_page($title, $body);
