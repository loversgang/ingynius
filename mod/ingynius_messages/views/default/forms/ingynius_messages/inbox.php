<?php
include_once elgg_get_plugins_path() . 'time_theme/functions.php';
$logged_user = elgg_get_logged_in_user_entity();
$friends = user_in_circles();
$msg_count = 0;
foreach ($friends as $user_id) {
    $messages = elgg_get_entities(array(
        'type' => 'object',
        'subtypes' => array('message_to_' . elgg_get_logged_in_user_guid() . '_' . $user_id, 'message_to_' . $user_id . '_' . elgg_get_logged_in_user_guid()),
        'limit' => 1,
        'order_by' => 'time_created desc'
    ));
    if ($messages) {
        $msg_count += 1;
    }
}
?>
<div class="row-fluid"> 
    <div class="col-md-12" style="min-height: 800px;background-color: #fff; padding-top: 10px">
        <h2 class="page-header" style="border:none;margin: 10px 0 20px 0">
            <span class="pull-right" style="margin-right: 15px;">
                <a href="<?php echo elgg_get_site_url(); ?>ingynius_messages/compose/" class="btn btn-success"><i class="fa fa-envelope"></i> Compose</a>
            </span>
            Inbox
        </h2>
        <?php if ($msg_count > 0) { ?>
            <div style="padding: 2px;background-color: #eee">
                <table class="table table-bordered" style="border: none">
                    <tbody>
                        <tr>
                            <td style="border:none;width: 35%;padding: 0px;" class="list_conversations"></td>
                            <td valign="top">
                                <div class="pull-right">
                                    <button type="button" data-guid="" class="open_in_chat btn btn-xs btn-success">Open Chat</button>
                                </div>
                                <hr/>
                                <div class="comments_loader"></div>
                                <div class="conversation" data-guid="" style="max-height: 300px;overflow: auto;"></div>
                                <hr/>
                                <div class="form reply_container">
                                    <div class="form-group">
                                        <textarea class="form-control message_content" rows="3"></textarea>
                                    </div>
                                    <div data-guid="" style="cursor: pointer" class="reply_conversation btn btn-success btn-xs pull-right">Reply</div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        <?php } else { ?>
            <div class="alert alert-info">
                No Conversation Yet!
            </div>
        <?php } ?>
    </div>
    <div class="col-md-12">
        <div class="chat_boxes"></div>
    </div>
</div>