<?php
include_once elgg_get_plugins_path() . 'time_theme/functions.php';
$friends = user_in_circles();
$get_user_id = isset($_GET['user_id']) ? $_GET['user_id'] : 'all';
if ($get_user_id != 'all') {
    if (!in_array($get_user_id, $friends)) {
        ?>
        <br/>
        <div class="alert alert-danger">
            <b>Error:</b> You are not authorized to use this service!
        </div>
        <?php
    }
}
?>
<div class="row-fluid message_container"> 
    <div class="col-md-12" style="min-height: 800px;background-color: #fff; padding-top: 10px">
        <h2 class="page-header" style="border:none;margin: 10px 0 20px 0">
            <span class="pull-right" style="margin-right: 15px;">
                <a href="<?php echo elgg_get_site_url(); ?>ingynius_messages/" class="btn btn-success"><i class="fa fa-inbox"></i> Inbox</a>
            </span>
            Compose Message
        </h2>
        <div class="form-group">
            <label class="control-label">Select Member</label>
            <select class="form-control chosen" id="to_user">
                <?php
                foreach ($friends as $user_id) {
                    $member = get_entity($user_id);
                    ?>
                    <option value="<?php echo $member->guid; ?>" <?php echo $get_user_id == $user_id ? 'selected' : '' ?>><?php echo $member->name; ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">Message</label>
            <textarea class="form-control" row="3" id="message_content"></textarea>
        </div>
        <div class="form-group">
            <div class="pull-right">
                <button class="btn btn-success send_message">Send</button>
            </div>
        </div>
    </div>
</div>