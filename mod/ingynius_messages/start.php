<?php

elgg_register_event_handler('init', 'system', 'ingynius_messages_init');

function ingynius_messages_init() {
    elgg_register_plugin_hook_handler('entity:url', 'object', 'pages_set_url');
    elgg_register_page_handler('ingynius_messages', 'ingynius_messages_page_handler');
}

function ingynius_messages_page_handler($segments) {
    if (!$segments[0]) {
        include elgg_get_plugins_path() . 'ingynius_messages/pages/ingynius_messages/inbox.php';
        return true;
    }
    if ($segments[0] == 'inbox') {
        include elgg_get_plugins_path() . 'ingynius_messages/pages/ingynius_messages/inbox.php';
        return true;
    }
    if ($segments[0] == 'compose') {
        include elgg_get_plugins_path() . 'ingynius_messages/pages/ingynius_messages/compose.php';
        return true;
    }
    if ($segments[0] == 'view') {
        include elgg_get_plugins_path() . 'ingynius_messages/pages/ingynius_messages/view.php';
        return true;
    }
    return false;
}