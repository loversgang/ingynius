//<script>
    elgg.provide('elgg.time_theme');
    elgg.time_theme.init = function () {
        $('.masonry_load_more').click(function () {
            if (ajax_sent === true)
                return;
            ajax_sent = true;
            var el = $(this);
            var text_container = el.find('#load_more_text');
            var original_text = text_container.html();
            text_container.html('Loading...');
            setTimeout(function () {
                $.ajax({type: "GET",
                    url: '<?php echo elgg_get_site_url() . "ajax/view/time_theme/ajax"; ?>',
                    dataType: "html",
                    cache: false,
                    success: function (data) {
                        ajax_sent = false;
                        text_container.html(original_text);
                        $('.js-masonry').append(data).ready(function () {
                            $('.js-masonry').masonry('reloadItems').masonry('layout');
                            $("div.bhoechie-tab-menu>div.list-group>a[data-clicked=\"1\"]").click();
                        });
                    }
                });
            }, 500);
        });
    });
    elgg.register_hook_handler('init', 'system', elgg.time_theme.init);