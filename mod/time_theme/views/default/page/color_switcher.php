<div id="custumize-style" style="right: -293px">
    <h1>Layout Color Switcher</h1>
    <a href="#" class="switcher"><span class="changebutton"><i class="fa fa-times"></i></span></a>
    <div class="switcher_content">
        <p>Choose one color below to change main layout color. You can apply more styles in theme options.</p>
        <div>
            <h3>Skins Colors</h3>
            <ul class="colors-style" id="color1">
                <li><a href="#" class="color_1"></a></li>
                <li><a href="#" class="color_2"></a></li>
                <li><a href="#" class="color_3"></a></li>
                <li><a href="#" class="color_4"></a></li>
                <li><a href="#" class="color_5"></a></li>
                <li><a href="#" class="color_6"></a></li>
                <li><a href="#" class="color_7"></a></li>
                <li><a href="#" class="color_8"></a></li>
                <li><a href="#" class="color_9"></a></li>
                <li><a href="#" class="color_10"></a></li>
                <li><a href="#" class="color_11"></a></li>
                <li><a href="#" class="color_12"></a></li>
                <li><a href="#" class="color_13"></a></li>
                <li><a href="#" class="color_14"></a></li>
                <li><a href="#" class="color_15"></a></li>
                <li><a href="#" class="color_16"></a></li>
                <li><a href="#" class="color_17"></a></li>
                <li><a href="#" class="color_18"></a></li>
                <li><a href="#" class="color_19"></a></li>
                <li><a href="#" class="color_20"></a></li>
                <li><a href="#" class="color_21"></a></li>
                <li><a href="#" class="color_22"></a></li>
                <li><a href="#" class="color_23"></a></li>
                <li><a href="#" class="color_24"></a></li>
                <li><a href="#" class="color_25"></a></li>
                <li><a href="#" class="color_26"></a></li>
                <li><a href="#" class="color_27"></a></li>
                <li><a href="#" class="color_28"></a></li>
                <li><a href="#" class="color_29"></a></li>
                <li><a href="#" class="color_30"></a></li>
                <li><a href="#" class="color_31"></a></li>
                <li><a href="#" class="color_32"></a></li>
                <li><a href="#" class="color_33"></a></li>
                <li><a href="#" class="color_34"></a></li>
                <li><a href="#" class="color_35"></a></li>
                <li><a href="#" class="color_36"></a></li>
                <li><a href="#" class="color_37"></a></li>
                <li><a href="#" class="color_38"></a></li>
                <li><a href="#" class="color_39"></a></li>
                <li><a href="#" class="color_40"></a></li>
            </ul>
        </div>
        <div>
            <div class="pull-right" style="padding-top: 2px">
                <button class=" btn btn-primary btn-xs" id="menu_layout_toggle_button">
                    <i class="fa fa-refresh"></i>
                </button>
            </div>
            <h3>Toggle Menu Layout</h3>
        </div>
        <div id="button-reset"><a href="#" class="button color green boxed">Reset</a></div>
    </div>
</div>