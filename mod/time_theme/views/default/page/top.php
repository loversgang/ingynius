<div class="col-md-2" style="width: 22%">
    <header id="header" class="header_1 content_3">
        <div class="logo logo-circle">
            <div class="dp-change">
                <i class="fa fa-camera fa-2x" id="dp-change"></i>
                <form class="save_user_dp">
                    <input type="file" name="userImage" id="user_profile_dp" class="custom-file-input" original-title="Change Profile Picture">
                </form>
            </div>
            <?php if ($logged_user->$user_dp) { ?>
                <img id="user_dp" src="<?php echo elgg_get_site_url(); ?>mod/time_theme/<?php echo $logged_user->$user_dp ?>" class="img img-circle" style="height: 200px;" alt="Logo">
            <?php } else { ?>
                <img id="user_dp" src="<?php echo elgg_get_site_url(); ?>_graphics/icons/user/defaultlarge.gif" class="img img-circle" style="height: 200px;" alt="Logo">
            <?php } ?>
        </div>
        <h3 style="text-align: center" class="colors-color"><?php echo $logged_user->name ?></h3>
        <h4 class="tagline"><?php echo $user->headline ? $user->headline : ''; ?></h4>
        <div class="row profile-links">
            <div class="col-md-1"></div>
            <div class="col-md-2 profile-link">
                <a href="<?php echo elgg_get_site_url(); ?>ingynius_messages/" title="Messages"><i class="fa fa-2x fa-envelope"></i></a>
            </div>
            <div class="col-md-2 profile-link" <?php echo count($unread_notif) > 0 ? 'style="top: -15px"' : ''; ?>>
                <a href="<?php echo elgg_get_site_url(); ?>ingynius_notif/" title="Notifications">
                    <?php echo count($unread_notif) > 0 ? '<span class="notif">' . count($unread_notif) . '</span>' : ''; ?>
                    <i class="fa fa-2x fa-bell">
                    </i>
                </a>
            </div>
            <div class="col-md-2 profile-link">
                <a href="" title="Add"><i class="fa fa-2x fa-plus-circle"></i></a>
            </div>
            <div class="col-md-2 profile-link">
                <a href="<?php echo elgg_get_site_url(); ?>profile_wizard/" title="Edit Profile"><i class="fa fa-2x fa-pencil"></i></a>
            </div>
            <div class="col-md-2 profile-link">
                <a href="<?php echo elgg_get_site_url(); ?>password/" title="Change Password"><i class="fa fa-2x fa-lock"></i></a>
            </div>
            <div class="col-md-1"></div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h5 class="post_title_cl colors-color">
                    About Me
                </h5>
            </div>
            <div class="col-md-12" style="margin-top: 10px;margin-bottom: 10px">
                <div class="col-md-6" style="text-align: center;">
                    <div class="colors-color">Work:</div>
                    <div class="colors-color">Live:</div>
                    <div class="colors-color">Hometown:</div>
                    <div class="colors-color">Birthday:</div>
                </div>
                <div class="col-md-6" style="text-align: center;color:#fff;">
                    <div class=""><?php echo $user->work ? ucfirst($user->work) : ''; ?></div>
                    <div class=""><?php echo $user->living ? ucfirst($user->living) : ''; ?></div>
                    <div class=""><?php echo $user->hometown ? ucfirst($user->hometown) : ''; ?></div>
                    <div class=""><?php echo date('M', mktime(0, 0, 0, $user->birth_month, 1, 2000)); ?> <?php echo $user->birth_date; ?>th</div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" style="margin-top: 10px">
                <h5 class="post_title_cl colors-color">
                    ONLINE
                </h5>
            </div>
            <div class="col-md-12" style="margin-bottom: 10px">
                <!-- Nav tabs -->
                <div class="card" style="box-shadow: none">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#online_newest" aria-controls="home" role="tab" data-toggle="tab">Newest</a></li>
                        <li role="presentation"><a href="#online_featured" aria-controls="profile" role="tab" data-toggle="tab">Featured</a></li>
                        <li role="presentation"><a href="#online_active" aria-controls="messages" role="tab" data-toggle="tab">Active</a></li>
                        <li role="presentation"><a href="#online_all" aria-controls="settings" role="tab" data-toggle="tab">See All</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="online_newest">
                            <div class="row-fluid">
                                <?php
                                foreach ($online_members_newest as $member) {
                                    $owner_dp = 'user_dp_' . $member->guid;
                                    $ia = elgg_set_ignore_access(true);
                                    $dp_src = ($member->$owner_dp) ? elgg_get_site_url() . 'mod/time_theme/' . $member->$owner_dp : elgg_get_site_url() . '_graphics/icons/user/defaultlarge.gif';
                                    elgg_set_ignore_access($ia);
                                    ?>
                                    <div class="col-md-4" style="margin-bottom: 10px">
                                        <a href="<?php echo elgg_get_site_url() ?>profile/<?php echo $member->username; ?>">
                                            <img style="width: 50px;height: 40px" src="<?php echo $dp_src; ?>" class="img img-responsive img-circle"/>
                                        </a>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="online_featured">
                            <div class="row-fluid">
                                <?php
                                foreach ($online_members_featured as $member) {
                                    $owner_dp = 'user_dp_' . $member->guid;
                                    $ia = elgg_set_ignore_access(true);
                                    $dp_src = ($member->$owner_dp) ? elgg_get_site_url() . 'mod/time_theme/' . $member->$owner_dp : elgg_get_site_url() . '_graphics/icons/user/defaultlarge.gif';
                                    elgg_set_ignore_access($ia);
                                    ?>
                                    <div class="col-md-4" style="margin-bottom: 10px">
                                        <a href="<?php echo elgg_get_site_url() ?>profile/<?php echo $member->username; ?>">
                                            <img style="width: 50px;height: 40px" src="<?php echo $dp_src; ?>" class="img img-responsive img-circle"/>
                                        </a>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="online_active">
                            <div class="row-fluid">
                                <?php
                                foreach ($online_members_popular as $member) {
                                    $owner_dp = 'user_dp_' . $member->guid;
                                    $ia = elgg_set_ignore_access(true);
                                    $dp_src = ($member->$owner_dp) ? elgg_get_site_url() . 'mod/time_theme/' . $member->$owner_dp : elgg_get_site_url() . '_graphics/icons/user/defaultlarge.gif';
                                    elgg_set_ignore_access($ia);
                                    ?>
                                    <div class="col-md-4" style="margin-bottom: 10px">
                                        <a href="<?php echo elgg_get_site_url() ?>profile/<?php echo $member->username; ?>">
                                            <img style="width: 50px;height: 40px" src="<?php echo $dp_src; ?>" class="img img-responsive img-circle"/>
                                        </a>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="online_all">
                            <div class="row-fluid">
                                <?php
                                foreach ($members as $member) {
                                    $owner_dp = 'user_dp_' . $member->guid;
                                    $ia = elgg_set_ignore_access(true);
                                    $dp_src = ($member->$owner_dp) ? elgg_get_site_url() . 'mod/time_theme/' . $member->$owner_dp : elgg_get_site_url() . '_graphics/icons/user/defaultlarge.gif';
                                    elgg_set_ignore_access($ia);
                                    ?>
                                    <div class="col-md-4" style="margin-bottom: 10px">
                                        <a href="<?php echo elgg_get_site_url() ?>profile/<?php echo $member->username; ?>">
                                            <img style="width: 50px;height: 40px" src="<?php echo $dp_src; ?>" class="img img-responsive img-circle"/>
                                        </a>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <h5 class="post_title_cl colors-color">
                        GROUPS
                    </h5>
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#groups_newest" aria-controls="home" role="tab" data-toggle="tab">Latest</a></li>
                        <li role="presentation"><a href="#groups_featured" aria-controls="profile" role="tab" data-toggle="tab">Featured</a></li>
                        <li role="presentation"><a href="#groups_popular" aria-controls="messages" role="tab" data-toggle="tab">Popular</a></li>
                        <li role="presentation"><a href="#groups_all" aria-controls="settings" role="tab" data-toggle="tab">See All</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="groups_newest">
                            <div class="row-fluid">
                                <div class="demo">
                                    <ul id="groups_latest_slider">
                                        <?php foreach ($groups_latest as $group) { ?>
                                            <li data-thumb="<?php echo getGroupDp($group->guid) ?>">
                                                <img class="group_img" data-entity="<?php echo $group->guid; ?>" src="<?php echo getGroupDp($group->guid) ?>" />
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="groups_featured">
                            <div class="row-fluid">
                                <div class="demo">
                                    <ul id="groups_featured_slider">
                                        <?php foreach ($groups_latest as $group) { ?>
                                            <li data-thumb="<?php echo getGroupDp($group->guid) ?>">
                                                <img class="group_img" data-entity="<?php echo $group->guid; ?>" src="<?php echo getGroupDp($group->guid) ?>" />
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="groups_popular">
                            <div class="row-fluid">
                                <div class="demo">
                                    <ul id="groups_popular_slider">
                                        <?php foreach ($groups_latest as $group) { ?>
                                            <li data-thumb="<?php echo getGroupDp($group->guid) ?>">
                                                <img class="group_img" data-entity="<?php echo $group->guid; ?>" src="<?php echo getGroupDp($group->guid) ?>" />
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="groups_all">
                            <div class="row-fluid">
                                <div class="demo">
                                    <ul id="groups_all_slider">
                                        <?php foreach ($groups_latest as $group) { ?>
                                            <li data-thumb="<?php echo getGroupDp($group->guid) ?>">
                                                <img class="group_img" data-entity="<?php echo $group->guid; ?>" src="<?php echo getGroupDp($group->guid) ?>" />
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <h5 class="post_title_cl colors-color">
                        PAGES
                    </h5>
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#pages_newest" aria-controls="home" role="tab" data-toggle="tab">Latest</a></li>
                        <li role="presentation"><a href="#pages_featured" aria-controls="profile" role="tab" data-toggle="tab">Featured</a></li>
                        <li role="presentation"><a href="#pages_popular" aria-controls="messages" role="tab" data-toggle="tab">Popular</a></li>
                        <li role="presentation"><a href="#pages_all" aria-controls="settings" role="tab" data-toggle="tab">See All</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="pages_newest">
                            <div class="row-fluid">
                                <div class="demo">
                                    <ul id="pages_newest_slider">
                                        <?php
                                        foreach ($pages_latest as $page) {
                                            $post = maybe_unserialize($page->description);
                                            ?>
                                            <li data-thumb="<?php echo elgg_get_site_url() ?>mod/time_theme/files/<?php echo $post['img_url']; ?>">

                                                <img class="pages_img" data-entity="<?php echo $page->guid; ?>" src="<?php echo elgg_get_site_url() ?>mod/time_theme/files/<?php echo $post['img_url']; ?>" />
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="pages_featured">
                            <div class="row-fluid">
                                <div class="demo">
                                    <ul id="pages_featured_slider">
                                        <?php
                                        foreach ($pages_featured as $page) {
                                            $post = maybe_unserialize($page->description);
                                            ?>
                                            <li data-thumb="<?php echo elgg_get_site_url() ?>mod/time_theme/files/<?php echo $post['img_url']; ?>">

                                                <img class="pages_img" data-entity="<?php echo $page->guid; ?>" src="<?php echo elgg_get_site_url() ?>mod/time_theme/files/<?php echo $post['img_url']; ?>" />
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="pages_popular">
                            <div class="row-fluid">
                                <div class="demo">
                                    <ul id="pages_popular_slider">
                                        <?php
                                        foreach ($pages_popular as $page) {
                                            $post = maybe_unserialize($page->description);
                                            ?>
                                            <li data-thumb="<?php echo elgg_get_site_url() ?>mod/time_theme/files/<?php echo $post['img_url']; ?>">

                                                <img class="pages_img" data-entity="<?php echo $page->guid; ?>" src="<?php echo elgg_get_site_url() ?>mod/time_theme/files/<?php echo $post['img_url']; ?>" />
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="pages_all">
                            <div class="row-fluid">
                                <div class="demo">
                                    <ul id="pages_all_slider">
                                        <?php
                                        foreach ($pages as $page) {
                                            $post = maybe_unserialize($page->description);
                                            ?>
                                            <li data-thumb="<?php echo elgg_get_site_url() ?>mod/time_theme/files/<?php echo $post['img_url']; ?>">

                                                <img class="pages_img" data-entity="<?php echo $page->guid; ?>" src="<?php echo elgg_get_site_url() ?>mod/time_theme/files/<?php echo $post['img_url']; ?>" />
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="copyright">
            <p style="margin-bottom:50px;">Copyright &copy; <?php echo date('Y') . ' - ' . date('Y', strtotime(date('Y') . '+ 1 Year')) ?>.</p>		
        </div>
    </header>
</div>