<?php

// Get Logged In User Details
$logged_user = elgg_get_logged_in_user_entity();
// Gel All Members
$members = elgg_get_entities(array(
    'type' => 'user',
        ));

//Get Online Members Newest
$online_members_newest = elgg_get_entities(array(
    'type' => 'user',
    'limit' => 4
        ));
// Gel All Members featured
$online_members_featured = elgg_get_entities(array(
    'type' => 'user',
    'limit' => 2
        ));

//Get Online Members Popular
$online_members_popular = elgg_get_entities(array(
    'type' => 'user',
    'limit' => 5
        ));
