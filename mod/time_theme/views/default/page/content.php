<div id="all_posts_div" class="col-md-7" style="padding: 0px 5px;width:50%;<?php echo elgg_get_context() != ('activity' || 'profile') ? 'background-color:#fff' : '' ?>">
    <?php if (count($vars['sysmessages']['success']) > 0) { ?>
        <?php foreach ($vars['sysmessages']['success'] as $success) { ?>
            <div class="alert alert-success" style="margin: 5px 15px"><?php echo $success; ?></div>
        <?php } ?>
    <?php } ?>
    <?php if (count($vars['sysmessages']['error']) > 0) { ?>
        <?php foreach ($vars['sysmessages']['error'] as $error) { ?>
            <div class="alert alert-danger" style="margin: 5px 15px"><?php echo $error; ?></div>
        <?php } ?>
    <?php } ?>
    <?php
    if ((elgg_get_context() == 'activity')) {
        include 'activity.php';
    } elseif ((elgg_get_context() == 'profile')) {
        include 'profile.php';
    } else {
        echo $content;
    }
    ?>
</div>