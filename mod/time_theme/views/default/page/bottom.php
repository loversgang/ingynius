<div class="col-md-2 chat_sb" style="width:22%">
    <?php include 'color_switcher.php'; ?>
    <div id="sidebar-wrapper" class="content_2 active">
        <div class="sidebar_closed" id="sidebar_closed" title="Close Sidebar" data-toggle="tooltip">
            <i class="icon-close"></i>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <h5 class="post_title_cl colors-color">
                        LATEST IMAGES
                    </h5>
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#images_newest" aria-controls="home" role="tab" data-toggle="tab">Latest</a></li>
                        <li role="presentation"><a href="#images_featured" aria-controls="profile" role="tab" data-toggle="tab">Featured</a></li>
                        <li role="presentation"><a href="#images_popular" aria-controls="messages" role="tab" data-toggle="tab">Popular</a></li>
                        <li role="presentation"><a href="#images_all" aria-controls="settings" role="tab" data-toggle="tab">See All</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="images_newest">
                            <div class="row-fluid">
                                <div class="demo">
                                    <ul id="images_newest_slider">
                                        <?php
                                        foreach ($files_latest as $file) {
                                            $post = maybe_unserialize($file->description);
                                            ?>
                                            <li data-thumb="<?php echo elgg_get_site_url() ?>mod/time_theme/<?php echo $post['img_url']; ?>">
                                                <img data-entity="<?php echo $file->guid; ?>" class="img_options" src="<?php echo elgg_get_site_url() ?>mod/time_theme/<?php echo $post['img_url']; ?>" />
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="images_featured">
                            <div class="row-fluid">
                                <div class="demo">
                                    <ul id="images_featured_slider">
                                        <?php
                                        foreach ($files_featured as $file) {
                                            $post = maybe_unserialize($file->description);
                                            ?>
                                            <li data-thumb="<?php echo elgg_get_site_url() ?>mod/time_theme/<?php echo $post['img_url']; ?>">
                                                <img data-entity="<?php echo $file->guid; ?>" class="img_options" src="<?php echo elgg_get_site_url() ?>mod/time_theme/<?php echo $post['img_url']; ?>" />
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="images_popular">
                            <div class="row-fluid">
                                <div class="demo">
                                    <ul id="images_popular_slider">
                                        <?php
                                        foreach ($files_popular as $file) {
                                            $post = maybe_unserialize($file->description);
                                            ?>
                                            <li data-thumb="<?php echo elgg_get_site_url() ?>mod/time_theme/<?php echo $post['img_url']; ?>">
                                                <img data-entity="<?php echo $file->guid; ?>" class="img_options" src="<?php echo elgg_get_site_url() ?>mod/time_theme/<?php echo $post['img_url']; ?>" />
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="images_all">
                            <div class="row-fluid">
                                <div class="demo">
                                    <ul id="images_all_slider">
                                        <?php
                                        foreach ($files as $file) {
                                            $post = maybe_unserialize($file->description);
                                            ?>
                                            <li data-thumb="<?php echo elgg_get_site_url() ?>mod/time_theme/<?php echo $post['img_url']; ?>">
                                                <img data-entity="<?php echo $file->guid; ?>" class="img_options" src="<?php echo elgg_get_site_url() ?>mod/time_theme/<?php echo $post['img_url']; ?>" />
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <h5 class="post_title_cl colors-color">
                        LATEST VIDEOS
                    </h5>
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#videos_newest" aria-controls="home" role="tab" data-toggle="tab">Latest</a></li>
                        <li role="presentation"><a href="#videos_featured" aria-controls="profile" role="tab" data-toggle="tab">Featured</a></li>
                        <li role="presentation"><a href="#videos_popular" aria-controls="messages" role="tab" data-toggle="tab">Popular</a></li>
                        <li role="presentation"><a href="#videos_all" aria-controls="settings" role="tab" data-toggle="tab">See All</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="videos_newest">
                            <div class="row-fluid">
                                <div class="demo">
                                    <ul id="videos_newest_slider">
                                        <?php
                                        foreach ($videos_latest as $video) {
                                            $post = maybe_unserialize($video->description);
                                            ?>
                                            <li data-thumb="<?php echo elgg_get_site_url() ?>mod/time_theme/lib/plugins/thumb/thumbs/<?php echo $post['videoID']; ?>-play.jpg">
                                                <img data-entity="<?php echo $video->guid; ?>" class="video_options" data-video="<?php echo $post['videoID']; ?>" src="<?php echo elgg_get_site_url() ?>mod/time_theme/lib/plugins/thumb/thumbs/<?php echo $post['videoID']; ?>-play.jpg" />
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="videos_featured">
                            <div class="row-fluid">
                                <div class="demo">
                                    <ul id="videos_featured_slider">
                                        <?php
                                        foreach ($videos_featured as $video) {
                                            $post = maybe_unserialize($video->description);
                                            ?>
                                            <li data-thumb="<?php echo elgg_get_site_url() ?>mod/time_theme/lib/plugins/thumb/thumbs/<?php echo $post['videoID']; ?>-play.jpg">
                                                <img data-entity="<?php echo $video->guid; ?>" class="video_options" data-video="<?php echo $post['videoID']; ?>" src="<?php echo elgg_get_site_url() ?>mod/time_theme/lib/plugins/thumb/thumbs/<?php echo $post['videoID']; ?>-play.jpg" />
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="videos_popular">
                            <div class="row-fluid">
                                <div class="demo">
                                    <ul id="videos_popular_slider">
                                        <?php
                                        foreach ($videos_popular as $video) {
                                            $post = maybe_unserialize($video->description);
                                            ?>
                                            <li data-thumb="<?php echo elgg_get_site_url() ?>mod/time_theme/lib/plugins/thumb/thumbs/<?php echo $post['videoID']; ?>-play.jpg">
                                                <img data-entity="<?php echo $video->guid; ?>" class="video_options" data-video="<?php echo $post['videoID']; ?>" src="<?php echo elgg_get_site_url() ?>mod/time_theme/lib/plugins/thumb/thumbs/<?php echo $post['videoID']; ?>-play.jpg" />
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="videos_all">
                            <div class="row-fluid">
                                <div class="demo">
                                    <ul id="videos_all_slider">
                                        <?php
                                        foreach ($videos as $video) {
                                            $post = maybe_unserialize($video->description);
                                            ?>
                                            <li data-thumb="<?php echo elgg_get_site_url() ?>mod/time_theme/lib/plugins/thumb/thumbs/<?php echo $post['videoID']; ?>-play.jpg">
                                                <img data-entity="<?php echo $video->guid; ?>" class="video_options" data-video="<?php echo $post['videoID']; ?>" src="<?php echo elgg_get_site_url() ?>mod/time_theme/lib/plugins/thumb/thumbs/<?php echo $post['videoID']; ?>-play.jpg" />
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 5px;margin-top: 15px">
                <h5 class="post_title_cl colors-color">
                    UPCOMING EVENTS
                </h5>
            </div>
            <div class="col-md-12" style="margin-bottom: 10px;margin-top: 10px">
                <div id="datetimepicker12" style="background-color: #fff"></div>
            </div>
            <?php
            foreach ($events as $event) {
                $post = maybe_unserialize($event->description);
                ?>
                <div class="col-md-12" style="margin-bottom: 10px;border-bottom: 1px solid #ddd">
                    <div class="col-md-2">
                        <center>
                            <div style="color: orange;">
                                <?php echo strtoupper(date('M', strtotime($post['date']))); ?>
                            </div>
                            <div style="font-size: 30px;margin-top: -5px;color: #999999;">
                                <?php echo strtoupper(date('d', strtotime($post['date']))); ?>
                            </div>
                        </center>
                    </div>
                    <div class="col-md-6" style="color:#fff">
                        <?php echo $post['title']; ?>
                    </div>
                    <div class="col-md-4">
                        <img src="/ingynius/images/cover-1.jpg" class="img img-responsive events_img"/>
                    </div>
                </div>
            <?php } ?>
            <div class="clearfix" style="margin-bottom: 50px"></div>
        </div>

        <!-- #sidebar -->

        <a id="go-top-button" class="test" href="#"><i class="fa fa-chevron-up"></i></a>
        <!--go-top-button-->

    </div>
</div>