<?php
/**
 * Elgg topbar wrapper
 * Check if the user is logged in and display a topbar
 * @since 1.10 
 */
// Get Logged In User Details
$logged_user = elgg_get_logged_in_user_entity();
?>
<div class="preloader">
    <div class="spinner">
        <div class="dot1"></div>
        <div class="dot2"></div>
    </div>
</div>
<div id="content-wrapper" class="menu_content_wrapper<?php echo isset($_COOKIE['menu_layout']) && $_COOKIE['menu_layout'] ? ' inverse' : '' ?>">
    <div id="main-content" style="margin: 10px;margin-bottom: 0">
        <?php if (elgg_is_logged_in()) { ?>
            <ul class="top-menu transition_200">
                <li style="position: absolute;margin-top: 5px;margin-left: -40px;">
                    <a href="<?php echo elgg_get_site_url() ?>action/logout" title="Logout"><i class="fa fa-2x fa-power-off colors-color"></i></a>
                </li>
                <li>
                    <a href="<?php echo elgg_get_site_url() ?>" class="">Business</a>
                </li>
                <li>
                    <a href="<?php echo elgg_get_site_url() ?>">Self</a>
                </li>
                <li>
                    <a href="<?php echo elgg_get_site_url() ?>">Shopping</a>
                </li>
                <li>
                    <a href="<?php echo elgg_get_site_url() ?>">Community</a>
                </li>
                <li>
                    <a id="base_url" href="<?php echo elgg_get_site_url() ?>"><img class="ig-icon" src="<?php echo elgg_get_site_url() ?>mod/time_theme/graphics/logo.png"></a>
                </li>
                <li>
                    <a href="<?php echo elgg_get_site_url() ?>">Events</a>
                </li>
                <li>
                    <a href="<?php echo elgg_get_site_url() ?>">Leisure</a>
                </li>
                <li>
                    <a href="<?php echo elgg_get_site_url() ?>">Entertainment</a>
                </li>
                <li>
                    <?php if ($logged_user->admin == 'yes') { ?>
                        <a href="<?php echo elgg_get_site_url(); ?>admin">Admin</a>
                    <?php } else { ?>
                        <a href="<?php echo elgg_get_site_url(); ?>profile/<?php echo $logged_user->username; ?>">Profile</a>
                    <?php } ?>
                </li>
                <li style="position: absolute;margin-top: 5px">
                    <a href="#" id="trigger_click" title="Color Switcher"><span class="changebutton"><i class="fa fa-2x fa-cog colors-color"></i></span></a>
                </li>
            </ul>
        <?php } else { ?>
            <style>
                ul.top-menu li{
                    margin: 0px 5px;
                }
            </style>
            <ul class="top-menu transition_200">
                <li style="position: absolute;margin-top: 5px;margin-left: -40px;">
                    <a href="<?php echo elgg_get_site_url() ?>action/logout" title="Logout"><i class="fa fa-2x fa-power-off colors-color"></i></a>
                </li>
                <li>
                    <a href="<?php echo elgg_get_site_url() ?>" class="">Business</a>
                </li>
                <li>
                    <a href="<?php echo elgg_get_site_url() ?>">Entertainment</a>
                </li>
                <li>
                    <a href="<?php echo elgg_get_site_url() ?>">Shopping</a>
                </li>
                <li>
                    <a id="base_url" href="<?php echo elgg_get_site_url() ?>"><img class="ig-icon" src="<?php echo elgg_get_site_url() ?>mod/time_theme/graphics/logo.png"></a>
                </li>
                <li>
                    <a href="<?php echo elgg_get_site_url() ?>">Events</a>
                </li>
                <li>
                    <a href="<?php echo elgg_get_site_url() ?>">Leisure</a>
                </li>
                <li>
                    <a href="<?php echo elgg_get_site_url() ?>">Self</a>
                </li>
                <li>
                    <a href="<?php echo elgg_get_site_url() ?>">Community</a>
                </li>
                <li style="position: absolute;margin-top: 5px">
                    <a href="#" id="trigger_click" title="Color Switcher"><span class="changebutton"><i class="fa fa-2x fa-cog colors-color"></i></span></a>
                </li>
            </ul>
        <?php } ?>
    </div>
</div>