<?php

// Get Logged In User Details
$logged_user = elgg_get_logged_in_user_entity();

// Get All Videos
$videos = elgg_get_entities(array(
    'type' => 'object',
    'subtype' => 'video_content',
        ));
// Get Latest Videos
$videos_latest = elgg_get_entities(array(
    'type' => 'object',
    'subtype' => 'video_content',
    'limit' => 10
        ));

// Get Featured Videos
$videos_featured = elgg_get_entities(array(
    'type' => 'object',
    'subtype' => 'video_content',
    'limit' => 2
        ));

// Get Popular Videos
$videos_popular = elgg_get_entities(array(
    'type' => 'object',
    'subtype' => 'video_content',
    'limit' => 3
        ));
