<?php

// Get Logged In User Details
$logged_user = elgg_get_logged_in_user_entity();

// Get All Videos
$events = elgg_get_entities(array(
    'type' => 'object',
    'subtype' => 'event_content',
    'limit' => 3
        ));
