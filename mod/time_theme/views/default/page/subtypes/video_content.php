<div class="item" data-type="media" id="post_<?php echo $entity->guid; ?>" data-entity="<?php echo $entity->guid; ?>">
    <div class="row item_content">
        <div class="col-md-2" style="width:65px">
            <img src="<?php echo $dp_src; ?>" class="img img-circle" style="height:35px"/>
        </div>
        <div class="col-md-8" style="margin-left: -20px;">
            <?php if ($entity->owner_guid == $logged_user->guid) { ?>
                <div class="pull-right actions_icon">
                    <i data-guid="<?php echo $entity->guid; ?>" data-type="video" class="fa fa-pencil edit_entity"></i>
                    <i data-guid="<?php echo $entity->guid; ?>" class="fa fa-trash delete_entity"></i>
                </div>
            <?php } ?>
            <i>
                <b>
                    <a href="<?php echo elgg_get_site_url() ?>profile/<?php echo $post_owner->username; ?>">
                        <?php echo $post_owner->name; ?>
                    </a>
                </b>
                <font style="color:#999">
                <?php echo $line ?>
                <br/>
                <?php echo time_stamp($entity->time_created); ?>
                </font>
            </i>
        </div>
        <div class="col-md-12" style="margin-top:10px;padding: 0px;">
            <div class="img-list">
                <img data-entity="<?php echo $entity->guid; ?>" class="video_options post_video_size" data-video="<?php echo $post['videoID']; ?>" src="<?php echo elgg_get_site_url() ?>mod/time_theme/lib/plugins/thumb/thumbs/<?php echo $post['videoID']; ?>-play.jpg" />
                <span class="text-content">
                    <span>
                        <i data-entity="<?php echo $entity->guid; ?>" class="fa fa-arrows-alt video_options" data-video="<?php echo $post['videoID']; ?>" style="padding:10px"></i>
                        <i data-entity="<?php echo $entity->guid; ?>" class="fa fa-edit video_options" data-video="<?php echo $post['videoID']; ?>"></i>
                        <i class="fa fa-star later" style="margin-top:-10px;margin-left: 18%;position: absolute;margin-top: -10px;">&nbsp;88</i>
                        <i class="fa fa-thumb-tack later" data-video="<?php echo $post['videoID']; ?>" style="padding: 8%;margin-left: 11%;position: absolute;">&nbsp;77</i>
                        <i class="fa fa-arrow-down later" data-video="<?php echo $post['videoID']; ?>" style="margin-top:10px;padding: 15%;margin-left: 3%;position: absolute;margin-top: 10px;">&nbsp;137</i>
                    </span>
                </span>
            </div>
            <div class="col-md-12">
                <div class="post_content">
                    <?php echo substr($post['text_content'], 0, 200); ?>..
                    <br/>
                    <br/>
                    <span class="location">
                        <?php echo $post['text_location'] ? $post['text_location'] . ' . ' : ''; ?><?php echo $post_location; ?>
                    </span>
                </div>
            </div>
        </div>
        <div class="col-md-12 options_row">
            <div class="col-md-3">
                <div class="pull-right">15</div>
                <div class="pull-left">
                    <i class="fa fa-share-alt"></i>
                </div>
            </div>
            <div class="col-md-3 like_post">
                <div class="pull-right post_like_count"><?php echo count($post_likes); ?></div>
                <div class="pull-left">
                    <i class="fa <?php echo in_array($logged_user->guid, $post_likes) ? 'fa-thumbs-up' : 'fa-thumbs-o-up'; ?>" data-owner_guid="<?php echo $entity->owner_guid; ?>" data-guid="<?php echo $entity->guid; ?>" data-post_like_count="<?php echo count($post_likes); ?>"></i>
                </div>
            </div>
            <div class="col-md-3 comment_post" data-guid="<?php echo $entity->guid; ?>">
                <div class="pull-right"><?php echo count($post_comments); ?></div>
                <div class="pull-left">
                    <i class="fa fa-comment"></i>
                </div>
            </div>
            <div class="col-md-3">
                <i class="fa fa-pencil"></i>
            </div>
        </div>
        <div class="col-md-12 comments_div">
            <div class="input-group" style="margin-bottom: 5px">
                <input type="text" class="form-control input-sm comment_post_data"/>
                <div data-guid="<?php echo $entity->guid; ?>" style="cursor: pointer" class="submit_post_comment input-group-addon black-green-button colors-color"><i class="fa fa-check-circle-o"></i></div>
            </div>
            <div class="comments_data" data-guid="<?php echo $entity->guid; ?>"></div>
        </div>
    </div>
</div>