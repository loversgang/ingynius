<?php
$line = 'created group <a href="' . elgg_get_site_url() . 'groups/profile/' . $entity->guid . '/' . str_replace(' ', '-', $entity->name) . '">' . $entity->name . '</a>';
?>
<div class="item" data-type="media">
    <div class="row item_content">
        <div class="col-md-2" style="width:65px">
            <img src="<?php echo $dp_src; ?>" class="img img-circle" style="height:35px"/>
        </div>
        <div class="col-md-8" style="margin-left: -20px;">
            <?php if ($entity->owner_guid == $logged_user->guid) { ?>
                <div class="pull-right actions_icon">
                    <i data-guid="<?php echo $entity->guid; ?>" class="fa fa-pencil edit_entity"></i>
                    <i data-guid="<?php echo $entity->guid; ?>" class="fa fa-trash delete_entity"></i>
                </div>
            <?php } ?>
            <i>
                <b>
                    <a href="<?php echo elgg_get_site_url() ?>profile/<?php echo $post_owner->username; ?>">
                        <?php echo $post_owner->name; ?>
                    </a>
                </b>
                <font style="color:#999">
                <?php echo $line ?>
                <br/>
                <?php echo time_stamp($entity->time_created); ?>
                </font>
            </i>
        </div>
        <div class="col-md-12" style="margin-top:10px;padding: 0px;">
            <div class="img-list">
                <img class="img post_photo_img img_options post_video_size" src="<?php echo $entity->getIconURL('master'); ?>" />
                <span class="text-content">
                    <span>
                        <i class="fa fa-arrows-alt  img_options" src="<?php echo $entity->getIconURL('master'); ?>" style="padding:10px"></i>
                        <i class="fa fa-edit  img_options" src="<?php echo $entity->getIconURL('master'); ?>"></i>
                        <i class="fa fa-star  later" style="margin-top:-10px;margin-left: 18%;position: absolute;margin-top: -10px;">&nbsp;88</i>
                        <i class="fa fa-thumb-tack  later" src="<?php echo $entity->getIconURL('master'); ?>" style="padding: 8%;margin-left: 11%;position: absolute;">&nbsp;77</i>
                        <i class="fa fa-arrow-down  later" src="<?php echo $entity->getIconURL('master'); ?>" style="margin-top:10px;padding: 15%;margin-left: 3%;position: absolute;margin-top: 10px;">&nbsp;137</i>
                    </span>
                </span>
            </div>
            <br/>
            <div class="post_content" style="padding:5px">
                <?php echo substr($entity->description, 0, 200); ?>..
            </div>
        </div>
    </div>
</div>
