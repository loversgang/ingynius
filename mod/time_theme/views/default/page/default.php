<?php
include_once elgg_get_plugins_path() . 'time_theme/functions.php';
include 'groups.php';
include 'members.php';
include 'files.php';
include 'videos.php';
include 'events.php';
include 'pages.php';
include 'header.php';
?>
<div class="col-md-12" style="top:70px;">
    <?php if (elgg_is_logged_in()) { ?>
        <?php include 'top.php'; ?>
        <?php include 'charmbar.php'; ?>
        <?php include 'content.php'; ?>
        <?php include 'bottom.php'; ?>
        <?php include 'modal.php'; ?>
    <?php } else { ?>
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
            <?php if (count($vars['sysmessages']['success']) > 0) { ?>
                <?php foreach ($vars['sysmessages']['success'] as $success) { ?>
                    <div class="alert alert-success" style="margin: 5px 15px"><?php echo $success; ?></div>
                <?php } ?>
            <?php } ?>
            <?php if (count($vars['sysmessages']['error']) > 0) { ?>
                <?php foreach ($vars['sysmessages']['error'] as $error) { ?>
                    <div class="alert alert-danger" style="margin: 5px 15px"><?php echo $error; ?></div>
                <?php } ?>
            <?php } ?>
            <?php echo $content; ?>
        </div>
        <div class="col-md-4">
            <?php include 'color_switcher.php'; ?>
        </div>
    <?php } ?>
</div>
<?php include 'footer.php'; ?>