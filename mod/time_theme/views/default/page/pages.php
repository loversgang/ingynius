<?php

// Get All Pages
$pages = elgg_get_entities(array(
    'type' => 'object',
    'subtype' => 'page_content',
    'limit' => FALSE,
        ));
// Get Latest Pages
$pages_latest = elgg_get_entities(array(
    'type' => 'object',
    'subtype' => 'page_content',
    'limit' => 10
        ));
// Get Featured Pages
$pages_featured = elgg_get_entities(array(
    'type' => 'object',
    'subtype' => 'page_content',
    'limit' => 2
        ));

// Get Popular Pages
$pages_popular = elgg_get_entities(array(
    'type' => 'object',
    'subtype' => 'page_content',
    'limit' => 3
        ));
