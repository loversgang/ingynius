<?php
// Get Circles/Cliques
$circles = elgg_get_entities(array(
    'type' => 'object',
    'subtype' => 'circle',
    'limit' => FALSE,
        ));
?>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Sample Post Title</h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer" style="border:none"></div>
        </div>
    </div>
</div>
<div class="modal fade" id="post_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" style="text-transform: capitalize" id="myModalLabel"></h4>
            </div>
            <div class="modal-body" style="height: auto;overflow: visible !important">
                <div id="post_content"></div>
                <div class="pac-container pac-logo hdpi"></div>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="circles_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 300px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><b>Add To Circles</b></h4>
            </div>
            <div class="modal-body" style="height: auto;overflow: visible !important">
                <div class="row-fluid">
                    <div class="col-md-12">
                        <form>
                            <?php
                            if ($logged_user->$circles_value) {
                                $user_circles = maybe_unserialize($logged_user->$circles_value);
                            }
                            ?>
                            <?php foreach ($circles as $circle) { ?>
                                <div>
                                    <input id="box<?php echo $circle->guid; ?>" type="checkbox" value="<?php echo $circle->guid; ?>" <?php echo in_array($user->guid, $user_circles[$circle->guid]) ? 'checked' : '' ?>>
                                    <label for="box<?php echo $circle->guid; ?>">
                                        <span><?php echo $circle->title; ?></span>
                                    </label>
                                </div>
                            <?php } ?>
                        </form>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <button type="button" id="user_add_circles" class="btn btn-primary">Add</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="groupModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer" style="border:none"></div>
        </div>
    </div>
</div>