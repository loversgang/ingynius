<?php

// Get Logged In User Details
$logged_user = elgg_get_logged_in_user_entity();

// Get All Groups
//'owner_guid' => $user_guid,
$groups = elgg_get_entities(array(
    'type' => 'object',
    'subtype' => 'group_content',
    'limit' => false,
        ));

// Latest Groups
$groups_latest = elgg_get_entities(array(
    'type' => 'object',
    'subtype' => 'group_content',
    'limit' => 10
        ));

// Get Featured Groups
$groups_featured = elgg_get_entities(array(
    'type' => 'object',
    'subtype' => 'group_content',
    'limit' => 2,
        ));

// Get Featured Groups
$groups_popular = elgg_get_entities(array(
    'type' => 'object',
    'subtype' => 'group_content',
    'limit' => 3,
        ));
