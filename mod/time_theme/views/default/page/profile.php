<?php
$user = elgg_get_page_owner_entity();
$friends = user_in_circles();
$owner_dp = 'user_dp_' . $user->guid;
$ia = elgg_set_ignore_access(true);
$dp_src = ($user->$owner_dp) ? elgg_get_site_url() . 'mod/time_theme/' . $user->$owner_dp : elgg_get_site_url() . '_graphics/icons/user/defaultlarge.gif';
elgg_set_ignore_access($ia);
?>
<div class="row-fluid" style="background: #fff">
    <div class="col-md-12" style="margin-top: 1px;padding: 0px">
        <div class="fb-profile">
            <img align="left" class="fb-image-lg" src="<?php echo $dp_src; ?>" alt="Profile image example">
            <span id="page_photo"><img class="fb-image-profile thumbnail" align="left" src="<?php echo $dp_src; ?>"></span>
            <div class="fb-profile-text" style="background-color: #fff;">
                <?php if ($logged_user->guid != $user->guid) { ?>
                    <div class="add_to_circles_button">
                        <?php if (in_array($user->guid, $friends)) { ?>
                            <a class="btn btn-default btn-xs" href="<?php echo elgg_get_site_url(); ?>ingynius_messages/compose/?user_id=<?php echo $user->guid; ?>"><i class="fa fa-comment"></i> Message</a>
                        <?php } ?>
                        <button class="btn btn-default btn-xs" id="add_to_circles" data-user_id="<?php echo $user->guid; ?>"><i class="fa fa-plus-circle"></i> Add</button>
                    </div>
                <?php } ?>
                <h1><span class="page_name"><?php echo $user->name; ?></span></h1>
            </div>
        </div>
    </div>
    <?php
    $entities = elgg_get_entities(array(
        'types' => array('object'),
        'subtypes' => array('text_content', 'photo_content', 'link_content', 'audio_content', 'video_content', 'event_content'),
        'limit' => FALSE,
        'owner_guid' => $user->guid,
    ));
    ?>
    <div class="col-md-12" style="padding: 0px">
        <div id="masonry-container" class="js-masonry" data-masonry-options='{ "columnWidth": ".item", "itemSelector": ".item", "transitionDuration": "1.2s" }' style="padding-right: 0px !important;">
            <div class="bhoechie-tab-content active">
                <div id="masonry-container" class="js-masonry" data-masonry-options='{ "columnWidth": ".item", "itemSelector": ".item", "transitionDuration": "1.2s" }' style="padding-right: 0px !important;">
                    <?php
                    foreach ($entities as $entity) {
                        $subtype = get_subtype_from_id($entity->subtype);
                        $post_owner = get_entity($entity->owner_guid);
                        $user_circles_list = array();
                        $user_circles_value = 'circles_' . $post_owner->guid;
                        if ($post_owner->$user_circles_value) {
                            $user_circles_list = maybe_unserialize($post_owner->$user_circles_value);
                        }
                        $post = maybe_unserialize($entity->description);
                        $collection = get_access_collection($entity->access_id);
                        $post_location = $collection->name;
                        $owner_dp = 'user_dp_' . $post_owner->guid;
                        $likes_value = 'post_likes_' . $entity->guid;
                        $comments_value = 'post_comments_' . $entity->guid;
                        $ia = elgg_set_ignore_access(true);
                        $post_likes = ($entity->$likes_value) ? maybe_unserialize($entity->$likes_value) : array();
                        $post_comments = ($entity->$comments_value) ? maybe_unserialize($entity->$comments_value) : array();
                        $dp_src = ($post_owner->$owner_dp) ? elgg_get_site_url() . 'mod/time_theme/' . $post_owner->$owner_dp : elgg_get_site_url() . '_graphics/icons/user/defaultlarge.gif';
                        elgg_set_ignore_access($ia);
                        if (!user_can_view($user_circles_list, $post['content_privacy'], $post_owner->guid)) {
                            continue;
                        }
                        if ($entity->type == 'group') {
                            include 'subtypes/group.php';
                        } elseif ($subtype == 'photo_content') {
                            $line = 'added new photo';
                            include 'subtypes/photo_content.php';
                        } elseif ($subtype == 'text_content') {
                            $line = 'updated status';
                            include 'subtypes/text_content.php';
                        } elseif ($subtype == 'audio_content') {
                            $line = 'shared audio track';
                            include 'subtypes/audio_content.php';
                        } elseif ($subtype == 'video_content') {
                            $line = 'shared video';
                            include 'subtypes/video_content.php';
                        } elseif ($subtype == 'link_content') {
                            $line = 'shared link';
                            include 'subtypes/link_content.php';
                        } elseif ($subtype == 'event_content') {
                            $line = 'created event';
                            include 'subtypes/event_content.php';
                        } else {
                            $line = 'Text';
                        }
                    }
                    ?>
                </div>
                <?php if (count($entities) > $postsLimit) { ?>
                    <div class="col-md-12">
                        <div class="main-pagination">
                            <div class="masonry_load_more colors-color">
                                <h3 id="load_more_text">Load More</h3>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>