<?php
$entities = elgg_get_entities(array(
    'types' => array('object'),
    'subtypes' => array('text_content', 'photo_content', 'link_content', 'audio_content', 'video_content', 'event_content'),
    'limit' => FALSE,
        ));
?>
<div class="col-md-2"></div>
<div class="col-md-8 post_box">
    <div class="row">
        <div>
            <div style="text-align: center;">
                <div class="col-md-2" id="post_text">
                    <i class="fa fa-font fa-2x colors-color-hover" style="color:#444"></i><br>
                    <span>Text</span>
                </div>
                <div class="col-md-2" id="post_photo">
                    <i class="fa fa-camera fa-2x colors-color-hover" style="color:#d95e40"></i><br>
                    <span>Photo</span>
                </div>
                <div class="col-md-2" id="post_link">
                    <i class="fa fa-link fa-2x colors-color-hover" style="color:#56bc8a"></i><br>
                    <span>Link</span>
                </div>
                <div class="col-md-2" id="post_audio">
                    <i class="fa fa-volume-up fa-2x colors-color-hover" style="color:#529ecc"></i><br>
                    <span>Audio</span>
                </div>
                <div class="col-md-2" id="post_video">
                    <i class="fa fa-film fa-2x colors-color-hover" style="color:#748089"></i><br>
                    <span>Video</span>
                </div>
                <div class="col-md-2" id="post_event">
                    <i class="fa fa-calendar fa-2x colors-color-hover" style="color:#f2992e"></i><br>
                    <span>Event</span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-2"></div>
<div class="clearfix"></div>
<div class="col-md-12" style="padding: 0">
    <div class="bhoechie-tab" style="min-height: 856px">
        <div class="bhoechie-tab-content active">
            <div id="masonry-container" class="js-masonry" data-masonry-options='{ "columnWidth": ".item", "itemSelector": ".item", "transitionDuration": "1.2s" }' style="padding-right: 0px !important;">
                <?php
                foreach ($entities as $entity) {
                    $subtype = get_subtype_from_id($entity->subtype);
                    $post_owner = get_entity($entity->owner_guid);
                    $user_circles_list = array();
                    $user_circles_value = 'circles_' . $post_owner->guid;
                    if ($post_owner->$user_circles_value) {
                        $user_circles_list = maybe_unserialize($post_owner->$user_circles_value);
                    }
                    $post = maybe_unserialize($entity->description);
                    $collection = get_access_collection($entity->access_id);
                    $post_location = $collection->name;
                    $owner_dp = 'user_dp_' . $post_owner->guid;
                    $likes_value = 'post_likes_' . $entity->guid;
                    $comments_value = 'post_comments_' . $entity->guid;
                    $ia = elgg_set_ignore_access(true);
                    $post_likes = ($entity->$likes_value) ? maybe_unserialize($entity->$likes_value) : array();
                    $post_comments = ($entity->$comments_value) ? maybe_unserialize($entity->$comments_value) : array();
                    $dp_src = ($post_owner->$owner_dp) ? elgg_get_site_url() . 'mod/time_theme/' . $post_owner->$owner_dp : elgg_get_site_url() . '_graphics/icons/user/defaultlarge.gif';
                    elgg_set_ignore_access($ia);
                    if (!user_can_view($user_circles_list, $post['content_privacy'], $post_owner->guid)) {
                        continue;
                    }
                    if ($entity->type == 'group') {
                        include 'subtypes/group.php';
                    } elseif ($subtype == 'photo_content') {
                        $line = 'added new photo';
                        include 'subtypes/photo_content.php';
                    } elseif ($subtype == 'text_content') {
                        $line = 'updated status';
                        include 'subtypes/text_content.php';
                    } elseif ($subtype == 'audio_content') {
                        $line = 'shared audio track';
                        include 'subtypes/audio_content.php';
                    } elseif ($subtype == 'video_content') {
                        $line = 'shared video';
                        include 'subtypes/video_content.php';
                    } elseif ($subtype == 'link_content') {
                        $line = 'shared link';
                        include 'subtypes/link_content.php';
                    } elseif ($subtype == 'event_content') {
                        $line = 'created event';
                        include 'subtypes/event_content.php';
                    } else {
                        $line = 'Text';
                    }
                }
                ?>
            </div>
            <?php if (count($entities) > $postsLimit) { ?>
                <div class="col-md-12">
                    <div class="main-pagination">
                        <div class="masonry_load_more colors-color">
                            <h3 id="load_more_text">Load More</h3>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="bhoechie-tab-content">
            <div id="masonry-container" class="js-masonry" data-masonry-options='{ "columnWidth": ".item", "itemSelector": ".item", "transitionDuration": "1.2s" }' style="padding-right: 0px !important;">
                <div class="only_mine_content"></div>
            </div>
        </div>
        <div class="bhoechie-tab-content">
            <div id="masonry-container" class="js-masonry" data-masonry-options='{ "columnWidth": ".item", "itemSelector": ".item", "transitionDuration": "1.2s" }' style="padding-right: 0px !important;">
                <div class="show_all_posts"></div>
            </div>
        </div>
        <div class="bhoechie-tab-content">
            <div id="masonry-container" class="js-masonry" data-masonry-options='{ "columnWidth": ".item", "itemSelector": ".item", "transitionDuration": "1.2s" }' style="padding-right: 0px !important;">
                <div class="default_content"></div>
            </div>
        </div>
        <div class="bhoechie-tab-content">
            <div id="masonry-container" class="js-masonry" data-masonry-options='{ "columnWidth": ".item", "itemSelector": ".item", "transitionDuration": "1.2s" }' style="padding-right: 0px !important;">
                <div class="default_content"></div>
            </div>
        </div>
        <div class="bhoechie-tab-content">
            <div id="masonry-container" class="js-masonry" data-masonry-options='{ "columnWidth": ".item", "itemSelector": ".item", "transitionDuration": "1.2s" }' style="padding-right: 0px !important;">
                <div class="default_content"></div>
            </div>
        </div>
        <div class="bhoechie-tab-content">
            <div id="masonry-container" class="js-masonry" data-masonry-options='{ "columnWidth": ".item", "itemSelector": ".item", "transitionDuration": "1.2s" }' style="padding-right: 0px !important;">
                <div class="default_content"></div>
            </div>
        </div>
        <div class="bhoechie-tab-content">
            <div id="masonry-container" class="js-masonry" data-masonry-options='{ "columnWidth": ".item", "itemSelector": ".item", "transitionDuration": "1.2s" }' style="padding-right: 0px !important;">
                <div class="all_groups_content"></div>
            </div>
        </div>
        <div class="bhoechie-tab-content">
            <div class="col-md-12" style="background-color: #fff;">
                <h2 class="page-header" style="border:none;margin: 5px 0px">
                    <span class="pull-right" style="margin-right: 15px;">
                        <a href="<?php echo elgg_get_site_url(); ?>company_pages/" class="btn btn-success btn-xs"><i class="fa fa-list"></i> My Pages</a>
                        <a href="<?php echo elgg_get_site_url(); ?>company_pages/create/" class="btn btn-success btn-xs"><i class="fa fa-plus"></i> Create Page</a>
                    </span>
                    Pages
                </h2>
            </div>
            <div class="clearfix"></div>
            <div id="masonry-container" class="js-masonry" data-masonry-options='{ "columnWidth": ".item", "itemSelector": ".item", "transitionDuration": "1.2s" }' style="padding-right: 0px !important;">
                <div class="all_pages_content"></div>
            </div>
        </div>
        <div class="bhoechie-tab-content">
            <div id="masonry-container" class="js-masonry" data-masonry-options='{ "columnWidth": ".item", "itemSelector": ".item", "transitionDuration": "1.2s" }' style="padding-right: 0px !important;">
                <div class="default_content"></div>
            </div>
        </div>
        <div class="bhoechie-tab-content">
            <div class="col-md-12" style="margin-bottom: 10px;">                
                <div class="col-md-1"></div>
                <div class="col-md-10" style="background-color:#000;padding: 0px">
                    <div class="col-md-12" style="padding:0px">
                        <div class="row item_content" style="background-color: #000;">
                            <div class="col-md-5" style="padding:0px">
                                <img class="img_options"  src="<?php echo $logged_user->getIconURL('large'); ?>" style="margin-top: -5px;height: 200px;" />
                                <h2 class="post_title_cl" style="color:#9CD759;margin-top: 30px;">
                                    <i class="fa fa-circle"></i> Online
                                </h2>
                            </div>
                            <div class="col-md-7" style="padding:5px">
                                <div class="post_user_detail">
                                    <h4 style="text-align: center" class="colors-color">
                                        <?php echo $logged_user->name; ?>
                                    </h4>
                                    <h4 style="text-align: center" class="colors-color">@<?php echo $logged_user->username; ?></h4>
                                    <p style="color:#fff;text-align: center">
                                        <i><?php echo $logged_user->briefdescription ?></i>
                                    </p>

                                    <div class="col-md-12">
                                        <h5 class="post_title_cl colors-color">
                                            About Me
                                        </h5>
                                    </div>
                                    <div class="col-md-12" style="margin-top: 10px;margin-bottom: 10px">
                                        <div class="col-md-6" style="text-align: center;">
                                            <div class="colors-color">Work:</div>
                                            <div class="colors-color">Live:</div>
                                            <div class="colors-color">Hometown:</div>
                                            <div class="colors-color">Birthday:</div>
                                        </div>
                                        <div class="col-md-6" style="text-align: center;color:#fff;">
                                            <div class="">XYZ Inc.</div>
                                            <div class=""><?php echo $logged_user->location; ?></div>
                                            <div class="">Foreign, EU</div>
                                            <div class=""><?php echo date('M', mktime(0, 0, 0, $user->birth_month, 1, 2000)); ?> <?php echo $user->birth_date; ?>th</div>
                                        </div>
                                        <div class="col-md-4" style="text-align: center;color:#fff;margin-top: 10px;">
                                            <div class="colors-color-hover">
                                                <a href="interests.php">
                                                    <i class="fa fa-user"></i><br/>
                                                    Interests
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-4" style="text-align: center;color:#fff;margin-top: 10px;">
                                            <div class="colors-color-hover">
                                                <a href="resume.php">
                                                    <i class="fa fa-list"></i><br/>
                                                    Resume
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-4" style="text-align: center;color:#fff;margin-top: 10px;">
                                            <div class="colors-color-hover">
                                                <a href="contact.php">
                                                    <i class="fa fa-paper-plane"></i><br/>
                                                    Contact
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-12" style="text-align: center;color:#fff;margin-top: 10px;">
                                            <button class="btn btn-default btn-xs">More</button>
                                        </div>
                                    </div>
                                    <div class="col-md-12 colors-color" style="padding: 0px;margin-left: -8px;">
                                        <div class="col-md-2">
                                            <i class="fa fa-inbox fa fa-2x"></i>
                                        </div>
                                        <div class="col-md-2">
                                            <i class="fa fa-cog  fa fa-2x"></i>
                                        </div>
                                        <div class="col-md-2">
                                            <i class="fa fa-plus  fa fa-2x"></i>
                                        </div>
                                        <div class="col-md-2">
                                            <i class="fa fa-eye fa fa-2x"></i>
                                        </div>
                                        <div class="col-md-2">
                                            <i class="fa fa-video-camera fa fa-2x"></i>
                                        </div>
                                        <div class="col-md-2">
                                            <i class="fa fa-gift fa fa-2x"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-1"></div>
            </div>
            <div class="clearfix"></div>
            <div id="masonry-container" class="js-masonry" data-masonry-options='{ "columnWidth": ".item", "itemSelector": ".item", "transitionDuration": "1.2s" }' style="padding-right: 0px !important;">
                <div class="show_online_members"></div>
            </div>
        </div>
        <div class="bhoechie-tab-content">
            <div id="masonry-container" class="js-masonry" data-masonry-options='{ "columnWidth": ".item", "itemSelector": ".item", "transitionDuration": "1.2s" }' style="padding-right: 0px !important;">
                <div class="get_settings_content"></div>
            </div>
        </div>
    </div>
</div>