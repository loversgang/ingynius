<?php

if (!function_exists('pr')) {

    function pr($e) {
        echo "<pre>";
        print_r($e);
        echo "</pre>";
    }

}
if (!function_exists('user_can_view')) {

    function user_can_view($owner_circles, $post_circles, $post_owner_id, $logged_id = 0) {
        if (!$logged_id) {
            $logged_user = elgg_get_logged_in_user_entity();
            $logged_id = $logged_user->guid;
        }
        $can_view = FALSE;
        foreach ($post_circles as $circle_id) {
            if (array_key_exists($circle_id, $owner_circles) && is_array($owner_circles[$circle_id]) && in_array($logged_id, $owner_circles[$circle_id])) {
                $can_view = TRUE;
                break;
            }
        }
        if ($post_owner_id == $logged_id) {
            return TRUE;
        }
        return $can_view;
    }

}
if (!function_exists('user_in_circles')) {

    function user_in_circles() {
        $logged_user = elgg_get_logged_in_user_entity();
        $logged_id = $logged_user->guid;
        $members = elgg_get_entities(array(
            'type' => 'user',
            'limit' => FALSE,
            'order_by' => 'last_action desc'
        ));
        $users_array = array();
        foreach ($members as $member) {
            $circles = 'circles_' . $member->guid;
            $user_circles = array();
            if ($member->$circles) {
                $user_circles = maybe_unserialize($member->$circles);
            }
            foreach ($user_circles as $circle) {
                if (in_array($logged_id, $circle)) {
                    $users_array[] = $member->guid;
                }
            }
        }
        return array_unique($users_array);
    }

}

function getGroupMembers($group_id) {
    $ia = elgg_set_ignore_access(true);
    $group_members = 'group_members_' . $group_id;
    $group = get_entity($group_id);
    $members = ($group->$group_members) ? maybe_unserialize($group->$group_members) : '';
    elgg_set_ignore_access($ia);
    return $members;
}

function getUserDp($user_id) {
    $user = get_entity($user_id);
    $owner_dp = 'user_dp_' . $user_id;
    $ia = elgg_set_ignore_access(true);
    $dp_src = ($user->$owner_dp) ? elgg_get_site_url() . 'mod/time_theme/' . $user->$owner_dp : elgg_get_site_url() . '_graphics/icons/user/defaultlarge.gif';
    elgg_set_ignore_access($ia);
    return $dp_src;
}

function getGroupDp($group_id) {
    $group = get_entity($group_id);
    $group_dp = 'group_dp_' . $group_id;
    $ia = elgg_set_ignore_access(true);
    $group_dp_src = ($group->$group_dp) ? elgg_get_site_url() . 'mod/time_theme/' . $group->$group_dp : elgg_get_site_url() . '_graphics/icons/default/large.png';
    elgg_set_ignore_access($ia);
    return $group_dp_src;
}

function getGroupCover($group_id) {
    $group = get_entity($group_id);
    $group_cover = 'group_cover_' . $group_id;
    $ia = elgg_set_ignore_access(true);
    $group_cover_src = ($group->$group_cover) ? elgg_get_site_url() . 'mod/time_theme/' . $group->$group_cover : elgg_get_site_url() . '_graphics/icons/default/large.png';
    elgg_set_ignore_access($ia);
    return $group_cover_src;
}

function getPostLikes($post_id) {
    $entity = get_entity($post_id);
    $likes_value = 'post_likes_' . $post_id;
    $ia = elgg_set_ignore_access(true);
    $post_likes = ($entity->$likes_value) ? maybe_unserialize($entity->$likes_value) : array();
    elgg_set_ignore_access($ia);
    return $post_likes;
}

function getPostComments($post_id) {
    $entity = get_entity($post_id);
    $comments_value = 'post_comments_' . $post_id;
    $ia = elgg_set_ignore_access(true);
    $post_comments = ($entity->$comments_value) ? maybe_unserialize($entity->$comments_value) : array();
    elgg_set_ignore_access($ia);
    return $post_comments;
}

function generateMsgNotifications($to_id) {
    $ia = elgg_set_ignore_access(true);
    $from = elgg_get_logged_in_user_entity();
    $to = get_entity($to_id);
    $notif_value = 'messages_notif_' . $to->guid . '_' . $from->guid;
    $msg_notifs = ($to->$notif_value) ? maybe_unserialize($to->$notif_value) : array();
    if ($to_id != $from->guid) {
        $msg_notifs[] = 'unread';
        $data = maybe_serialize($msg_notifs);
        create_metadata($to->guid, $notif_value, $data, '', $to->guid, ACCESS_PUBLIC);
        \Elgg\Database\MetastringsTable::_elgg_delete_orphaned_metastrings();
        $to->save();
    }
    elgg_set_ignore_access($ia);
}

function unsetMsgNotifications($from_id) {
    $ia = elgg_set_ignore_access(true);
    $to = elgg_get_logged_in_user_entity();
    $from = get_entity($from_id);
    $notif_value = 'messages_notif_' . $to->guid . '_' . $from->guid;
    $msg_notifs = array();
    $data = maybe_serialize($msg_notifs);
    create_metadata($to->guid, $notif_value, $data, '', $to->guid, ACCESS_PUBLIC);
    \Elgg\Database\MetastringsTable::_elgg_delete_orphaned_metastrings();
    $to->save();
    elgg_set_ignore_access($ia);
}

function generateNotifications($owner_guid, $post_id, $from_user, $type) {
    $ia = elgg_set_ignore_access(true);
    $owner = get_entity($owner_guid);
    $notif_value = 'notifications_' . $owner_guid;
    $post_notifs = ($owner->$notif_value) ? maybe_unserialize($owner->$notif_value) : array();
    if ($owner_guid != $from_user) {
        $post_notifs[] = array(
            'post_id' => $post_id,
            'from_id' => $from_user,
            'type' => $type,
            'time' => time(),
            'seen' => 0
        );
        $data = maybe_serialize($post_notifs);
        create_metadata($owner_guid, $notif_value, $data, '', $owner_guid, ACCESS_PUBLIC);
        \Elgg\Database\MetastringsTable::_elgg_delete_orphaned_metastrings();
        $owner->save();
    }
    elgg_set_ignore_access($ia);
}

function unsetNotifications($post_id, $from_user, $type, $time) {
    $ia = elgg_set_ignore_access(true);
    $owner = elgg_get_logged_in_user_entity();
    $notif_value = 'notifications_' . $owner->guid;
    $post_notifs = ($owner->$notif_value) ? maybe_unserialize($owner->$notif_value) : array();
    $key = multi_array_search($post_notifs, array('post_id' => $post_id, 'time' => $time));
    unset($post_notifs[$key[0]]);
    $post_notifs[] = array(
        'post_id' => $post_id,
        'from_id' => $from_user,
        'type' => $type,
        'time' => $time,
        'seen' => 1
    );
    $data = maybe_serialize($post_notifs);
    create_metadata($owner->guid, $notif_value, $data, '', $owner->guid, ACCESS_PUBLIC);
    \Elgg\Database\MetastringsTable::_elgg_delete_orphaned_metastrings();
    $owner->save();
    elgg_set_ignore_access($ia);
}

if (!function_exists('time_stamp')) {

    function time_stamp($session_time) {
        $time_difference = time() - $session_time;

        $seconds = $time_difference;
        $minutes = round($time_difference / 60);
        $hours = round($time_difference / 3600);
        $days = round($time_difference / 86400);
        $weeks = round($time_difference / 604800);
        $months = round($time_difference / 2419200);
        $years = round($time_difference / 29030400);
// Seconds
        if ($seconds <= 60) {
            echo "$seconds seconds ago";
        }
//Minutes
        else if ($minutes <= 60) {

            if ($minutes == 1) {
                echo "$minutes minute ago";
            } else {
                echo "$minutes minutes ago";
            }
        }
//Hours
        else if ($hours <= 24) {

            if ($hours == 1) {
                echo "$hours hour ago";
            } else {
                echo "$hours hours ago";
            }
        }
//Days
        else if ($days <= 7) {

            if ($days == 1) {
                echo "$days day ago";
            } else {
                echo "$days days ago";
            }
        }
//Weeks
        else if ($weeks <= 4) {

            if ($weeks == 1) {
                echo "$weeks week ago";
            } else {
                echo "$weeks weeks ago";
            }
        }
//Months
        else if ($months <= 12) {

            if ($months == 1) {
                echo "$months month ago";
            } else {
                echo "$months months ago";
            }
        }
//Years
        else {

            if ($years == 1) {
                echo "$years year ago";
            } else {
                echo "$years years ago";
            }
        }
    }

}
if (!function_exists('maybe_unserialize')) {

    function maybe_unserialize($original) {
        if (is_serialized($original))
            return @unserialize($original);
        return $original;
    }

}
if (!function_exists('maybe_serialize')) {

    function maybe_serialize($data) {
        if (is_array($data) || is_object($data))
            return serialize($data);
        if (is_serialized($data, false))
            return serialize($data);
        return $data;
    }

}
if (!function_exists('is_serialized')) {

    function is_serialized($data, $strict = true) {
        if (!is_string($data)) {
            return false;
        }
        $data = trim($data);
        if ('N;' == $data) {
            return true;
        }
        if (strlen($data) < 4) {
            return false;
        }
        if (':' !== $data[1]) {
            return false;
        }
        if ($strict) {
            $lastc = substr($data, -1);
            if (';' !== $lastc && '}' !== $lastc) {
                return false;
            }
        } else {
            $semicolon = strpos($data, ';');
            $brace = strpos($data, '}');
            if (false === $semicolon && false === $brace)
                return false;
            if (false !== $semicolon && $semicolon < 3)
                return false;
            if (false !== $brace && $brace < 4)
                return false;
        }
        $token = $data[0];
        switch ($token) {
            case 's' :
                if ($strict) {
                    if ('"' !== substr($data, -2, 1)) {
                        return false;
                    }
                } elseif (false === strpos($data, '"')) {
                    return false;
                }
            case 'a' :
            case 'O' :
                return (bool) preg_match("/^{$token}:[0-9]+:/s", $data);
            case 'b' :
            case 'i' :
            case 'd' :
                $end = $strict ? '$' : '';
                return (bool) preg_match("/^{$token}:[0-9.E-]+;$end/", $data);
        }
        return false;
    }

}

function multi_array_search($array, $search) {
    $result = array();
    foreach ($array as $key => $value) {
        foreach ($search as $k => $v) {
            if (!isset($value[$k]) || $value[$k] != $v) {
                continue 2;
            }
        }
        $result[] = $key;
    }
    return $result;
}
