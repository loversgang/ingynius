<div class="panel panel-info panel-login" style="border-radius: 5px;">
    <div class="panel-heading">
        <h3 class="panel-title">Settings</h3>
    </div>
    <div class="panel-body settings_options" style="border: none !important;margin: 0px 20px;">
        <div class="col-md-6">
            <ul class="list-group">
                <li class="list-group-item list-group-item-success"><i class="fa fa-lock"></i> <a href="<?php echo elgg_get_site_url(); ?>password/">Change Password</a></li>
                <li class="list-group-item list-group-item-success"><i class="fa fa-bell"></i> <a href="<?php echo elgg_get_site_url(); ?>ingynius_notif/">Notifications</a></li>
                <li class="list-group-item list-group-item-success"><i class="fa fa-envelope"></i> <a href="<?php echo elgg_get_site_url(); ?>ingynius_messages/">Messages</a></li>
            </ul>
        </div>
        <div class="col-md-6">
            <ul class="list-group">
                <li class="list-group-item list-group-item-success"><i class="fa fa-pencil"></i> <a href="<?php echo elgg_get_site_url(); ?>profile_wizard/">Edit Profile</a></li>
                <li class="list-group-item list-group-item-success"><i class="fa fa-group"></i> <a href="<?php echo elgg_get_site_url(); ?>ingynius_groups/">Groups</a></li>
                <li class="list-group-item list-group-item-success"><i class="fa fa-pagelines"></i> <a href="<?php echo elgg_get_site_url(); ?>company_pages/">Company Pages</a></li>
            </ul>
        </div>
    </div>
</div>