<?php

// Show All Posts
$entities = elgg_get_entities(array(
    'limit' => FALSE,
        ));
foreach ($entities as $entity) {
    $subtype = get_subtype_from_id($entity->subtype);
    if ($entity->type == 'group' || $subtype == 'file' || $subtype == 'text_content' || $subtype == 'link_content' || $subtype == 'audio_content' || $subtype == 'video_content') {
        $user_entity = elgg_get_entities(array(
            'type' => 'user',
            'guid' => $entity->owner_guid,
        ));
        $post_owner = $user_entity[0];
        $user_circles_list = array();
        $user_circles_value = 'circles_' . $post_owner->guid;
        if ($post_owner->$user_circles_value) {
            $user_circles_list = maybe_unserialize($post_owner->$user_circles_value);
        }
        $owner = get_user_entity_as_row($entity->owner_guid);
        if ($entity->type == 'group') {
            include 'subtypes/group.php';
        } elseif ($subtype == 'file') {
            include 'subtypes/file.php';
        } elseif ($subtype == 'text_content') {
            $post = maybe_unserialize($entity->description);
            $collection = get_access_collection($entity->access_id);
            $post_location = $collection->name;
            $line = 'updated status';
            if (!user_can_view($user_circles_list, $post['content_privacy'], $post_owner->guid)) {
                continue;
            }
            include 'subtypes/text_content.php';
        } elseif ($subtype == 'audio_content') {
            $post = maybe_unserialize($entity->description);
            $collection = get_access_collection($entity->access_id);
            $post_location = $collection->name;
            $line = 'shared audio track';
            if (!user_can_view($user_circles_list, $post['content_privacy'], $post_owner->guid)) {
                continue;
            }
            include 'subtypes/audio_content.php';
        } elseif ($subtype == 'video_content') {
            $post = maybe_unserialize($entity->description);
            $collection = get_access_collection($entity->access_id);
            $post_location = $collection->name;
            $line = 'shared video';
            if (!user_can_view($user_circles_list, $post['content_privacy'], $post_owner->guid)) {
                continue;
            }
            include 'subtypes/video_content.php';
        } elseif ($subtype == 'link_content') {
            $post = maybe_unserialize($entity->description);
            $collection = get_access_collection($entity->access_id);
            $post_location = $collection->name;
            $line = 'shared link';
            if (!user_can_view($user_circles_list, $post['content_privacy'], $post_owner->guid)) {
                continue;
            }
            include 'subtypes/link_content.php';
        } else {
            $line = 'Text';
        }
    } else {
        continue;
    }
}