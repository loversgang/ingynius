<?php
// Get Active Members
$active_members = find_active_users();
foreach ($active_members as $member) {
    ?>
    <div class="item" data-type="member">
        <div class="row item_content">
            <div class="col-md-12" style="padding:0px;margin-top: -5px">
                <img class="img_options post_video_size" data-video="XZ4X1wcZ1GE" src="<?php echo $member->getIconURL('large'); ?>" />
            </div>
            <div class="col-md-12 photo_comments">
                <div class="col-md-2 comments_col_width">
                    <img src="<?php echo getUserDp($member->guid) ?>" class="img img-circle" style="height:35px;margin-top: -30px;"/>
                </div>
                <div class="col-md-9 members_details">
                    <div class="post_user_detail">
                        <span class="p_bold"><?php echo $member->name; ?></span>
                        <span class="pull-right" style="color:#9CD759">
                            <i class="fa fa-circle"></i> Online
                        </span>
                        <br/>
                        <span class="p_bold">@<?php echo $member->username; ?></span><br/>
                        <span class="photo_content"><?php echo $member->briefdescription ? $member->briefdescription : '' ?></span><br />
                    </div>
                </div>
            </div>
            <div class="col-md-12" style="color:#FF7101;padding: 0px;margin-left: -8px;">
                <div class="col-md-2">
                    <i class="fa fa-inbox fa fa-2x"></i>
                </div>
                <div class="col-md-2">
                    <i class="fa fa-cog  fa fa-2x"></i>
                </div>
                <div class="col-md-2">
                    <i class="fa fa-plus  fa fa-2x"></i>
                </div>
                <div class="col-md-2">
                    <i class="fa fa-eye fa fa-2x"></i>
                </div>
                <div class="col-md-2">
                    <i class="fa fa-video-camera fa fa-2x"></i>
                </div>
                <div class="col-md-2">
                    <i class="fa fa-gift fa fa-2x"></i>
                </div>
            </div>
        </div>
    </div>
    <?php
}