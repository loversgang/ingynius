<?php

$entities = elgg_get_entities(array(
    'types' => array('object'),
    'subtypes' => array('text_content', 'photo_content', 'link_content', 'audio_content', 'video_content', 'event_content'),
    'limit' => FALSE,
        ));
foreach ($entities as $entity) {
    $subtype = get_subtype_from_id($entity->subtype);
    $post_owner = get_entity($entity->owner_guid);
    $user_circles_list = array();
    $user_circles_value = 'circles_' . $post_owner->guid;
    if ($post_owner->$user_circles_value) {
        $user_circles_list = maybe_unserialize($post_owner->$user_circles_value);
    }
    $post = maybe_unserialize($entity->description);
    $collection = get_access_collection($entity->access_id);
    $post_location = $collection->name;
    $post_likes = getPostLikes($entity->guid);
    $post_comments = getPostComments($entity->guid);
    $dp_src = getUserDp($entity->owner_guid);
    if (!user_can_view($user_circles_list, $post['content_privacy'], $post_owner->guid)) {
        continue;
    }
    if ($subtype == 'photo_content') {
        $line = 'added new photo';
        include elgg_get_plugins_path() . 'time_theme/views/default/page/subtypes/photo_content.php';
    } elseif ($subtype == 'text_content') {
        $line = 'updated status';
        include elgg_get_plugins_path() . 'time_theme/views/default/page/subtypes/text_content.php';
    } elseif ($subtype == 'audio_content') {
        $line = 'shared audio track';
        include elgg_get_plugins_path() . 'time_theme/views/default/page/subtypes/audio_content.php';
    } elseif ($subtype == 'video_content') {
        $line = 'shared video';
        include elgg_get_plugins_path() . 'time_theme/views/default/page/subtypes/video_content.php';
    } elseif ($subtype == 'link_content') {
        $line = 'shared link';
        include elgg_get_plugins_path() . 'time_theme/views/default/page/subtypes/link_content.php';
    } elseif ($subtype == 'event_content') {
        $line = 'created event';
        include elgg_get_plugins_path() . 'time_theme/views/default/page/subtypes/event_content.php';
    } else {
        $line = 'Text';
    }
}