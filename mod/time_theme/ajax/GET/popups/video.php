<div class="row-fluid">
    <div class="col-md-8">
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-12">
                <iframe src="https://www.youtube.com/embed/<?php echo $post['videoID']; ?>" style="width:100%;height: 360px;" class="border-radius"></iframe>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="col-md-2" style="padding: 0px;width:65px">
            <img src="<?php echo $dp_src; ?>" class="img img-circle" style="height:35px"/>
        </div>
        <div class="col-md-8" style="padding: 0px;margin-left: -20px;">
            <i>
                <b>
                    <a href="<?php echo elgg_get_site_url() ?>profile/<?php echo $post_owner->username; ?>">
                        <?php echo $post_owner->name; ?>
                    </a>
                </b>
                <br/>
                <font style="color:#999">
                <i class="fa fa-clock-o colors-color"></i> <?php echo time_stamp($entity->time_created); ?>
                <br/>
                <?php echo $post['text_location'] ? '<i class="fa fa-map-marker colors-color"></i> ' . $post['text_location'] . ' . ' : ''; ?><?php echo $post_location; ?>
                </font>
            </i>
        </div>
        <div class="col-md-12 comments_popup_div" style="max-height: 350px; overflow: auto;background-color: #fff;padding-top: 10px">
            <div class="input-group" style="margin-bottom: 5px">
                <input type="text" class="form-control input-sm comment_post_data_popup"/>
                <div data-guid="<?php echo $guid; ?>" style="cursor: pointer" class="submit_post_comment_popup input-group-addon black-green-button colors-color"><i class="fa fa-check-circle-o"></i></div>
            </div>
            <div class="comments_data" data-guid="<?php echo $guid; ?>"></div>
        </div>
    </div>
</div>