<div class="add_group_members_main">
    <?php
    $members = getGroupMembers($guid);
    $friends = user_in_circles();
    foreach ($friends as $user_id) {
        $user = get_entity($user_id);
        $owner_dp = 'user_dp_' . $user->guid;
        $ia = elgg_set_ignore_access(true);
        $dp_src = ($user->$owner_dp) ? elgg_get_site_url() . 'mod/time_theme/' . $user->$owner_dp : elgg_get_site_url() . '_graphics/icons/user/defaultlarge.gif';
        elgg_set_ignore_access($ia);
        ?>
        <div class="col-md-4">
            <table class="table table-bordered">
                <tr>
                    <td style="width: 25%">
                        <img src="<?php echo $dp_src; ?>" class="img" style="height:35px"/>
                    </td>
                    <td style="width: 50%">
                        <?php echo $user->name; ?>
                    </td>
                    <td style="width: 25%">
                        <input type="checkbox" value="<?php echo $user->guid; ?>" <?php echo (in_array($user->guid, $members)) ? 'checked' : ''; ?>/>
                    </td>
                </tr>
            </table>
        </div>
    <?php } ?>
    <div class="col-md-12">
        <hr/>
        <button type="button" class="btn btn-success btn-sm add_members_to_group pull-right" data-entity="<?php echo $guid; ?>">Add Members</button>
    </div>
</div>