<?php

$ia = elgg_set_ignore_access(true);
$data = is_array($members) ? maybe_serialize($members) : array();
$group_value = 'group_members_' . $group_id;
$entity = get_entity($group_id);
create_metadata($group_id, $group_value, $data, '', $group_id, ACCESS_PUBLIC);
\Elgg\Database\MetastringsTable::_elgg_delete_orphaned_metastrings();
$entity->save();
elgg_set_ignore_access($ia);
