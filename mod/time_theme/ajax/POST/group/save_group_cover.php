<?php

$ia = elgg_set_ignore_access(true);
$img_value = 'group_cover_' . $guid;
$entity = get_entity($guid);
create_metadata($guid, $img_value, $img_url, '', $guid, ACCESS_PUBLIC);
\Elgg\Database\MetastringsTable::_elgg_delete_orphaned_metastrings();
$entity->save();
echo "success";
elgg_set_ignore_access($ia);
