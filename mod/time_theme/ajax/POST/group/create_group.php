<?php

$data = new ElggObject();
$data->subtype = "group_content";
$data->title = 'group_content';
$data->description = maybe_serialize($group_data);
$data->access_id = $group_data['collection'] ? $group_data['collection'] : ACCESS_PUBLIC;
$data->owner_guid = elgg_get_logged_in_user_guid();
$saved = $data->save();
if ($saved) {
    echo "success";
}