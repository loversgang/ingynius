<?php

$targ_w = 165;
$targ_h = 170;
$src = $data_array['img_url'];
$img_r = imagecreatefromjpeg($src);
$dst_r = ImageCreateTrueColor($targ_w, $targ_h);
imagecopyresampled($dst_r, $img_r, 0, 0, $data_array['xchord'], $data_array['ychord'], $targ_w, $targ_h, $data_array['wchord'], $data_array['hchord']);
$path = elgg_get_plugins_path() . 'time_theme/files/pages/' . basename($src);
imagejpeg($dst_r, $path, 100);
imagedestroy($dst_r);
echo 'success';
