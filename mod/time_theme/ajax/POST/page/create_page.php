<?php

$page_data['img_url'] = basename($page_data['img_url']);
$data = new ElggObject();
$data->subtype = "page_content";
$data->title = 'page_content';
$data->description = maybe_serialize($page_data);
$data->access_id = $page_data['collection'] ? $page_data['collection'] : ACCESS_PUBLIC;
$data->owner_guid = elgg_get_logged_in_user_guid();
$saved = $data->save();
if ($saved) {
    echo "success";
}