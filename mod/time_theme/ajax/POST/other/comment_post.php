<?php

$ia = elgg_set_ignore_access(true);
$post_value = 'post_comments_' . $guid;
$entity = get_entity($guid);
$post_comments = ($entity->$post_value) ? maybe_unserialize($entity->$post_value) : array();
$data_array = array(
    'user_id' => $logged_user->guid,
    'comment' => $comment,
    'date_add' => time()
);
if (count($post_comments) > 0) {
    array_unshift($post_comments, $data_array);
} else {
    $post_comments[] = $data_array;
}
$data = maybe_serialize($post_comments);
create_metadata($guid, $post_value, $data, '', $guid, ACCESS_PUBLIC);
\Elgg\Database\MetastringsTable::_elgg_delete_orphaned_metastrings();
$entity->save();
generateNotifications($entity->owner_guid, $guid, $logged_user->guid, 'comment');
elgg_set_ignore_access($ia);
