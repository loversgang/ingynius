<?php

$circles_value = 'circles_' . $logged_user->guid;
$user_circles = ($logged_user->$circles_value) ? maybe_unserialize($logged_user->$circles_value) : array();
$users_array = array();
foreach ($circles as $circle) {
    if (array_key_exists($circle->guid, $user_circles)) {
        $users_array[$circle->guid] = $user_circles[$circle->guid];
    }
    if (in_array($circle->guid, $user_circles_array)) {
        if (!is_array($users_array[$circle->guid])) {
            $users_array[$circle->guid] = array();
        }
        if (!in_array($user_id, $users_array[$circle->guid])) {
            $users_array[$circle->guid][] = $user_id;
        }
    } else {
        if (in_array($user_id, $users_array[$circle->guid])) {
            unset($users_array[$circle->guid][array_search($user_id, $users_array[$circle->guid])]);
        } else {
            if (!$users_array[$circle->guid]) {
                $users_array[$circle->guid] = '';
            }
        }
    }
}
$data = maybe_serialize($users_array);
create_metadata($logged_user->guid, $circles_value, $data, '', $logged_user->guid, ACCESS_PUBLIC);
\Elgg\Database\MetastringsTable::_elgg_delete_orphaned_metastrings();

$logged_user->save();
system_message(elgg_echo('User Added To Circle!'));
