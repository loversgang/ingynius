<?php

$entity = get_entity($guid);
if (($entity) && ($entity->canEdit())) {
    if ($entity->delete()) {
        echo "success";
    }
}