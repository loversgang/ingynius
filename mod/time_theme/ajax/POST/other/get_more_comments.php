<?php if (count($post_comments) > 0) { ?>
    <?php
    $comments_array = array_slice($post_comments, $key, $limit);
    foreach ($comments_array as $comment) {
        $user = get_entity($comment['user_id']);
        $owner_dp = 'user_dp_' . $user->guid;
        $ia = elgg_set_ignore_access(true);
        $dp_src = ($user->$owner_dp) ? elgg_get_site_url() . 'mod/time_theme/' . $user->$owner_dp : elgg_get_site_url() . '_graphics/icons/user/defaultlarge.gif';
        elgg_set_ignore_access($ia);
        ?>
        <div class="col-md-12 photo_comments">
            <div class="col-md-2 comments_col_width" style="padding: 1px">
                <img src="<?php echo $dp_src; ?>" class="img img-circle" style="height:35px">
            </div>
            <div class="col-md-9" style="margin-left: -20px;padding: 1px">
                <div class="post_user_detail">
                    <span class="p_bold">
                        <a href="<?php echo elgg_get_site_url() ?>profile/<?php echo $user->username; ?>">
                            <?php echo $user->name; ?>
                        </a>
                    </span>
                    <br/>
                    <span class="photo_content">
                        <?php echo $comment['comment']; ?>
                    </span>
                    <br/>
                    <span class="comment_actions">
                        <?php echo time_stamp($comment['date_add']); ?>
                        <?php if ($logged_user->guid == $comment['user_id']) { ?>
                            . <i class="fa fa-trash delete_comment" data-guid="<?php echo $entity->guid; ?>" data-date_add="<?php echo $comment['date_add']; ?>"></i>
                        <?php } ?>
                    </span>
                    <br/>
                </div>
            </div>
        </div>
    <?php } ?>
<?php } ?>