<?php

$ia = elgg_set_ignore_access(true);
$post_value = 'post_comments_' . $guid;
$entity = get_entity($guid);
$post_comments = ($entity->$post_value) ? maybe_unserialize($entity->$post_value) : array();
if (($key = array_search($date_add, array_column($post_comments, 'date_add'))) !== false) {
    unset($post_comments[$key]);
}
$data = maybe_serialize(array_values($post_comments));
create_metadata($guid, $post_value, $data, '', $guid, ACCESS_PUBLIC);
\Elgg\Database\MetastringsTable::_elgg_delete_orphaned_metastrings();
$entity->save();
elgg_set_ignore_access($ia);
