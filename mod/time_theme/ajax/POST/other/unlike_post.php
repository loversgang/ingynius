<?php

$ia = elgg_set_ignore_access(true);
$post_value = 'post_likes_' . $guid;
$entity = get_entity($guid);
$post_likes = ($entity->$post_value) ? maybe_unserialize($entity->$post_value) : array();
if (($key = array_search($logged_user->guid, $post_likes)) !== false) {
    unset($post_likes[$key]);
}
$data = maybe_serialize($post_likes);
create_metadata($guid, $post_value, $data, '', $guid, ACCESS_PUBLIC);
\Elgg\Database\MetastringsTable::_elgg_delete_orphaned_metastrings();
$entity->save();
elgg_set_ignore_access($ia);
