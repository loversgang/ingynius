<?php

ob_start();
include 'conversations.php';
$conversations = ob_get_clean();
$data['conversations'] = $conversations;
if ($data_array['guid']) {
    $guid = $data_array['guid'];
    ob_start();
    include 'get_messages.php';
    $messages = ob_get_clean();
    $data['messages'] = $messages;
}
if (count($data_array['guids'] > 0)) {
    foreach ($data_array['guids'] as $uid) {
        $guid = $uid;
        ob_start();
        include 'getChat.php';
        $data['msg'][$guid] = ob_get_clean();
    }
}
echo json_encode($data);
