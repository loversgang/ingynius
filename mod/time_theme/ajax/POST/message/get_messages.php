<?php
unsetMsgNotifications($guid);
$messages = elgg_get_entities(array(
    'type' => 'object',
    'subtypes' => array('message_to_' . elgg_get_logged_in_user_guid() . '_' . $guid, 'message_to_' . $guid . '_' . elgg_get_logged_in_user_guid()),
    'limit' => FALSE,
    'order_by' => 'time_created desc'
        ));
?>
<div class="conversation_div">
    <?php
    foreach ($messages as $entity) {
        $user = get_entity($entity->owner_guid);
        $owner_dp = 'user_dp_' . $user->guid;
        $ia = elgg_set_ignore_access(true);
        $dp_src = ($user->$owner_dp) ? elgg_get_site_url() . 'mod/time_theme/' . $user->$owner_dp : elgg_get_site_url() . '_graphics/icons/user/defaultlarge.gif';
        elgg_set_ignore_access($ia);
        $message = maybe_unserialize($entity->description);
        ?>
        <table class="table table-bordered" style="border: none;margin-bottom: 0px">
            <td style="border:none;width: 50px">
                <img src="<?php echo $dp_src; ?>" class="img" style="height: 35px;width: 35px"/>
            </td>
            <td style="border:none;">
                <a href="<?php echo elgg_get_site_url() ?>profile/<?php echo $user->username; ?>">
                    <b><?php echo $user->name; ?></b>
                </a>
                <br/>
                <?php echo $message['content']; ?>
                <br/>
                <?php echo time_stamp($entity->time_created); ?>
            </td>
        </table>
        <hr class="messages"/>
    <?php } ?>
</div>