<?php
unsetMsgNotifications($guid);
$messages = elgg_get_entities(array(
    'type' => 'object',
    'subtypes' => array('message_to_' . elgg_get_logged_in_user_guid() . '_' . $guid, 'message_to_' . $guid . '_' . elgg_get_logged_in_user_guid()),
    'limit' => FALSE,
    'order_by' => 'time_created asc'
        ));
?>
<div class="conversation_div">
    <?php
    foreach ($messages as $entity) {
        $user = get_entity($entity->owner_guid);
        $owner_dp = 'user_dp_' . $user->guid;
        $ia = elgg_set_ignore_access(true);
        $dp_src = ($user->$owner_dp) ? elgg_get_site_url() . 'mod/time_theme/' . $user->$owner_dp : elgg_get_site_url() . '_graphics/icons/user/defaultlarge.gif';
        elgg_set_ignore_access($ia);
        $message = maybe_unserialize($entity->description);
        ?>
        <table class="table table-bordered" style="border: none;margin-bottom: 0px">
            <?php if ($user->guid != $logged_user->guid) { ?>
                <td style="border:none;width: 40px;padding: 5px 8px;">
                    <a href="<?php echo elgg_get_site_url() ?>profile/<?php echo $user->username; ?>">
                        <img src="<?php echo $dp_src; ?>" class="img img-circle" style="height: 25px;"/>
                    </a>
                </td>
                <td style="border:none;padding: 8px 0px;">
                    <div>
                        <span data-toggle="tooltip" class="from_msg" title="<?php echo time_stamp($entity->time_created); ?>">
                            <?php echo $message['content']; ?>
                        </span>
                    </div>
                </td>
            <?php } ?>
            <?php if ($user->guid == $logged_user->guid) { ?>
                <td style="border:none;width: 50px">
                    <div class="pull-right" style="margin: 5px;">
                        <span data-toggle="tooltip" class="my_chat_msg" title="<?php echo time_stamp($entity->time_created); ?>">
                            <?php echo $message['content']; ?>
                        </span>
                    </div>
                </td>
            <?php } ?>
        </table>
    <?php } ?>
</div>