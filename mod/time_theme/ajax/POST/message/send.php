<?php

$data_to = new ElggObject();
$data_from = new ElggObject();
$data_to->subtype = "message_to_" . $data_array['to_id'] . "_" . $logged_user->guid;
$data_from->subtype = "message_from_" . $logged_user->guid . "_" . $data_array['to_id'];
$data_to->title = "message_to_" . $data_array['to_id'] . "_" . $logged_user->guid;
$data_from->title = "message_from_" . $logged_user->guid . "_" . $data_array['to_id'];
generateMsgNotifications($data_array['to_id']);
unset($data_array['to_id']);
$data_to->description = maybe_serialize($data_array);
$data_from->description = maybe_serialize($data_array);
$data_to->access_id = ACCESS_PUBLIC;
$data_from->access_id = ACCESS_PUBLIC;
$data_to->owner_guid = elgg_get_logged_in_user_guid();
$data_from->owner_guid = elgg_get_logged_in_user_guid();
$data_to->save();
$data_from->save();
echo "success";
