<?php
include_once elgg_get_plugins_path() . 'time_theme/functions.php';
$logged_user = elgg_get_logged_in_user_entity();
$friends = user_in_circles();
$msg_count = 0;
foreach ($friends as $user_id) {
    $messages = elgg_get_entities(array(
        'type' => 'object',
        'subtypes' => array('message_to_' . elgg_get_logged_in_user_guid() . '_' . $user_id, 'message_to_' . $user_id . '_' . elgg_get_logged_in_user_guid()),
        'limit' => 1,
        'order_by' => 'time_created desc'
    ));
    if ($messages) {
        $msg_count += 1;
    }
}
?>
<table class="table table-bordered">
    <tbody>
        <?php
        $sort_array = array();
        foreach ($friends as $user_id) {
            $messages = elgg_get_entities(array(
                'type' => 'object',
                'subtypes' => array('message_to_' . elgg_get_logged_in_user_guid() . '_' . $user_id, 'message_to_' . $user_id . '_' . elgg_get_logged_in_user_guid()),
                'limit' => 1,
                'order_by' => 'time_created desc'
            ));
            if ($messages) {
                $entity = $messages[0];
                $sort_array[$user_id] = $entity->time_created;
            }
        }

        function cmp($a, $b) {
            return $b - $a;
        }

        uasort($sort_array, "cmp");
        foreach ($sort_array as $user_id => $timestamp) {
            $notif_value = 'messages_notif_' . $logged_user->guid . '_' . $user_id;
            $notifications = ($logged_user->$notif_value) ? maybe_unserialize($logged_user->$notif_value) : array();
            $messages = elgg_get_entities(array(
                'type' => 'object',
                'subtypes' => array('message_to_' . elgg_get_logged_in_user_guid() . '_' . $user_id, 'message_to_' . $user_id . '_' . elgg_get_logged_in_user_guid()),
                'limit' => 1,
                'order_by' => 'time_created desc'
            ));
            if ($messages) {
                $user = get_entity($user_id);
                $owner_dp = 'user_dp_' . $user->guid;
                $ia = elgg_set_ignore_access(true);
                $dp_src = ($user->$owner_dp) ? elgg_get_site_url() . 'mod/time_theme/' . $user->$owner_dp : elgg_get_site_url() . '_graphics/icons/user/defaultlarge.gif';
                elgg_set_ignore_access($ia);
                $entity = $messages[0];
                $message = maybe_unserialize($entity->description);
                ?>
                <tr>
                    <td data-guid="<?php echo $user_id; ?>" class="start_conversation">
                        <table class="table table-bordered" style="border: none;margin-bottom: 0px">
                            <tbody>
                                <tr>
                                    <td style="border:none;width: 50px">
                                        <img src="<?php echo $dp_src; ?>" class="img" style="height: 35px;width: 35px"/>
                                    </td>
                                    <td style="border:none;">
                                        <b class="username"><?php echo $user->name; ?></b><br/>
                                        <?php echo substr($message['content'], 0, 15); ?>..<br/>
                                        <span style="font-size: 10px">
                                            <?php echo time_stamp($entity->time_created); ?>
                                        </span>
                                    </td>
                                    <?php if (count($notifications) > 0) { ?>
                                        <td style="border:none;">
                                            <span class="count_color"><?php echo count($notifications); ?></span>
                                        </td>
                                    <?php } ?>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <?php
            } else {
                continue;
            }
            ?>
        <?php } ?>
    </tbody>
</table>