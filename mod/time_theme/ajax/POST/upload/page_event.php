<?php

$data = new ElggObject();
$data->subtype = "event_content_" . $data_array['page_id'];
$data->title = "event_content_" . $data_array['page_id'];
unset($data_array['page_id']);
$data->description = maybe_serialize($data_array);
$data->access_id = $data_array['collection'] ? $data_array['collection'] : ACCESS_PUBLIC;
$data->owner_guid = elgg_get_logged_in_user_guid();
$saved = $data->save();
if ($saved) {
    echo "success";
}