<div class="row-fluid" id="get_post_photo_html">
    <div id="targetLayer"></div>
    <form id="uploadForm" method="post">
        <div class="col-md-12">
            <center>
                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;">
                    </div>
                    <div>
                        <span class="btn btn-default btn-file">
                            <span class="fileinput-new">Select image</span>
                            <span class="fileinput-exists">Change</span>
                            <input type="file" name="userImage" class="inputFile">
                        </span>
                        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                    </div>
                </div>
            </center>
        </div>
        <div class="col-md-12" style="margin-top:10px">
            <div class="form-group">
                <div class="input-group">
                    <textarea id="text_content" class="form-control border-radius" rows="2" placeholder="Share something.."></textarea>
                    <div style="cursor:pointer" class="input-group-addon border-radius black-green-button colors-color" id="get_location_html"><i class="fa fa-map-marker"></i></div>
                </div>
            </div>
        </div>
        <div class="col-md-12" style="margin-top:10px">
            <div id="location_input" style="display: none">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon border-radius  black-green-button colors-color"><i class="fa fa-map-marker"></i></div>
                        <input id="text_location" placeholder="Type Location" type="text" class="form-control border-radius" />
                    </div>
                </div>
                <div id="location_html" style="width: 100%;height: 250px">
                    <center><img src="images/splash-screen.png"/></center>
                </div>
            </div>
        </div>
        <?php if ($collections) { ?>
            <div class="col-md-12">
                <div class="form-group">
                    <label name="select_collection">Add To Collection</label>
                    <div class="input-group">
                        <div class="input-group-addon border-radius black-green-button colors-color"><i class="fa fa-database"></i></div>
                        <select id="collection" class="form-control border-radius" data-placeholder="Choose Circle" style="width: 100% !important">
                            <?php foreach ($collections as $collection) { ?>
                                <option value="<?php echo $collection->id; ?>"><?php echo $collection->name; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
        <?php } ?>
        <div class="col-md-12" style="margin-top:10px">
            <div class="form-group">
                <label class="control-label">Privacy</label>
                <div class="input-group">
                    <div class="input-group-addon border-radius black-green-button colors-color"><i class="fa fa-share-alt"></i></div>
                    <select id="content_privacy" class="chosen form-control border-radius" multiple="true" data-placeholder="Choose Circle" style="width: 100% !important">
                        <?php foreach ($circles as $circle) { ?>
                            <option value="<?php echo $circle->guid ?>"><?php echo $circle->title ?></option>
                        <?php } ?>
                    </select>
                    <div style="cursor: pointer" class="submit_photo_content input-group-addon border-radius border-radius black-green-button colors-color">POST</div>
                </div>
            </div>
        </div>
    </form>
</div>