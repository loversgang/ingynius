<div class="row-fluid" id="get_post_link_html">
    <form class="form-inline">
        <div class="col-md-12" style="margin-top:10px">
            <div class="form-group">
                <label class="control-label">Grab Link Info</label>
                <div class="input-group">
                    <input id="url" placeholder="Enter URL" value="<?php echo $post['url']; ?>" type="text" class="form-control border-radius" />
                    <div id="get_url_info" class="input-group-addon border-radius black-green-button colors-color" style="cursor: pointer">Fetch</div>
                </div>
            </div>
        </div>
        <div class="col-md-12" style="margin-top:10px">
            <div id="output_link_details">
                <?php if ($post['image_url']) { ?>
                    <a href="<?php echo $post['url']; ?>">
                        <div class="img-list">
                            <img class="img img-responsive" src="<?php echo $post['image_url']; ?>" />
                        </div>
                    </a>
                <?php } ?>
                <?php if ($post['url_content']) { ?>
                    <span class="url_content">
                        <?php echo $post['url_content']; ?>
                    </span>
                <?php } ?>
            </div>
        </div>
        <div class="col-md-12" style="margin-top:10px">
            <div class="form-group">
                <div class="input-group">
                    <textarea id="text_content" class="form-control border-radius border-radius" rows="2" placeholder="Share something.."><?php echo $post['text_content']; ?></textarea>
                    <div style="cursor:pointer" class="input-group-addon border-radius black-green-button colors-color" id="get_location_html"><i class="fa fa-map-marker"></i></div>
                </div>
            </div>
        </div>
        <div class="col-md-12" style="margin-top:10px">
            <div id="location_input" style="display: none">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon border-radius black-green-button colors-color colors-color"><i class="fa fa-map-marker"></i></div>
                        <input id="text_location" placeholder="Type Location" type="text" class="form-control border-radius" value="<?php echo $post['text_location']; ?>" />
                    </div>
                </div>
                <div id="location_html" style="width: 100%;height: 250px">
                    <center><img src="images/splash-screen.png"/></center>
                </div>
            </div>
        </div>
        <?php if ($collections) { ?>
            <div class="col-md-12">
                <div class="form-group">
                    <label name="select_collection">Add To Collection</label>
                    <div class="input-group">
                        <div class="input-group-addon border-radius black-green-button colors-color"><i class="fa fa-database"></i></div>
                        <select id="collection" class="form-control border-radius" data-placeholder="Choose Circle" style="width: 100% !important">
                            <?php foreach ($collections as $collection) { ?>
                                <option value="<?php echo $collection->id; ?>" <?php echo $collection->id == $entity->access_id ? 'selected' : '' ?>><?php echo $collection->name; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
        <?php } ?>
        <div class="col-md-12" style="margin-top:10px">
            <div class="form-group">
                <label class="control-label">Privacy</label>
                <div class="input-group">
                    <div class="input-group-addon border-radius black-green-button colors-color"><i class="fa fa-share-alt"></i></div>
                    <select id="content_privacy" class="chosen form-control border-radius" multiple="true" data-placeholder="Choose Circle" style="width: 100% !important">
                        <?php foreach ($circles as $circle) { ?>
                            <option value="<?php echo $circle->guid ?>" <?php echo in_array($circle->guid, $post['content_privacy']) ? 'selected' : '' ?>><?php echo $circle->title ?></option>
                        <?php } ?>
                    </select>
                    <div data-guid="<?php echo $guid; ?>" style="cursor: pointer" class="submit_edit_link_content input-group-addon border-radius border-radius black-green-button colors-color">POST</div>
                </div>
            </div>
        </div>
    </form>
</div>