<?php

if (is_array($_FILES['userImage']) && $_FILES['userImage']['error'] == 0) {
    if (is_uploaded_file($_FILES['userImage']['tmp_name'])) {
        $sourcePath = $_FILES['userImage']['tmp_name'];
        $targetPath = "files/" . time() . $_FILES['userImage']['name'];
        if (!is_dir('files/')) {
            mkdir('files/', 0777, true);
        }
        if (move_uploaded_file($sourcePath, $targetPath)) {
            echo $targetPath;
        }
    }
}
if (is_array($_FILES['groupDp']) && $_FILES['groupDp']['error'] == 0) {
    $groups = array();
    if (is_uploaded_file($_FILES['groupDp']['tmp_name'])) {
        $sourcePath = $_FILES['groupDp']['tmp_name'];
        $targetPath = "files/group/dp/" . time() . $_FILES['groupDp']['name'];
        if (!is_dir('files/group/dp/')) {
            mkdir('files/group/dp/', 0777, true);
        }
        if (move_uploaded_file($sourcePath, $targetPath)) {
            $groups['type'] = 'dp';
            $groups['img_url'] = $targetPath;
            echo json_encode($groups);
        }
    }
}
if (is_array($_FILES['groupCover']) && $_FILES['groupCover']['error'] == 0) {
    $groups = array();
    if (is_uploaded_file($_FILES['groupCover']['tmp_name'])) {
        $sourcePath = $_FILES['groupCover']['tmp_name'];
        $targetPath = "files/group/cover/" . time() . $_FILES['groupCover']['name'];
        if (!is_dir('files/group/cover/')) {
            mkdir('files/group/cover/', 0777, true);
        }
        if (move_uploaded_file($sourcePath, $targetPath)) {
            $groups['type'] = 'cover';
            $groups['img_url'] = $targetPath;
            echo json_encode($groups);
        }
    }
}