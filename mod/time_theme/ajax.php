<?php

include dirname(dirname(__DIR__)) . "/engine/start.php";
include 'functions.php';
$logged_user = elgg_get_logged_in_user_entity();
$circles_value = 'circles_' . $logged_user->guid;
$user_dp = 'user_dp_' . $logged_user->guid;
$postsLimit = 12;
// Get Default Collections
$default_collections = get_default_access();

// Get Logged User Collections
$collections = get_user_access_collections($logged_user->guid);

// Get Circles/Cliques
$circles = elgg_get_entities(array(
    'type' => 'object',
    'subtype' => 'circle',
    'limit' => FALSE,
        ));

// GET DATA USING AJAX
if (isset($_GET['action']) && $_GET['action']) {
    extract($_GET);
    if ($action === 'get_all_posts_data') {
        include GET_AJAX . 'posts/all_posts_data.php';
    }
    if ($action === 'more_home_posts') {
        include GET_AJAX . 'posts/more_home_posts.php';
    }
    if ($action === 'only_mine_posts') {
        include GET_AJAX . 'posts/only_mine_posts.php';
    }
    if ($action === 'get_all_groups') {
        include GET_AJAX . 'posts/get_all_groups.php';
    }
    if ($action === 'get_all_pages') {
        include GET_AJAX . 'posts/get_all_pages.php';
    }
    if ($action === 'get_online_members') {
        include GET_AJAX . 'posts/get_online_members.php';
    }
    if ($action === 'get_settings_content') {
        include GET_AJAX . 'other/settings.php';
    }
    if ($action === 'get_photo_modal') {
        $entity = get_entity($guid);
        $post_owner = get_entity($entity->owner_guid);
        $post = maybe_unserialize($entity->description);
        $collection = get_access_collection($entity->access_id);
        $post_location = $collection->name;
        $owner_dp = 'user_dp_' . $post_owner->guid;
        $likes_value = 'post_likes_' . $entity->guid;
        $comments_value = 'post_comments_' . $entity->guid;
        $ia = elgg_set_ignore_access(true);
        $post_likes = ($entity->$likes_value) ? maybe_unserialize($entity->$likes_value) : array();
        $post_comments = ($entity->$comments_value) ? maybe_unserialize($entity->$comments_value) : array();
        $dp_src = ($post_owner->$owner_dp) ? elgg_get_site_url() . 'mod/time_theme/' . $post_owner->$owner_dp : elgg_get_site_url() . '_graphics/icons/user/defaultlarge.gif';
        elgg_set_ignore_access($ia);
        include GET_AJAX . 'popups/photo.php';
    }
    if ($action === 'get_video_modal') {
        $entity = get_entity($guid);
        $post_owner = get_entity($entity->owner_guid);
        $post = maybe_unserialize($entity->description);
        $collection = get_access_collection($entity->access_id);
        $post_location = $collection->name;
        $owner_dp = 'user_dp_' . $post_owner->guid;
        $likes_value = 'post_likes_' . $entity->guid;
        $comments_value = 'post_comments_' . $entity->guid;
        $ia = elgg_set_ignore_access(true);
        $post_likes = ($entity->$likes_value) ? maybe_unserialize($entity->$likes_value) : array();
        $post_comments = ($entity->$comments_value) ? maybe_unserialize($entity->$comments_value) : array();
        $dp_src = ($post_owner->$owner_dp) ? elgg_get_site_url() . 'mod/time_theme/' . $post_owner->$owner_dp : elgg_get_site_url() . '_graphics/icons/user/defaultlarge.gif';
        elgg_set_ignore_access($ia);
        include GET_AJAX . 'popups/video.php';
    }
    if ($action == 'get_add_members_html') {
        include GET_AJAX . 'popups/add_group_members.php';
    }
}
// POST DATA USING AJAX
if (isset($_POST['action']) && $_POST['action']) {
    extract($_POST);

    // GET POST MODALS
    if ($action == 'get_post_text_html') {
        include POST_AJAX . 'posttype/get_post_text_html.php';
    }
    if ($action == 'get_post_photo_html') {
        include POST_AJAX . 'posttype/get_post_photo_html.php';
    }
    if ($action == 'get_post_link_html') {
        include POST_AJAX . 'posttype/get_post_link_html.php';
    }
    if ($action == 'get_post_audio_html') {
        include POST_AJAX . 'posttype/get_post_audio_html.php';
    }
    if ($action == 'get_post_video_html') {
        include POST_AJAX . 'posttype/get_post_video_html.php';
    }
    if ($action == 'get_post_event_html') {
        include POST_AJAX . 'posttype/get_post_event_html.php';
    }
    // GET PAGE POST MODALS
    if ($action == 'get_page_text_html') {
        include POST_AJAX . 'pagepost/get_page_text_html.php';
    }
    if ($action == 'get_page_photo_html') {
        include POST_AJAX . 'pagepost/get_page_photo_html.php';
    }
    if ($action == 'get_page_link_html') {
        include POST_AJAX . 'pagepost/get_page_link_html.php';
    }
    if ($action == 'get_page_audio_html') {
        include POST_AJAX . 'pagepost/get_page_audio_html.php';
    }
    if ($action == 'get_page_video_html') {
        include POST_AJAX . 'pagepost/get_page_video_html.php';
    }
    if ($action == 'get_page_event_html') {
        include POST_AJAX . 'pagepost/get_page_event_html.php';
    }
    if ($action == 'get_post_edit_text_html') {
        $entity = get_entity($guid);
        $post = maybe_unserialize($entity->description);
        include POST_AJAX . 'posttype/get_post_edit_text_html.php';
    }
    if ($action == 'get_post_edit_photo_html') {
        $entity = get_entity($guid);
        $post = maybe_unserialize($entity->description);
        include POST_AJAX . 'posttype/get_post_edit_photo_html.php';
    }
    if ($action == 'get_post_edit_link_html') {
        $entity = get_entity($guid);
        $post = maybe_unserialize($entity->description);
        include POST_AJAX . 'posttype/get_post_edit_link_html.php';
    }
    if ($action == 'get_post_edit_audio_html') {
        $entity = get_entity($guid);
        $post = maybe_unserialize($entity->description);
        include POST_AJAX . 'posttype/get_post_edit_audio_html.php';
    }
    if ($action == 'get_post_edit_video_html') {
        $entity = get_entity($guid);
        $post = maybe_unserialize($entity->description);
        include POST_AJAX . 'posttype/get_post_edit_video_html.php';
    }
    if ($action == 'get_post_edit_event_html') {
        $entity = get_entity($guid);
        $post = maybe_unserialize($entity->description);
        include POST_AJAX . 'posttype/get_post_edit_event_html.php';
    }

    // SAVE POST DATA
    if ($action == 'upload_status') {
        include POST_AJAX . 'upload/text.php';
    }
    if ($action == 'upload_photo') {
        include POST_AJAX . 'upload/photo.php';
    }
    if ($action == 'upload_link') {
        include POST_AJAX . 'upload/link.php';
    }
    if ($action == 'upload_audio') {
        include POST_AJAX . 'upload/audio.php';
    }
    if ($action == 'upload_video') {
        include POST_AJAX . 'upload/video.php';
    }
    if ($action == 'upload_event') {
        include POST_AJAX . 'upload/event.php';
    }
    // SAVE POST DATA
    if ($action == 'upload_page_status') {
        include POST_AJAX . 'upload/page_text.php';
    }
    if ($action == 'upload_page_photo') {
        include POST_AJAX . 'upload/page_photo.php';
    }
    if ($action == 'upload_page_link') {
        include POST_AJAX . 'upload/page_link.php';
    }
    if ($action == 'upload_page_audio') {
        include POST_AJAX . 'upload/page_audio.php';
    }
    if ($action == 'upload_page_video') {
        include POST_AJAX . 'upload/page_video.php';
    }
    if ($action == 'upload_page_event') {
        include POST_AJAX . 'upload/page_event.php';
    }

    // EDIT POST DATA
    if ($action == 'edit_post') {
        include POST_AJAX . 'other/update.php';
    }

    // Get Other Data
    if ($action == 'save_user_dp') {
        include POST_AJAX . 'other/save_dp.php';
    }
    if ($action == 'extract_url') {
        include POST_AJAX . 'other/extract_url.php';
    }
    if ($action == 'unset_notifications') {
        include POST_AJAX . 'other/unset_notifications.php';
    }
    if ($action === 'delete_post') {
        include POST_AJAX . 'other/delete.php';
    }
    if ($action == 'save_user_circles') {
        include POST_AJAX . 'other/save_user_circles.php';
    }
    if ($action == 'like_post') {
        include POST_AJAX . 'other/like_post.php';
    }
    if ($action == 'unlike_post') {
        include POST_AJAX . 'other/unlike_post.php';
    }
    if ($action == 'comment_post') {
        include POST_AJAX . 'other/comment_post.php';
    }
    if ($action == 'delete_comment') {
        include POST_AJAX . 'other/delete_comment.php';
    }
    if ($action == 'get_comments') {
        $entity = get_entity($guid);
        $comments_value = 'post_comments_' . $guid;
        $ia = elgg_set_ignore_access(true);
        $owner_dp = 'user_dp_' . $post_owner->guid;
        $post_comments = ($entity->$comments_value) ? maybe_unserialize($entity->$comments_value) : array();
        elgg_set_ignore_access($ia);
        include POST_AJAX . 'other/get_comments.php';
    }
    if ($action == 'get_more_comments') {
        $entity = get_entity($guid);
        $comments_value = 'post_comments_' . $guid;
        $ia = elgg_set_ignore_access(true);
        $post_comments = ($entity->$comments_value) ? maybe_unserialize($entity->$comments_value) : array();
        elgg_set_ignore_access($ia);
        include POST_AJAX . 'other/get_more_comments.php';
    }
    if ($action == 'update_picture_size') {
        include POST_AJAX . 'page/update_picture_size.php';
    }
    if ($action == 'save_page_profile_picture') {
        include POST_AJAX . 'page/upload_page_profile_picture.php';
    }
    if ($action == 'create_page') {
        include POST_AJAX . 'page/create_page.php';
    }
    if ($action == 'send_message') {
        include POST_AJAX . 'message/send.php';
    }
    if ($action == 'get_messages') {
        include POST_AJAX . 'message/get_messages.php';
    }
    if ($action == 'getChat') {
        include POST_AJAX . 'message/getChat.php';
    }
    if ($action == 'checkChange') {
        include POST_AJAX . 'message/checkChange.php';
    }
    if ($action == 'get_conversations') {
        include POST_AJAX . 'message/conversations.php';
    }
    if ($action == 'create_group') {
        include POST_AJAX . 'group/create_group.php';
    }
    if ($action == 'save_group_cover') {
        include POST_AJAX . 'group/save_group_cover.php';
    }
    if ($action == 'save_group_dp') {
        include POST_AJAX . 'group/save_group_dp.php';
    }
    if ($action == 'add_group_members') {
        include POST_AJAX . 'group/add_group_members.php';
    }
}