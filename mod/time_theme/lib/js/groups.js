$(document).ready(function () {
    $(document).on('click', '.save_group_btn', function (e) {
        e.preventDefault();
        toastr.remove();
        var main_div = $(this).closest('.create_group_main');
        var group_data = {};
        group_data.group_name = main_div.find('#group_name').val();
        if (group_data.group_name) {
            group_data.group_privacy = main_div.find('input[name="privacy"]:checked').val();
            group_data.collection = main_div.find('#collection').val();
            group_data.content_privacy = main_div.find('#content_privacy').val();
            $.post(ajax_url, {action: 'create_group', group_data: group_data}, function (data) {
                if (data) {
                    toastr.success('Group Created Successfully!', 'Success');
                    setTimeout(function () {
                        window.location = base_url + 'ingynius_groups/';
                    }, 500);
                }
            });
        } else {
            toastr.error('Group Name Not Empty!', 'Error');
        }
    });
    // Group DP Change
    $(document).on('click', '#group-dp-change', function () {
        $('input[name="groupCover"]').val('');
        $('#group_profile_dp').trigger('click');
    }).on('change', '#group_profile_dp', function (e) {
        if (this.files.length) {
            $('.save_group_images').submit();
        }
    }).on('submit', '.save_group_images', (function (e) {
        e.preventDefault();
        $("#group-dp-change").removeClass('fa-camera').addClass('fa-spinner fa-spin');
        $(".group-dp-change").addClass('f_show');
        var guid = $(this).find('.group_main').attr('data-entity');
        $.ajax({
            url: upload_url,
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (json)
            {
                var group = $.parseJSON(json);
                if (group.type === 'dp') {
                    $.post(ajax_url, {action: 'save_group_dp', img_url: group.img_url, guid: guid}, function (data) {
                        var src_url = base_url + 'mod/time_theme/' + group.img_url;
                        $('.fb-image-profile').attr('src', src_url);
                        $("#group-dp-change").removeClass('fa-spinner fa-spin').addClass('fa-camera');
                        $(".group-dp-change").removeClass('f_show');
                        $("#group-cover-change").removeClass('fa-spinner fa-spin').addClass('fa-camera');
                        $(".group-cover-change").removeClass('f_show');
                    });
                }
            }
        });
    }));
    // Group Cover Change
    $(document).on('click', '#group-cover-change', function () {
        $('input[name="groupDp"]').val('');
        $('#group_profile_cover').trigger('click');
    }).on('change', '#group_profile_cover', function (e) {
        if (this.files.length) {
            $('.save_group_images').submit();
        }
    }).on('submit', '.save_group_images', (function (e) {
        e.preventDefault();
        $("#group-cover-change").removeClass('fa-camera').addClass('fa-spinner fa-spin');
        $(".group-cover-change").addClass('f_show');
        var guid = $(this).find('.group_main').attr('data-entity');
        $.ajax({
            url: upload_url,
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (json_cover)
            {
                var group = $.parseJSON(json_cover);
                if (group.type === 'cover') {
                    $.post(ajax_url, {action: 'save_group_cover', img_url: group.img_url, guid: guid}, function (data) {
                        var src_url = base_url + 'mod/time_theme/' + group.img_url;
                        $('.fb-image-lg').attr('src', src_url);
                        $("#group-cover-change").removeClass('fa-spinner fa-spin').addClass('fa-camera');
                        $(".group-cover-change").removeClass('f_show');
                        $("#group-dp-change").removeClass('fa-spinner fa-spin').addClass('fa-camera');
                        $(".group-dp-change").removeClass('f_show');
                    });
                }
            }
        });
    }));
    $(document).on('click', '.add_group_members', function () {
        var guid = $(this).attr('data-entity');
        var modal = $('#groupModal');
        modal.find('.modal-title').html('Add Members');
        modal.find('.modal-body').html('<center><img style="margin: 30px 0;" src="' + base_url + 'images/ajax-loader.gif"/></center>');
        modal.modal();
        $.get(ajax_url, {action: 'get_add_members_html', guid: guid}, function (data) {
            modal.find('.modal-body').html(data);
        });
    });
    $(document).on('click', '.add_members_to_group', function () {
        var members = [];
        var main_div = $(this).closest('.add_group_members_main');
        var group_id = $(this).attr('data-entity');
        main_div.find('input[type="checkbox"]:checked').each(function (i) {
            members[i] = $(this).val();
        });
        $.post(ajax_url, {action: 'add_group_members', members: members, group_id: group_id}, function (data) {
            toastr.success('Members Added Successfully!', 'Success');
            setTimeout(function () {
                window.location.reload();
            }, 500);
        });
    });
});
