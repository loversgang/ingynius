/* -----------------------------------
 * Page Posts
 * Version 1.9
 *
 * Written by Balbinder Singh
 * http://www.eimli.com
 *
 * -------------------
 * Post Type Popups
 *
 * 001 - Text
 * 002 - Video
 * 003 - Link
 * 004 - Audio
 * 005 - Event
 * 006 - Photo
 */

$(document).on('click', '#save_page_text', function () {
    var post_type = $(this).find('span').html();
    var page_id = $(this).attr('data-page_id');
    var modal = $('#post_modal');
    $('.modal-title').html(post_type);
    $('#post_content').html('<center><img style="margin: 30px 0;" src="' + base_url + 'images/ajax-loader.gif"/></center>');
    modal.modal();
    $.post(ajax_url, {action: 'get_page_text_html', page_id: page_id}, function (data) {
        $('#post_content').html(data);
        setTimeout(function () {
            $(".chosen").chosen();
            getPlaces();
        }, 200);
    });
});
// Photo Post
$(document).on('click', '#save_page_photo', function () {
    var post_type = $(this).find('span').html();
    var page_id = $(this).attr('data-page_id');
    var modal = $('#post_modal');
    $('.modal-title').html(post_type);
    $('#post_content').html('<center><img style="margin: 30px 0;" src="' + base_url + 'images/ajax-loader.gif"/></center>');
    modal.modal();
    $.post(ajax_url, {action: 'get_page_photo_html', page_id: page_id}, function (data) {
        $('#post_content').html(data);
        setTimeout(function () {
            $(".chosen").chosen();
            getPlaces();
        }, 200);
    });
});
// Link Post
$(document).on('click', '#save_page_link', function () {
    var post_type = $(this).find('span').html();
    var page_id = $(this).attr('data-page_id');
    var modal = $('#post_modal');
    $('.modal-title').html(post_type);
    $('#post_content').html('<center><img style="margin: 30px 0;" src="' + base_url + 'images/ajax-loader.gif"/></center>');
    modal.modal();
    $.post(ajax_url, {action: 'get_page_link_html', page_id: page_id}, function (data) {
        $('#post_content').html(data);
        setTimeout(function () {
            $(".chosen").chosen();
            getPlaces();
        }, 200);
    });
});
// Video Post
$(document).on('click', '#save_page_video', function () {
    var post_type = $(this).find('span').html();
    var page_id = $(this).attr('data-page_id');
    var modal = $('#post_modal');
    $('.modal-title').html(post_type);
    $('#post_content').html('<center><img style="margin: 30px 0;" src="' + base_url + 'images/ajax-loader.gif"/></center>');
    modal.modal();
    $.post(ajax_url, {action: 'get_page_video_html', page_id: page_id}, function (data) {
        $('#post_content').html(data);
        setTimeout(function () {
            $(".chosen").chosen();
            getPlaces();
        }, 200);
    });
});
// Audio Post
$(document).on('click', '#save_page_audio', function () {
    var post_type = $(this).find('span').html();
    var page_id = $(this).attr('data-page_id');
    var modal = $('#post_modal');
    $('.modal-title').html(post_type);
    $('#post_content').html('<center><img style="margin: 30px 0;" src="' + base_url + 'images/ajax-loader.gif"/></center>');
    modal.modal();
    $.post(ajax_url, {action: 'get_page_audio_html', page_id: page_id}, function (data) {
        $('#post_content').html(data);
        setTimeout(function () {
            $(".chosen").chosen();
            getPlaces();
        }, 200);
    });
});
// Event Post
$(document).on('click', '#save_page_event', function () {
    var post_type = $(this).find('span').html();
    var page_id = $(this).attr('data-page_id');
    var modal = $('#post_modal');
    $('.modal-title').html(post_type);
    $('#post_content').html('<center><img style="margin: 30px 0;" src="' + base_url + 'images/ajax-loader.gif"/></center>');
    modal.modal();
    $.post(ajax_url, {action: 'get_page_event_html', page_id: page_id}, function (data) {
        $('#post_content').html(data);
        setTimeout(function () {
            $(".chosen").chosen();
            var autocompleteEvent = new google.maps.places.Autocomplete($("#event_location")[0], {});
            getPlaces();
        }, 200);
        $('#datepicker').datepicker({
            format: "dd-mm-yyyy"
        });
    });
});

// Save Text Post
$(document).on('click', '.submit_page_text_content', function () {
    var data_array = {};
    data_array.page_id = $(this).attr('data-page_id');
    data_array.text_content = $('#text_content').val();
    data_array.text_location = $('#text_location').val();
    data_array.collection = $('#collection').val();
    data_array.content_privacy = $('#content_privacy').val();
    $('.modal-body').html('<center><img style="margin: 30px 0;" src="' + base_url + 'images/ajax-loader.gif"/></center>');
    $.post(ajax_url, {action: 'upload_page_status', data_array: data_array}, function (data) {
        if (data) {
            window.location.reload();
        }
    });
});

// Save PHoto Post
$(document).on('click', '.submit_page_photo_content', function (e) {
    $('#uploadForm').submit();
}).on('submit', '#uploadForm', (function (e) {
    e.preventDefault();
    var data_array = {};
    var main_div = $('#get_post_photo_html');
    data_array.page_id = $('.submit_page_photo_content').attr('data-page_id');
    data_array.text_content = main_div.find('#text_content').val();
    data_array.text_location = main_div.find('#text_location').val();
    data_array.collection = main_div.find('#collection').val();
    data_array.content_privacy = main_div.find('#content_privacy').val();
    $("#targetLayer").html('<center><img style="margin: 65px 0;" src="' + base_url + 'images/ajax-loader.gif"/></center>');
    $.ajax({
        url: upload_url,
        type: "POST",
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        success: function (img_url)
        {
            $("#targetLayer").html('');
            data_array.img_url = img_url;
            $('.modal-body').html('<center><img style="margin: 30px 0;" src="' + base_url + 'images/ajax-loader.gif"/></center>');
            $.post(ajax_url, {action: 'upload_page_photo', data_array: data_array}, function (data) {
                if (data) {
                    window.location.reload();
                }
            });
        },
        error: function ()
        {
        }
    });
}));
// Save Link Post
$(document).on('click', '.submit_page_link_content', function () {
    var data_array = {};
    var main_div = $('#get_post_link_html');
    data_array.page_id = $(this).attr('data-page_id');
    data_array.url = main_div.find('#url').val();
    data_array.image_url = main_div.find('#image_url').attr('src');
    if (!data_array.image_url) {
        data_array.image_url = '';
    }
    data_array.url_title = main_div.find('#url_title').html();
    if (!data_array.url_title) {
        data_array.url_title = '';
    }
    data_array.url_content = main_div.find('#url_content').html();
    if (!data_array.url_content) {
        data_array.url_content = '';
    }
    data_array.text_content = main_div.find('#text_content').val();
    data_array.text_location = main_div.find('#text_location').val();
    data_array.collection = main_div.find('#collection').val();
    data_array.content_privacy = main_div.find('#content_privacy').val();
    $('.modal-body').html('<center><img style="margin: 30px 0;" src="' + base_url + 'images/ajax-loader.gif"/></center>');
    $.post(ajax_url, {action: 'upload_page_link', data_array: data_array}, function (data) {
        if (data) {
            window.location.reload();
        }
    });
});
// Save Audio Post
$(document).on('click', '.submit_page_audio_content', function () {
    var data_array = {};
    var main_div = $('#get_post_audio_html');
    data_array.page_id = $(this).attr('data-page_id');
    data_array.audioID = main_div.find('#audioUrl').val();
    if (data_array.audioID) {
        data_array.text_content = main_div.find('#text_content').val();
        data_array.text_location = main_div.find('#text_location').val();
        data_array.collection = main_div.find('#collection').val();
        data_array.content_privacy = main_div.find('#content_privacy').val();
        $('.modal-body').html('<center><img style="margin: 30px 0;" src="' + base_url + 'images/ajax-loader.gif"/></center>');
        $.post(ajax_url, {action: 'upload_page_audio', data_array: data_array}, function (data) {
            if (data) {
                window.location.reload();
            }
        });
    } else {
        toastr.error('Enter SoundCloud Track ID.', 'Error');
    }
});
// Save Video Post
$(document).on('click', '.submit_page_video_content', function () {
    var data_array = {};
    var main_div = $('#get_post_video_html');
    data_array.page_id = $(this).attr('data-page_id');
    data_array.videoID = main_div.find('#videoUrl').val();
    if (data_array.videoID) {
        data_array.text_content = main_div.find('#text_content').val();
        data_array.text_location = main_div.find('#text_location').val();
        data_array.collection = main_div.find('#collection').val();
        data_array.content_privacy = main_div.find('#content_privacy').val();
        $('.modal-body').html('<center><img style="margin: 30px 0;" src="' + base_url + 'images/ajax-loader.gif"/></center>');
        $.post(ajax_url, {action: 'upload_page_video', data_array: data_array}, function (data) {
            if (data) {
                window.location.reload();
            }
        });
    } else {
        toastr.error('Enter Youtube Video ID.', 'Error');
    }
});
// Save Event Post
$(document).on('click', '.submit_page_event_content', function () {
    var data_array = {};
    var main_div = $('#get_post_event_html');
    data_array.page_id = $(this).attr('data-page_id');
    data_array.title = main_div.find('#event_title').val();
    data_array.date = main_div.find('#datepicker').val();
    data_array.location = main_div.find('#event_location').val();
    if (data_array.title) {
        data_array.text_content = main_div.find('#text_content').val();
        data_array.text_location = main_div.find('#text_location').val();
        data_array.collection = main_div.find('#collection').val();
        data_array.content_privacy = main_div.find('#content_privacy').val();
        $.post(ajax_url, {action: 'upload_page_event', data_array: data_array}, function (data) {
            if (data) {
                window.location.reload();
            }
        });
    } else {
        toastr.error('Enter Event Title.', 'Error');
    }
});
