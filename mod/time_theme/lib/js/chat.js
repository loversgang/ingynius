$(document).ready(function () {
    $(document).on('click', '.close_chat', function () {
        $(this).closest('.user.open').remove();
    });
//    $(document).on('click', '.slide_toggle', function () {
//        $(this).closest('.user.open').find('.toggle_area').slideToggle();
//    });

    $(document).on('click', '.open_in_chat', function () {
        var el = $('.chat_boxes');
        var guid = $(this).attr('data-guid');
        var to_name = $('td.start_conversation[data-guid="' + guid + '"]').find('.username').html();
        $(document).data('chat.targetID', guid);
        var user = '<div class="user open" id="' + guid + '"><header class="slide_toggle"><div class="status"></div><div class="header-text">' + to_name + '</div><div class="close"><i class="close_chat fa fa-close"></i></div></header><div class="toggle_area"><div class="message-area" data-guid="' + guid + '"></div><div class="input-area"><input data-guid="' + guid + '" type="text" id="shout_message" /></div></div></div>';
        var already_added = el.find('#' + guid + '.user.open').length;
        if (!already_added) {
            if ($('.user.open').length < 3) {
                el.append(user);
            } else {
                $('.user.open').first().find('.close_chat').click();
                el.append(user);
            }
        }
        var user_el = $('.message-area[data-guid="' + guid + '"]');
        user_el.html('<center><img style="margin: 30px 0;" src="' + base_url + 'images/ajax-loader.gif"/></center>');
        $.post(ajax_url, {action: 'getChat', guid: guid}, function (data) {
            user_el.html(data);
            window['msg_' + guid] = data.length;
            var scrolltoh = $('.message-area[data-guid="' + guid + '"]')[0].scrollHeight;
            user_el.scrollTop(scrolltoh);
            $('[data-toggle="tooltip"]').tooltip();
        });
    });
    $(document).on('keypress', '#shout_message', function (e) {
        var el = $(this);
        if (e.which === 13) {
            e.preventDefault();
            var data_array = {};
            var guid = el.attr('data-guid');
            var content = el.val();
            data_array.to_id = el.attr('data-guid');
            data_array.content = el.val();
            if (data_array.content) {
                $.post(ajax_url, {action: 'send_message', data_array: data_array}, function () {
                    el.val('');
                    $('.conversation_div').append('<table class="table table-bordered" style="border: none;margin-bottom: 0px"><td style="border:none;width: 50px"><div class="pull-right" style="margin: 5px;"><span data-toggle="tooltip" class="my_chat_msg" title="' + $.timeago($.now()) + '">' + content + '</span></div></td></table>');
                    var scrolltoh = $('.message-area[data-guid="' + guid + '"]')[0].scrollHeight;
                    $('.message-area[data-guid="' + guid + '"]').scrollTop(scrolltoh);
                    $('[data-toggle="tooltip"]').tooltip();
                }).fail(function (err) {
                    alert(err.statusText);
                });
            }
        }
    });
});