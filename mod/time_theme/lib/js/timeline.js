$(document).ready(function () {
    $(document).on('click', '#add_to_circles', function () {
        var circles_modal = $('#circles_modal');
        circles_modal.modal();
    });
    $(document).on('click', '#user_add_circles', function () {
        toastr.remove();
        var user_id = $('#add_to_circles').data('user_id');
        var user_circles_array = [];
        $('#circles_modal.modal input[type=checkbox]:checked').each(function () {
            var circle = $(this).val();
            user_circles_array.push(circle);
        });
        if (user_circles_array.length) {
            $.post(ajax_url, {action: 'save_user_circles', user_id: user_id, user_circles_array: user_circles_array}, function () {
                window.location.reload();
            });
        } else {
            toastr.error('Please Select Circle!', 'Error');
        }
    });
    // Save PHoto Post
    $(document).on('change', '#page_profile_pic', function (e) {
        $('#page_profile_pic_upload_form').submit();
    }).on('submit', '#page_profile_pic_upload_form', (function (e) {
        e.preventDefault();
        $("#result_pic").html('<center><img style="margin: 65px 0;" src="' + base_url + 'images/ajax-loader.gif"/></center>');
        $.ajax({
            url: upload_url,
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (img_url)
            {
                $.post(ajax_url, {action: 'update_picture_size', img_url: img_url}, function (data) {
                    if (data === 'success') {
                        $("#result_pic").html('<center><img style="width:100%;height: 156px" src="' + base_url + 'mod/time_theme/' + img_url + '"/></center>');
                        $(".profile_picture_thumbnail_crop").html('<center><img style="width: 100%" src="' + base_url + 'mod/time_theme/' + img_url + '" id="cropbox" /></center>');
                        $(".preview-container").html('<center><img src="' + base_url + 'mod/time_theme/' + img_url + '"  class="jcrop-preview" /></center>');

                        $(function () {
                            var boundx, boundy,
                                    $pcnt = $('#preview-pane .preview-container'),
                                    $pimg = $('#preview-pane .preview-container img'),
                                    xsize = $('.preview_div').width(),
                                    ysize = $pcnt.height();
                            $pcnt.css('width', xsize);
                            $('#cropbox').Jcrop({
                                onChange: updatePreview,
                                onSelect: updatePreview,
                                aspectRatio: 1
                            }, function () {
                                var bounds = this.getBounds();
                                boundx = bounds[0];
                                boundy = bounds[1];
                            });
                            function updatePreview(c)
                            {
                                $('#x-chord').val(c.x);
                                $('#y-chord').val(c.y);
                                $('#w-chord').val(c.w);
                                $('#h-chord').val(c.h);
                                if (parseInt(c.w) > 0)
                                {
                                    var rx = xsize / c.w;
                                    var ry = ysize / c.h;
                                    $pimg.css({
                                        width: Math.round(rx * boundx) + 'px',
                                        height: Math.round(ry * boundy) + 'px',
                                        marginLeft: '-' + Math.round(rx * c.x) + 'px',
                                        marginTop: '-' + Math.round(ry * c.y) + 'px'
                                    }).show();
                                }
                            }
                            ;
                        });
                    }
                });
            },
            error: function ()
            {
            }
        });
    }));
    $(document).on('click', '.profile_picture_save', function (e) {
        e.preventDefault();
        var main_div = $('.create_page_main');
        var page_data = {};
        page_data.page_name = main_div.find('.page_name_input').val();
        if (page_data.page_name) {
            page_data.page_info = main_div.find('.page_description_input').val();
            page_data.page_web = main_div.find('.page_website_input').val();
            page_data.img_url = $('#cropbox').attr('src');
            page_data.collection = main_div.find('#collection').val();
            page_data.content_privacy = main_div.find('#content_privacy').val();
            var c_div = $('#coordinates');
            var data_array = {};
            data_array.img_url = $('#cropbox').attr('src');
            data_array.xchord = c_div.find('#x-chord').val();
            data_array.ychord = c_div.find('#y-chord').val();
            data_array.wchord = c_div.find('#w-chord').val();
            data_array.hchord = c_div.find('#h-chord').val();
            $.post(ajax_url, {action: 'save_page_profile_picture', data_array: data_array}, function (success) {
                if (success === 'success') {
                    $.post(ajax_url, {action: 'create_page', page_data: page_data}, function (data) {
                        if (data) {
                            toastr.success('Page Created Successfully!', 'Success');
                            setTimeout(function () {
                                window.location = base_url + 'company_pages/';
                            }, 500);
                        }
                    });
                }
            });
        } else {
            toastr.error('Page Name Not Empty!', 'Error');
        }
    });

    // DP Change
    $(document).on('click', '#dp-change', function () {
        $('#user_profile_dp').trigger('click');
    }).on('change', '#user_profile_dp', function (e) {
        if (this.files.length) {
            $('.save_user_dp').submit();
        }
    }).on('submit', '.save_user_dp', (function (e) {
        e.preventDefault();
        $("#dp-change").removeClass('fa-camera').addClass('fa-spinner fa-spin');
        $(".dp-change").addClass('f_show');
        $.ajax({
            url: upload_url,
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (img_url)
            {
                $.post(ajax_url, {action: 'save_user_dp', img_url: img_url}, function (data) {
                    var src_url = base_url + 'mod/time_theme/' + img_url;
                    $('#user_dp').attr('src', src_url);
                    $("#dp-change").removeClass('fa-spinner fa-spin').addClass('fa-camera');
                    $(".dp-change").removeClass('f_show');
                });
            }
        });
    }));
});