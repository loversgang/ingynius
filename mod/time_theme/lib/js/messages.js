$(document).ready(function () {
    getConversations();
    setTimeout(function () {
        checkChange();
    }, 5000);
    window.first_bg = true;
    $(document).on('click', '.list_conversations', function () {
        var main_div = $(this).closest('.message_container');
        var data_array = {};
        data_array.to_id = main_div.find('#to_user').val();
        data_array.content = main_div.find('#message_content').val();
        if (data_array.content) {
            $.post(ajax_url, {action: 'send_message', data_array: data_array}, function (data) {
                window.location = base_url + 'ingynius_messages/';
            });
        }
    });
    $(document).on('click', '.send_message', function () {
        var main_div = $(this).closest('.message_container');
        var data_array = {};
        data_array.to_id = main_div.find('#to_user').val();
        data_array.content = main_div.find('#message_content').val();
        if (data_array.content) {
            $.post(ajax_url, {action: 'send_message', data_array: data_array}, function (data) {
                window.location = base_url + 'ingynius_messages/';
            });
        }
    });
    $(document).on('click', '.start_conversation', function () {
        window.first_bg = false;
        var guid = $(this).attr('data-guid');
        $('.reply_conversation').attr('data-guid', guid);
        $('.conversation').attr('data-guid', guid);
        $('.open_in_chat').attr('data-guid', guid);
        $('.start_conversation').find('table').removeClass('selected_conversation');
        $(this).find('table').addClass('selected_conversation');
        $.post(ajax_url, {action: 'get_messages', guid: guid}, function (data) {
            $('.comments_loader').html('');
            $('.conversation').html(data).animate({
                scrollTop: 0
            }, 800);
            window.messages = data.length;
            window.m_guid = guid;
        });
    });
    $(document).on('click', '.reply_conversation', function () {
        var main_div = $(this).closest('.reply_container');
        var data_array = {};
        data_array.to_id = $(this).attr('data-guid');
        data_array.content = main_div.find('.message_content').val();
        if (data_array.content) {
            $.post(ajax_url, {action: 'send_message', data_array: data_array}, function () {
                main_div.find('.message_content').val('');
            });
        }
    });
    function getConversations() {
        var el = $('.list_conversations');
        $.post(ajax_url, {action: 'get_conversations'}, function (data) {
            el.html(data);
            window.conversations = data.length;
            if (first_bg) {
                $('.start_conversation').first().click();
            }
        });
    }

    function checkChange() {
        var data_array = {};
        var conversation_el = $('.list_conversations');
        var messages_el = $('.conversation');
        if (window.m_guid) {
            data_array.guid = m_guid;
        }
        var chatbox = $('.chat_boxes');
        if (chatbox.length) {
            var guids = {};
            chatbox.find('.user.open').each(function (i) {
                var uid = $(this).attr('id');
                guids[i] = uid;
            });
            data_array.guids = guids;
        }
        if (conversations > 0 || data_array.length) {
            $.post(ajax_url, {action: 'checkChange', data_array: data_array}, function (data) {
                data = $.parseJSON(data);
                if (messages_el.length) {
                    if (messages !== (data.messages).length) {
                        messages_el.html(data.messages);
                        window.messages = (data.messages).length;
                    }
                }
                if (conversation_el.length) {
                    if (conversations !== (data.conversations).length) {
                        conversation_el.html(data.conversations);
                        window.conversations = (data.conversations).length;
                    }
                }
                if (data.msg) {
                    $.each(data.msg, function (guid, content) {
                        if (window['msg_' + guid] !== content.length) {
                            $('.message-area[data-guid="' + guid + '"]').html(content);
                            window['msg_' + guid] = content.length;
                            var scrolltoh = $('.message-area[data-guid="' + guid + '"]')[0].scrollHeight;
                            $('.message-area[data-guid="' + guid + '"]').scrollTop(scrolltoh);
                            $('[data-toggle="tooltip"]').tooltip();
                            //$('#chatAudio')[0].play();
                        }
                    });
                }
                setTimeout(function () {
                    checkChange();
                }, 3000);
            });
        }
    }
});