$(document).ready(function () {
    $('#images_newest_slider').lightSlider({
        gallery: true,
        item: 1,
        loop: true,
        slideMargin: 0,
        thumbItem: 3,
        auto: true
    });
    $('#groups_latest_slider').lightSlider({
        gallery: true,
        item: 1,
        loop: true,
        slideMargin: 0,
        thumbItem: 3,
        auto: true
    });
    $('#videos_newest_slider').lightSlider({
        gallery: true,
        item: 1,
        loop: true,
        slideMargin: 0,
        thumbItem: 3,
        auto: true
    });
    $('#pages_newest_slider').lightSlider({
        gallery: true,
        item: 1,
        loop: true,
        slideMargin: 0,
        thumbItem: 3,
        auto: true
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var target = $(e.target).attr("href");
        if (target === '#groups_featured') {
            if (!window.gf_slider) {
                window.gf_slider = $('#groups_featured_slider').lightSlider({
                    gallery: true,
                    item: 1,
                    loop: true,
                    slideMargin: 0,
                    thumbItem: 3,
                    auto: true
                });
            }
            window.gf_slider.refresh();
        }
        if (target === '#groups_popular') {
            if (!window.gp_slider) {
                window.gp_slider = $('#groups_popular_slider').lightSlider({
                    gallery: true,
                    item: 1,
                    loop: true,
                    slideMargin: 0,
                    thumbItem: 3,
                    auto: true
                });
            }
            window.gp_slider.refresh();
        }
        if (target === '#groups_all') {
            if (!window.ga_slider) {
                window.ga_slider = $('#groups_all_slider').lightSlider({
                    gallery: true,
                    item: 1,
                    loop: true,
                    slideMargin: 0,
                    thumbItem: 3,
                    auto: true
                });
            }
            window.ga_slider.refresh();
        }

        if (target === '#images_featured') {
            if (!window.if_slider) {
                window.if_slider = $('#images_featured_slider').lightSlider({
                    gallery: true,
                    item: 1,
                    loop: true,
                    slideMargin: 0,
                    thumbItem: 3,
                    auto: true
                });
            }
            window.if_slider.refresh();
        }
        if (target === '#images_popular') {
            if (!window.ip_slider) {
                window.ip_slider = $('#images_popular_slider').lightSlider({
                    gallery: true,
                    item: 1,
                    loop: true,
                    slideMargin: 0,
                    thumbItem: 3,
                    auto: true
                });
            }
            window.ip_slider.refresh();
        }
        if (target === '#images_all') {
            if (!window.ia_slider) {
                window.ia_slider = $('#images_all_slider').lightSlider({
                    gallery: true,
                    item: 1,
                    loop: true,
                    slideMargin: 0,
                    thumbItem: 3,
                    auto: true
                });
            }
            window.ia_slider.refresh();
        }
        if (target === '#videos_featured') {
            if (!window.if_slider) {
                window.if_slider = $('#videos_featured_slider').lightSlider({
                    gallery: true,
                    item: 1,
                    loop: true,
                    slideMargin: 0,
                    thumbItem: 3,
                    auto: true
                });
            }
            window.if_slider.refresh();
        }
        if (target === '#videos_popular') {
            if (!window.ip_slider) {
                window.ip_slider = $('#videos_popular_slider').lightSlider({
                    gallery: true,
                    item: 1,
                    loop: true,
                    slideMargin: 0,
                    thumbItem: 3,
                    auto: true
                });
            }
            window.ip_slider.refresh();
        }
        if (target === '#videos_all') {
            if (!window.ia_slider) {
                window.ia_slider = $('#videos_all_slider').lightSlider({
                    gallery: true,
                    item: 1,
                    loop: true,
                    slideMargin: 0,
                    thumbItem: 3,
                    auto: true
                });
            }
            window.ia_slider.refresh();
        }
        if (target === '#pages_featured') {
            if (!window.if_slider) {
                window.if_slider = $('#pages_featured_slider').lightSlider({
                    gallery: true,
                    item: 1,
                    loop: true,
                    slideMargin: 0,
                    thumbItem: 3,
                    auto: true
                });
            }
            window.if_slider.refresh();
        }
        if (target === '#pages_popular') {
            if (!window.ip_slider) {
                window.ip_slider = $('#pages_popular_slider').lightSlider({
                    gallery: true,
                    item: 1,
                    loop: true,
                    slideMargin: 0,
                    thumbItem: 3,
                    auto: true
                });
            }
            window.ip_slider.refresh();
        }
        if (target === '#pages_all') {
            if (!window.ia_slider) {
                window.ia_slider = $('#pages_all_slider').lightSlider({
                    gallery: true,
                    item: 1,
                    loop: true,
                    slideMargin: 0,
                    thumbItem: 3,
                    auto: true
                });
            }
            window.ia_slider.refresh();
        }
    });
});