/* -----------------------------------
 * User Posts
 * Version 1.9
 *
 * Written by Balbinder Singh
 * http://www.eimli.com
 *
 * -------------------
 * Post Type Popups
 *
 * 001 - Text
 * 002 - Video
 * 003 - Link
 * 004 - Audio
 * 005 - Event
 * 006 - Photo
 */

var autocomplete;
$(document).on('click', '#post_text', function () {
    var post_type = $(this).find('span').html();
    var modal = $('#post_modal');
    $('.modal-title').html(post_type);
    $('#post_content').html('<center><img style="margin: 30px 0;" src="' + base_url + 'images/ajax-loader.gif"/></center>');
    modal.modal();
    $.post(ajax_url, {action: 'get_post_text_html'}, function (data) {
        $('#post_content').html(data);
        setTimeout(function () {
            $(".chosen").chosen();
            getPlaces();
        }, 200);
    });
});
// Photo Post
$(document).on('click', '#post_photo', function () {
    var post_type = $(this).find('span').html();
    var modal = $('#post_modal');
    $('.modal-title').html(post_type);
    $('#post_content').html('<center><img style="margin: 30px 0;" src="' + base_url + 'images/ajax-loader.gif"/></center>');
    modal.modal();
    $.post(ajax_url, {action: 'get_post_photo_html'}, function (data) {
        $('#post_content').html(data);
        setTimeout(function () {
            $(".chosen").chosen();
            getPlaces();
        }, 200);
    });
});
// Link Post
$(document).on('click', '#post_link', function () {
    var post_type = $(this).find('span').html();
    var modal = $('#post_modal');
    $('.modal-title').html(post_type);
    $('#post_content').html('<center><img style="margin: 30px 0;" src="' + base_url + 'images/ajax-loader.gif"/></center>');
    modal.modal();
    $.post(ajax_url, {action: 'get_post_link_html'}, function (data) {
        $('#post_content').html(data);
        setTimeout(function () {
            $(".chosen").chosen();
            getPlaces();
        }, 200);
    });
});
// Video Post
$(document).on('click', '#post_video', function () {
    var post_type = $(this).find('span').html();
    var modal = $('#post_modal');
    $('.modal-title').html(post_type);
    $('#post_content').html('<center><img style="margin: 30px 0;" src="' + base_url + 'images/ajax-loader.gif"/></center>');
    modal.modal();
    $.post(ajax_url, {action: 'get_post_video_html'}, function (data) {
        $('#post_content').html(data);
        setTimeout(function () {
            $(".chosen").chosen();
            getPlaces();
        }, 200);
    });
});
// Audio Post
$(document).on('click', '#post_audio', function () {
    var post_type = $(this).find('span').html();
    var modal = $('#post_modal');
    $('.modal-title').html(post_type);
    $('#post_content').html('<center><img style="margin: 30px 0;" src="' + base_url + 'images/ajax-loader.gif"/></center>');
    modal.modal();
    $.post(ajax_url, {action: 'get_post_audio_html'}, function (data) {
        $('#post_content').html(data);
        setTimeout(function () {
            $(".chosen").chosen();
            getPlaces();
        }, 200);
    });
});
// Event Post
$(document).on('click', '#post_event', function () {
    var post_type = $(this).find('span').html();
    var modal = $('#post_modal');
    $('.modal-title').html(post_type);
    $('#post_content').html('<center><img style="margin: 30px 0;" src="' + base_url + 'images/ajax-loader.gif"/></center>');
    modal.modal();
    $.post(ajax_url, {action: 'get_post_event_html'}, function (data) {
        $('#post_content').html(data);
        setTimeout(function () {
            $(".chosen").chosen();
            var autocompleteEvent = new google.maps.places.Autocomplete($("#event_location")[0], {});
            getPlaces();
        }, 200);
        $('#datepicker').datepicker({
            format: "dd-mm-yyyy"
        });
    });
});

// Delete Post
$(document).on('click', '.delete_entity', function () {
    toastr.remove();
    var guid = $(this).data('guid');
    if (confirm('Are You Sure You Want To Delete This Post?')) {
        $('.modal-body').html('<center><img style="margin: 30px 0;" src="' + base_url + 'images/ajax-loader.gif"/></center>');
        $.post(ajax_url, {action: 'delete_post', guid: guid}, function (data) {
            if (data === 'success') {
                toastr.success('Item deleted successfully!', 'Success');
            }
            setTimeout(function () {
                window.location.reload();
            }, 1000);
        });
    }
});

// Save Text Post
$(document).on('click', '.submit_text_content', function () {
    var data_array = {};
    data_array.text_content = $('#text_content').val();
    data_array.text_location = $('#text_location').val();
    data_array.collection = $('#collection').val();
    data_array.content_privacy = $('#content_privacy').val();
    $('.modal-body').html('<center><img style="margin: 30px 0;" src="' + base_url + 'images/ajax-loader.gif"/></center>');
    $.post(ajax_url, {action: 'upload_status', data_array: data_array}, function (data) {
        if (data) {
            window.location.reload();
        }
    });
});

// Save PHoto Post
$(document).on('click', '.submit_photo_content', function (e) {
    $('#uploadForm').submit();
}).on('submit', '#uploadForm', (function (e) {
    e.preventDefault();
    var data_array = {};
    var main_div = $('#get_post_photo_html');
    data_array.text_content = main_div.find('#text_content').val();
    data_array.text_location = main_div.find('#text_location').val();
    data_array.collection = main_div.find('#collection').val();
    data_array.content_privacy = main_div.find('#content_privacy').val();
    $("#targetLayer").html('<center><img style="margin: 65px 0;" src="' + base_url + 'images/ajax-loader.gif"/></center>');
    $.ajax({
        url: upload_url,
        type: "POST",
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        success: function (img_url)
        {
            $("#targetLayer").html('');
            data_array.img_url = img_url;
            $('.modal-body').html('<center><img style="margin: 30px 0;" src="' + base_url + 'images/ajax-loader.gif"/></center>');
            $.post(ajax_url, {action: 'upload_photo', data_array: data_array}, function (data) {
                if (data) {
                    window.location.reload();
                }
            });
        },
        error: function ()
        {
        }
    });
}));
// Save Link Post
$(document).on('click', '.submit_link_content', function () {
    var data_array = {};
    var main_div = $('#get_post_link_html');
    data_array.url = main_div.find('#url').val();
    data_array.image_url = main_div.find('#image_url').attr('src');
    if (!data_array.image_url) {
        data_array.image_url = '';
    }
    data_array.url_title = main_div.find('#url_title').html();
    if (!data_array.url_title) {
        data_array.url_title = '';
    }
    data_array.url_content = main_div.find('#url_content').html();
    if (!data_array.url_content) {
        data_array.url_content = '';
    }
    data_array.text_content = main_div.find('#text_content').val();
    data_array.text_location = main_div.find('#text_location').val();
    data_array.collection = main_div.find('#collection').val();
    data_array.content_privacy = main_div.find('#content_privacy').val();
    $('.modal-body').html('<center><img style="margin: 30px 0;" src="' + base_url + 'images/ajax-loader.gif"/></center>');
    $.post(ajax_url, {action: 'upload_link', data_array: data_array}, function (data) {
        if (data) {
            window.location.reload();
        }
    });
});
// Save Audio Post
$(document).on('click', '.submit_audio_content', function () {
    var data_array = {};
    var main_div = $('#get_post_audio_html');
    data_array.audioID = main_div.find('#audioUrl').val();
    if (data_array.audioID) {
        data_array.text_content = main_div.find('#text_content').val();
        data_array.text_location = main_div.find('#text_location').val();
        data_array.collection = main_div.find('#collection').val();
        data_array.content_privacy = main_div.find('#content_privacy').val();
        $('.modal-body').html('<center><img style="margin: 30px 0;" src="' + base_url + 'images/ajax-loader.gif"/></center>');
        $.post(ajax_url, {action: 'upload_audio', data_array: data_array}, function (data) {
            if (data) {
                window.location.reload();
            }
        });
    } else {
        toastr.error('Enter SoundCloud Track ID.', 'Error');
    }
});
// Save Video Post
$(document).on('click', '.submit_video_content', function () {
    var data_array = {};
    var main_div = $('#get_post_video_html');
    data_array.videoID = main_div.find('#videoUrl').val();
    if (data_array.videoID) {
        data_array.text_content = main_div.find('#text_content').val();
        data_array.text_location = main_div.find('#text_location').val();
        data_array.collection = main_div.find('#collection').val();
        data_array.content_privacy = main_div.find('#content_privacy').val();
        $('.modal-body').html('<center><img style="margin: 30px 0;" src="' + base_url + 'images/ajax-loader.gif"/></center>');
        $.post(ajax_url, {action: 'upload_video', data_array: data_array}, function (data) {
            if (data) {
                window.location.reload();
            }
        });
    } else {
        toastr.error('Enter Youtube Video ID.', 'Error');
    }
});
// Save Event Post
$(document).on('click', '.submit_event_content', function () {
    var data_array = {};
    var main_div = $('#get_post_event_html');
    data_array.title = main_div.find('#event_title').val();
    data_array.date = main_div.find('#datepicker').val();
    data_array.location = main_div.find('#event_location').val();
    if (data_array.title) {
        data_array.text_content = main_div.find('#text_content').val();
        data_array.text_location = main_div.find('#text_location').val();
        data_array.collection = main_div.find('#collection').val();
        data_array.content_privacy = main_div.find('#content_privacy').val();
        $.post(ajax_url, {action: 'upload_event', data_array: data_array}, function (data) {
            if (data) {
                window.location.reload();
            }
        });
    } else {
        toastr.error('Enter Event Title.', 'Error');
    }
});

// Update Posts
$(document).on('click', '.edit_entity', function () {
    var post_type = $(this).data('type');
    var guid = $(this).data('guid');
    var modal = $('#post_modal');
    $('.modal-title').html('Update ' + post_type);
    $('#post_content').html('<center><img style="margin: 30px 0;" src="' + base_url + 'images/ajax-loader.gif"/></center>');
    modal.modal();
    var action = '';
    if (post_type === 'text') {
        action = 'get_post_edit_text_html';
    } else if (post_type === 'photo') {
        action = 'get_post_edit_photo_html';
    } else if (post_type === 'link') {
        action = 'get_post_edit_link_html';
    } else if (post_type === 'audio') {
        action = 'get_post_edit_audio_html';
    } else if (post_type === 'video') {
        action = 'get_post_edit_video_html';
    } else if (post_type === 'event') {
        action = 'get_post_edit_event_html';
    } else {
        action = '';
    }
    if (action) {
        $.post(ajax_url, {action: action, guid: guid}, function (data) {
            $('#post_content').html(data);
            setTimeout(function () {
                $(".chosen").chosen();
                if (action === 'get_post_edit_event_html') {
                    var autocompleteEvent = new google.maps.places.Autocomplete($("#event_location")[0], {});
                    $('#datepicker').datepicker({
                        format: "dd-mm-yyyy"
                    });
                }
                getPlaces();
            }, 200);
        });
    }
});

// Update Text Post
$(document).on('click', '.submit_edit_text_content', function () {
    var data_array = {};
    var guid = $(this).data('guid');
    data_array.text_content = $('#text_content').val();
    data_array.text_location = $('#text_location').val();
    data_array.collection = $('#collection').val();
    data_array.content_privacy = $('#content_privacy').val();
    $.post(ajax_url, {action: 'edit_post', data_array: data_array, guid: guid}, function (data) {
        if (data) {
            window.location.reload();
        }
    });
});
// Update Photo Post
$(document).on('click', '.submit_edit_photo_content', function (e) {
    e.preventDefault();
    $('#uploadForm_edit').submit();
}).on('submit', '#uploadForm_edit', (function (e) {
    e.preventDefault();
    var data_array = {};
    var guid = $('.submit_edit_photo_content').data('guid');
    var main_div = $('#get_post_photo_html');
    data_array.text_content = main_div.find('#text_content').val();
    data_array.text_location = main_div.find('#text_location').val();
    data_array.collection = main_div.find('#collection').val();
    data_array.content_privacy = main_div.find('#content_privacy').val();
    $("#targetLayer").html('<center><img style="margin: 65px 0;" src="' + base_url + 'images/ajax-loader.gif"/></center>');
    console.log(upload_url);
    $.ajax({
        url: upload_url,
        type: "POST",
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        success: function (img_url) {
            $("#targetLayer").html('');
            data_array.img_url = img_url;
            $.post(ajax_url, {action: 'edit_post', data_array: data_array, guid: guid}, function (data) {
                if (data) {
                    window.location.reload();
                }
            });
        },
        error: function ()
        {
        }
    });
}));
// Update Link Post
$(document).on('click', '.submit_edit_link_content', function () {
    var data_array = {};
    var guid = $(this).data('guid');
    var main_div = $('#get_post_link_html');
    data_array.url = main_div.find('#url').val();
    data_array.image_url = main_div.find('#image_url').attr('src');
    if (!data_array.image_url) {
        data_array.image_url = '';
    }
    data_array.url_title = main_div.find('#url_title').html();
    if (!data_array.url_title) {
        data_array.url_title = '';
    }
    data_array.url_content = main_div.find('#url_content').html();
    if (!data_array.url_content) {
        data_array.url_content = '';
    }
    data_array.text_content = main_div.find('#text_content').val();
    data_array.text_location = main_div.find('#text_location').val();
    data_array.collection = main_div.find('#collection').val();
    data_array.content_privacy = main_div.find('#content_privacy').val();
    $.post(ajax_url, {action: 'edit_post', data_array: data_array, guid: guid}, function (data) {
        if (data) {
            window.location.reload();
        }
    });
});
// Update Audio Post
$(document).on('click', '.submit_edit_audio_content', function () {
    var data_array = {};
    var guid = $(this).data('guid');
    var main_div = $('#get_post_audio_html');
    data_array.audioID = main_div.find('#audioUrl').val();
    if (data_array.audioID) {
        data_array.text_content = main_div.find('#text_content').val();
        data_array.text_location = main_div.find('#text_location').val();
        data_array.collection = main_div.find('#collection').val();
        data_array.content_privacy = main_div.find('#content_privacy').val();
        $.post(ajax_url, {action: 'edit_post', data_array: data_array, guid: guid}, function (data) {
            if (data) {
                window.location.reload();
            }
        });
    } else {
        toastr.error('Enter SoundCloud Track ID.', 'Error');
    }
});
// Update Video Post
$(document).on('click', '.submit_edit_video_content', function () {
    var data_array = {};
    var guid = $(this).data('guid');
    var main_div = $('#get_post_video_html');
    data_array.videoID = main_div.find('#videoUrl').val();
    if (data_array.videoID) {
        data_array.text_content = main_div.find('#text_content').val();
        data_array.text_location = main_div.find('#text_location').val();
        data_array.collection = main_div.find('#collection').val();
        data_array.content_privacy = main_div.find('#content_privacy').val();
        $.post(ajax_url, {action: 'edit_post', data_array: data_array, guid: guid}, function (data) {
            if (data) {
                window.location.reload();
            }
        });
    } else {
        toastr.error('Enter Youtube Video ID.', 'Error');
    }
});
// Update Event Post
$(document).on('click', '.submit_edit_event_content', function () {
    var data_array = {};
    var guid = $(this).data('guid');
    var main_div = $('#get_post_event_html');
    data_array.title = main_div.find('#event_title').val();
    data_array.date = main_div.find('#datepicker').val();
    data_array.location = main_div.find('#event_location').val();
    if (data_array.title) {
        data_array.text_content = main_div.find('#text_content').val();
        data_array.text_location = main_div.find('#text_location').val();
        data_array.collection = main_div.find('#collection').val();
        data_array.content_privacy = main_div.find('#content_privacy').val();
        $.post(ajax_url, {action: 'edit_post', data_array: data_array, guid: guid}, function (data) {
            if (data) {
                window.location.reload();
            }
        });
    } else {
        toastr.error('Enter Event Title.', 'Error');
    }
});

// Other Options
$(document).on('click', '#upload_photo', function () {
    $(".inputFile").click();
});
$(document).on('change', '.inputFile', function () {
    $('#f_name').html($(this).val().replace(/C:\\fakepath\\/i, ''));
});

// Get Youtube Video By ID
$(document).on('click', '#embed_youtube_video', function () {
    var url = $('#videoUrl').val();
    if (url) {
        var video = '<iframe class="img img-thumbnail" src="https://www.youtube.com/embed/' + url + '" style="width:100%"></iframe>';
        $("#youtube_frame").html('<center><img style="margin: 65px 15px;" src="' + base_url + 'images/ajax-loader.gif"/></center>');
        setTimeout(function () {
            $("#youtube_frame").html(video);
        }, 1000);
    } else {
        toastr.error('Enter Youtube Video ID.', 'Error');
    }
});

// Get Soundcloud Track By ID
$(document).on('click', '#embed_soundcloud_audio', function () {
    var url = $('#audioUrl').val();
    if (url) {
        var audio = '<iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/' + url + '&amp;color=ff6600&amp;auto_play=false&amp;show_artwork=true"></iframe>';
        $("#soundcloud_frame").html('<center><img style="margin: 65px 15px;" src="' + base_url + 'images/ajax-loader.gif"/></center>');
        setTimeout(function () {
            $("#soundcloud_frame").html(audio);
        }, 1000);
    } else {
        toastr.error('Enter SoundCloud Track ID.', 'Error');
    }
});

// Toggle Location Map
$(document).on('click', '#get_location_html', function () {
    $('#location_html').css({
        'background-color': '#eee',
        'margin-top': '10px'
    });
    $('#location_input').slideToggle(100);
});

// Get Url Info
$(document).on('click', '#get_url_info', function () {
    var url = $('#url').val();
    if (url) {
        extractURL(url);
    } else {
        toastr.error('Please Enter Valid Url.', 'Error');
    }
});

// Extract Url
function extractURL(url) {
    $.ajax({
        url: ajax_url,
        data: {
            action: 'extract_url',
            url: url
        },
        type: "POST",
        beforeSend: function () {
            $("#url").css("background", "#FFF url(" + base_url + "' + base_url + 'images/ajax-loader.gif) no-repeat right center");
        },
        success: function (responseData) {
            $("#output_link_details").html(responseData).hide().slideDown();
            $("#url").css("background", "");
        }
    });
}
// GMaps Integration
function getPlaces() {
    autocomplete = new google.maps.places.Autocomplete($("#text_location")[0], {});
    autocomplete.addListener('place_changed', getMap);
}
function getMap() {
    var place = autocomplete.getPlace();
    var lat = place.geometry.location.lat();
    var lng = place.geometry.location.lng();
    var LatLng = {lat: lat, lng: lng};
    var map_div = document.getElementById('location_html');
    var map = new google.maps.Map(map_div, {
        zoom: 12,
        center: LatLng
    });

    var marker = new google.maps.Marker({
        position: LatLng,
        map: map,
        //title: 'Hello World!'
    });
}

// Post Likes
$(document).on('click', '.fa-thumbs-o-up', function () {
    $(this).removeClass('fa-thumbs-o-up').addClass('fa-thumbs-up');
    var guid = $(this).data('guid');
    var el = $(this).closest('.like_post').find('.post_like_count');
    var post_like_count = +el.html();
    el.html(post_like_count + 1);
    $.post(ajax_url, {action: 'like_post', guid: guid});
});
$(document).on('click', '.fa-thumbs-up', function () {
    $(this).removeClass('fa-thumbs-up').addClass('fa-thumbs-o-up');
    var guid = $(this).data('guid');
    var el = $(this).closest('.like_post').find('.post_like_count');
    var post_like_count = +el.html();
    el.html(post_like_count - 1);
    $.post(ajax_url, {action: 'unlike_post', guid: guid});
});

// Post Comments
$(document).on('click', '.comment_post', function () {
    var guid = $(this).data('guid');
    var main_div = $(this).closest('.item');
    main_div.find('.comments_div').fadeIn(100, function () {
        getComments(guid);
    });
});
$(document).on('click', '.submit_post_comment', function () {
    toastr.remove();
    var guid = $(this).data('guid');
    var main_div = $(this).closest('.item');
    var comment = main_div.find('.comment_post_data').val();
    if (comment) {
        $.post(ajax_url, {action: 'comment_post', guid: guid, comment: comment}, function () {
            getComments(guid);
            main_div.find('.comment_post_data').val('');
        });
    } else {
        toastr.error('Please Enter Comment!', 'Error!');
    }
});
$(document).on('click', '.submit_post_comment_popup', function () {
    toastr.remove();
    var guid = $(this).data('guid');
    var comment = $('.comments_popup_div').find('.comment_post_data_popup').val();
    if (comment) {
        $.post(ajax_url, {action: 'comment_post', guid: guid, comment: comment}, function () {
            getComments(guid);
            $('.comments_popup_div').find('.comment_post_data_popup').val('');
        });
    } else {
        toastr.error('Please Enter Comment!', 'Error!');
    }
});
$(document).on('click', '.delete_comment', function () {
    var el = $(this);
    var guid = el.data('guid');
    var date_add = el.data('date_add');
    if (confirm('Are You Sure You Want To Delete This Post?')) {
        $.post(ajax_url, {action: 'delete_comment', guid: guid, date_add: date_add}, function () {
            $(el.parents('.photo_comments')[0]).hide(400, function () {
                $(this).remove();
                getComments(guid);
            });
        });
    }
});
$(document).on('click', '.load_more_comments', function () {
    var el = $(this);
    var key_value = +el.attr('data-key');
    var count = +el.data('count');
    var key = (key_value + 1);
    var limit = +el.data('limit');
    var guid = el.data('guid');
    var data_key = (key_value + limit);
    var load_more = $('.load_more_data_' + guid).find('center .load_more_comments');
    var load_more_prev_html = load_more.html();
    load_more.append(' <i class="fa fa-refresh fa-spin"></i>').prop('disabled', true);
    $('.js-masonry').masonry('reloadItems').masonry('layout');
    $.post(ajax_url, {action: 'get_more_comments', guid: guid, key: key, limit: limit}, function (data) {
        $('.comments_data_div_' + guid).append(data);
        if (count > data_key) {
            load_more.html(load_more_prev_html).prop("disabled", false).attr('data-key', data_key);
        } else {
            $('.load_more_data_' + guid).fadeOut();
        }
        $('.js-masonry').masonry('reloadItems').masonry('layout');
    });
});

// Images Popup
$(document).on('click', '.img_options', function () {
    var guid = $(this).attr('data-entity');
    var modal = $("#myModal");
    modal.modal();
    modal.find('.modal-body').html('<center><img style="margin: 30px 0;" src="' + base_url + 'images/ajax-loader.gif"/></center>');
    $.get(ajax_url, {action: 'get_photo_modal', guid: guid}, function (data) {
        modal.find('.modal-body').html(data);
        getComments(guid);
    });
});

// Video Popup
$(document).on('click', '.video_options', function () {
    var guid = $(this).attr('data-entity');
    var modal = $("#myModal");
    modal.modal();
    modal.find('.modal-body').html('<center><img style="margin: 30px 0;" src="' + base_url + 'images/ajax-loader.gif"/></center>');
    $.get(ajax_url, {action: 'get_video_modal', guid: guid}, function (data) {
        modal.find('.modal-body').html(data);
        getComments(guid);
    });
});

// Pages Redirection
$(document).on('click', '.pages_img', function () {
    var guid = $(this).attr('data-entity');
    window.location = base_url + 'company_pages/view/?page_id=' + guid;
});

// Groups Redirection
$(document).on('click', '.group_img', function () {
    var guid = $(this).attr('data-entity');
    window.location = base_url + 'ingynius_groups/view/?group_id=' + guid;
});

// Unset Notifications
$(document).on('click', '.post_details_notif', function (e) {
    e.preventDefault();
    var main_div = $(this).closest('.notif_tr');
    var post_url = $(this).attr('href');
    var data_array = {};
    data_array.post_id = $(this).attr('data-post');
    data_array.from_id = $(this).attr('data-from');
    data_array.type = $(this).attr('data-type');
    data_array.time = $(this).attr('data-time');
    main_div.find('.notif_loader').html('<center><img src="' + base_url + 'images/ajax-loader.gif"/></center>');
    $.post(ajax_url, {action: 'unset_notifications', data_array: data_array}, function () {
        main_div.find('.notif_loader').html('');
        window.location = post_url;
    });
});

// Get Comments
function getComments(guid) {
    var comments_data = $('.comments_data[data-guid="' + guid + '"]');
    comments_data.html('<center><img style="margin: 30px 0;" src="' + base_url + 'images/ajax-loader.gif"/></center>');
    $.post(ajax_url, {action: 'get_comments', guid: guid}, function (data) {
        comments_data.html(data);
        var totalComments = $('.comments_data_div_' + guid).attr('data-count');
        var c_count = 0;
        if (totalComments) {
            c_count = totalComments;
        }
        $('.comment_post[data-guid="' + guid + '"]').find('.pull-right').html(c_count);
        $('.js-masonry').masonry('reloadItems').masonry('layout');
    });
}