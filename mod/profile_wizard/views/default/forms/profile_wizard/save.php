<?php
$logged_user = elgg_get_logged_in_user_entity();
$data = 'data_' . $logged_user->guid;
$user_details = unserialize($logged_user->$data);
$user = (object) $user_details;
?>
<div class="row-fluid" style="background-color: #fff; margin-top: 10px;min-height: 780px;">
    <div class="col-md-12">
        <h2 class="page-header" style="border:none;margin: 10px 0 20px 0">
            Edit Profile
        </h2>
        <hr/>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label">Headline</label><br />
            <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-header"></i></div>
                <input type="text" class="form-control" name="headline" value="<?php echo $user->headline ? $user->headline : ''; ?>"/>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label"><?php echo elgg_echo("Phone Number"); ?></label><br />
            <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-phone"></i></div>
                <?php
                echo elgg_view('input/text', array(
                    'type' => 'number',
                    'name' => 'mobile',
                    'class' => 'form-control',
                    'id' => 'mobile',
                    'placeholder' => '1234567890',
                    'value' => $user->mobile ? $user->mobile : ''
                ));
                ?>
                <div title="Verify Phone Number" class="input-group-addon" style="    background-color: #3b5998;color: #fff;border: 1px;"><i class="fa fa-check-circle"></i></div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label"><?php echo elgg_echo("Email Address"); ?></label><br />
            <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-at"></i></div>
                <?php
                echo elgg_view('input/text', array(
                    'type' => 'text',
                    'name' => 'email',
                    'class' => 'form-control',
                    'id' => 'email',
                    'disabled' => 'true',
                    'value' => $logged_user->email
                ));
                ?>
                <div title="Verify Email Address" class="input-group-addon" style="background-color: #3b5998;color: #fff;border: 1px;"><i class="fa fa-check-circle"></i></div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label" for="Date"><?php echo elgg_echo("Date"); ?></label>
            <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-birthday-cake"></i></div>
                <?php
                echo elgg_view('input/text', array(
                    'type' => 'number',
                    'name' => 'birth_date',
                    'class' => 'form-control',
                    'id' => 'birth_date',
                    'placeholder' => 'DD',
                    'value' => $user->birth_date ? $user->birth_date : ''
                ));
                ?>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label" for="Month"><?php echo elgg_echo("Month"); ?></label>
            <div class="input-group">
                <div class="input-group-addon"></div>
                <?php
                echo elgg_view('input/text', array(
                    'type' => 'number',
                    'name' => 'birth_month',
                    'class' => 'form-control',
                    'id' => 'birth_month',
                    'placeholder' => 'MM',
                    'value' => $user->birth_month ? $user->birth_month : ''
                ));
                ?>
                <div class="input-group-addon"></div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group" style="margin-left: 5px">
            <label class="control-label" for="Year"><?php echo elgg_echo("Year"); ?></label>
            <div class="input-group">
                <div class="input-group-addon"></div>
                <?php
                echo elgg_view('input/text', array(
                    'type' => 'number',
                    'name' => 'birth_year',
                    'class' => 'form-control',
                    'id' => 'birth_year',
                    'placeholder' => 'YYYY',
                    'value' => $user->birth_year ? $user->birth_year : ''
                ));
                ?>
                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label" for="Gender"><?php echo elgg_echo("Gender"); ?></label>
            <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-user"></i></div>
                <select name="gender" class="form-control">
                    <option value="male">Male</option>
                    <option value="female" <?php echo $user->gender == 'female' ? 'selected' : ''; ?>>Female</option>
                </select>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label">Hometown</label><br />
            <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-home"></i></div>
                <input type="text" class="form-control" name="hometown" value="<?php echo $user->hometown ? $user->hometown : ''; ?>"/>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label">Currently Living</label><br />
            <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-map-marker"></i></div>
                <input type="text" class="form-control" name="living" value="<?php echo $user->living ? $user->living : ''; ?>"/>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label class="control-label">Work</label><br />
            <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-jsfiddle"></i></div>
                <input type="text" class="form-control" name="work" value="<?php echo $user->work ? $user->work : ''; ?>"/>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <input type="submit" class="btn btn-success" value="Edit Profile"/>
    </div>
</div>