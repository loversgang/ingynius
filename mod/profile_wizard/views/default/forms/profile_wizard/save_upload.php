<?php
$logged_user = elgg_get_logged_in_user_entity();
?>
<div class="row-fluid">
    <div class="col-md-4">
        <div id="targetLayer">
            <span id="page_photo"><?php echo elgg_view_entity_icon($logged_user, "large"); ?></span>
        </div>
    </div>
    <div class="col-md-8">
        <div id="uploadFormLayer">
            <label>Upload Image File:</label><br/>
            <input name="userImage" type="file" class="inputFile" />
            <br/>
            <input type="submit" value="Submit" class="btnSubmit btn btn-success" />
        </div>
    </div>
</div>
<script type="text/javascript" >
    $(document).ready(function (e) {
        $("#uploadForm").on('submit', (function (e) {
            e.preventDefault();
            $("#targetLayer").html('<center><img src="<?php echo elgg_get_site_url(); ?>images/loader.gif"/></center>');
            $.ajax({
                url: "<?php echo elgg_get_site_url(); ?>mod/profile_wizard/upload.php",
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function (data)
                {
                    $("#targetLayer").html(data);
                },
                error: function ()
                {
                }
            });
        }));
    });
</script>