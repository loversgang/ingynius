<?php

// Get Logged In User Details
$logged_user = elgg_get_logged_in_user_entity();
// Create MetaData
$profile->headline = get_input('headline');
$profile->mobile = get_input('mobile');
$profile->birth_date = get_input('birth_date');
$profile->birth_month = get_input('birth_month');
$profile->birth_year = get_input('birth_year');
$profile->gender = get_input('gender');
$profile->hometown = get_input('hometown');
$profile->living = get_input('living');
$profile->work = get_input('work');
$profile->access_id = ACCESS_PUBLIC;
$profile->owner_guid = elgg_get_logged_in_user_guid();
$data = serialize((array) $profile);
create_metadata($logged_user->guid, 'data_' . $logged_user->guid, $data);
if ($logged_user->save()) {
    system_message("Profile Updated Succesfully!");
    forward(elgg_get_site_url() . 'profile_wizard/');
} else {
    register_error("Something Went Wrong");
    forward(elgg_get_site_url() . 'profile_wizard/');
}