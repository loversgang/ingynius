<?php

elgg_register_event_handler('init', 'system', 'profile_wizard_init');

function profile_wizard_init() {
    elgg_register_page_handler('profile_wizard', 'profile_wizard_page_handler');
}

function profile_wizard_page_handler($segments) {
    if (!$segments[0]) {
        include elgg_get_plugins_path() . 'profile_wizard/pages/profile_wizard/add.php';
        return true;
    }
    if ($segments[0] == 'add') {
        include elgg_get_plugins_path() . 'profile_wizard/pages/profile_wizard/add.php';
        return true;
    }
    if ($segments[0] == 'upload') {
        include elgg_get_plugins_path() . 'profile_wizard/pages/profile_wizard/upload.php';
        return true;
    }
    return false;
}

elgg_register_action("profile_wizard/save", elgg_get_plugins_path() . "profile_wizard/actions/profile_wizard/save.php");
elgg_register_action("profile_wizard/save_upload", elgg_get_plugins_path() . "profile_wizard/actions/profile_wizard/save_upload.php");
