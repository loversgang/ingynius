<?php

elgg_register_event_handler('init', 'system', 'ingynius_groups_init');

function ingynius_groups_init() {
    elgg_register_plugin_hook_handler('entity:url', 'object', 'pages_set_url');
    elgg_register_page_handler('ingynius_groups', 'ingynius_groups_page_handler');
}

function ingynius_groups_page_handler($segments) {
    if (!$segments[0]) {
        include elgg_get_plugins_path() . 'ingynius_groups/pages/ingynius_groups/list.php';
        return true;
    }
    if ($segments[0] == 'list') {
        include elgg_get_plugins_path() . 'ingynius_groups/pages/ingynius_groups/list.php';
        return true;
    }
    if ($segments[0] == 'create') {
        include elgg_get_plugins_path() . 'ingynius_groups/pages/ingynius_groups/create.php';
        return true;
    }
    if ($segments[0] == 'view') {
        include elgg_get_plugins_path() . 'ingynius_groups/pages/ingynius_groups/view.php';
        return true;
    }
    if ($segments[0] == 'members') {
        include elgg_get_plugins_path() . 'ingynius_groups/pages/ingynius_groups/members.php';
        return true;
    }
    return false;
}

elgg_register_action("ingynius_groups/create", elgg_get_plugins_path() . "ingynius_groups/actions/ingynius_groups/create.php");
elgg_register_action("ingynius_groups/save_upload", elgg_get_plugins_path() . "ingynius_groups/actions/ingynius_groups/save_upload.php");
