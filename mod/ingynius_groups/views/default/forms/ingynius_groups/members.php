<?php
include_once elgg_get_plugins_path() . 'time_theme/functions.php';
$logged_user = elgg_get_logged_in_user_entity();
$guid = $_GET['group_id'];
$entity = get_entity($guid);
$members = getGroupMembers($guid);
if ($entity->title == 'group_content') {
    ?>
    <div class="row-fluid"> 
        <div class="col-md-12" style="min-height: 800px;background-color: #fff; padding-top: 10px">
            <h2 class="page-header" style="border:none;margin: 10px 0 20px 0">
                <span class="pull-right" style="margin-right: 15px;">
                    <a href="<?php echo elgg_get_site_url(); ?>ingynius_groups/view/?group_id=<?php echo $guid; ?>" class="btn btn-success btn-xs"><i class="fa fa-reply"></i> Back</a>
                    <a href="<?php echo elgg_get_site_url(); ?>ingynius_groups/" class="btn btn-success btn-xs"><i class="fa fa-list"></i> My Groups</a>
                    <?php if ($entity->owner_guid == $logged_user->guid) { ?>
                        <button type="button" data-entity="<?php echo $guid; ?>" class="btn btn-success btn-xs add_group_members"><i class="fa fa-plus"></i> Add Members</button>
                    <?php } ?>
                </span>
                Members
            </h2>
            <hr/>
            <div class="row-fluid">
                <?php
                foreach ($members as $user_id) {
                    $user = get_entity($user_id);
                    ?>
                    <div class="col-md-4">
                        <table class="table table-bordered">
                            <tr>
                                <td style="width: 25%">
                                    <img src="<?php echo getUserDp($user_id); ?>" class="img" style="height:35px"/>
                                </td>
                                <td style="width: 50%">
                                    <?php echo $user->name; ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <?php
} else {
    forward(elgg_get_site_url() . 'company_pages/');
}