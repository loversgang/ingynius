<?php
$logged_user = elgg_get_logged_in_user_entity();
$collections = get_user_access_collections($logged_user->guid);
$circles = elgg_get_entities(array(
    'type' => 'object',
    'subtype' => 'circle',
    'limit' => FALSE,
        ));
?>
<div class="row-fluid create_group_main"> 
    <div class="col-md-12" style="min-height: 800px;background-color: #fff; padding-top: 10px">
        <h2 class="page-header" style="border:none;margin: 10px 0 20px 0">
            <span class="pull-right" style="margin-right: 15px;">
                <a href="<?php echo elgg_get_site_url(); ?>ingynius_groups/" class="btn btn-success"><i class="fa fa-list"></i> My Groups</a>
            </span>
            Create Group
        </h2>
        <div class="col-md-12">
            <div class="form-group">
                <label for="fname" class="col-md-2 control-label">Group Name</label>
                <div class="col-md-10">
                    <input type="text" class="form-control" name="group_name" id="group_name" placeholder="Group Name">
                </div>
            </div>
            <hr/>
            <div class="form-group">
                <label class="control-label col-md-2">Privacy</label>
                <div class="col-md-10">
                    <div class="radio">
                        <label>
                            <input name="privacy" type="radio" value="public" checked>
                            <i class="fa fa-globe"></i> Public<br>
                            <font color="grey">Anyone can see the group, its members and their posts.</font>
                            <hr>
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input name="privacy" type="radio" value="closed">
                            <i class="fa fa-lock"></i> Closed<br>
                            <font color="grey">Anyone can find the group and see who's in it. Only members can see posts.</font>
                            <hr>
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input name="privacy" type="radio" value="secret">
                            <i class="fa fa-key"></i> Secret<br>
                            <font color="grey">Only members can find the group and see posts.</font>
                        </label>
                    </div>
                </div>
            </div>
            <?php if ($collections) { ?>
                <div class="col-md-12">
                    <div class="form-group">
                        <label name="select_collection">Add To Collection</label>
                        <div class="input-group">
                            <div class="input-group-addon border-radius black-green-button colors-color"><i class="fa fa-database"></i></div>
                            <select id="collection" class="form-control border-radius" data-placeholder="Choose Circle" style="width: 100% !important">
                                <?php foreach ($collections as $collection) { ?>
                                    <option value="<?php echo $collection->id; ?>"><?php echo $collection->name; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="col-md-12" style="margin-top:10px">
                <div class="form-group">
                    <label class="control-label">Privacy</label>
                    <div class="input-group">
                        <div class="input-group-addon border-radius black-green-button colors-color"><i class="fa fa-share-alt"></i></div>
                        <select id="content_privacy" class="chosen form-control border-radius" multiple="true" data-placeholder="Choose Circle" style="width: 100% !important">
                            <?php foreach ($circles as $circle) { ?>
                                <option value="<?php echo $circle->guid ?>"><?php echo $circle->title ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <button type="button" class="save_group_btn btn btn-success pull-right">Create</button>
                </div>
            </div>
        </div>
    </div>
</div>