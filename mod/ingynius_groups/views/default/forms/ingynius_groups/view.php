<?php
include_once elgg_get_plugins_path() . 'time_theme/functions.php';
$logged_user = elgg_get_logged_in_user_entity();
$guid = $_GET['group_id'];
$entity = get_entity($guid);
$members = getGroupMembers($guid);
if ($entity->title == 'group_content') {
    $group = maybe_unserialize($entity->description);
    $likes_value = 'post_likes_' . $entity->guid;
    $comments_value = 'post_comments_' . $entity->guid;
    $group_dp = 'group_dp_' . $entity->guid;
    $group_cover = 'group_cover_' . $entity->guid;
    $ia = elgg_set_ignore_access(true);
    $post_likes = ($entity->$likes_value) ? maybe_unserialize($entity->$likes_value) : array();
    $post_comments = ($entity->$comments_value) ? maybe_unserialize($entity->$comments_value) : array();
    $group_dp_src = ($entity->$group_dp) ? elgg_get_site_url() . 'mod/time_theme/' . $entity->$group_dp : elgg_get_site_url() . '_graphics/icons/default/large.png';
    $group_cover_src = ($entity->$group_cover) ? elgg_get_site_url() . 'mod/time_theme/' . $entity->$group_cover : elgg_get_site_url() . '_graphics/icons/default/large.png';
    elgg_set_ignore_access($ia);

    // Get Group Posts
    $entities = elgg_get_entities(array(
        'type' => 'object',
        'subtypes' => array('text_content_' . $guid, 'photo_content_' . $guid, 'audio_content_' . $guid, 'link_content_' . $guid, 'video_content_' . $guid, 'event_content_' . $guid),
        'limit' => FALSE,
    ));
    ?>
    <div class="col-md-12 group_main" data-entity="<?php echo $guid; ?>" style="margin-top: 1px;padding: 0px">
        <div class="fb-profile">
            <div class="cover_change">
                <div class="group-cover-change">
                    <i class="fa fa-camera fa-2x" id="group-cover-change"></i>
                    <input type="file" name="groupCover" id="group_profile_cover" class="custom-file-input" original-title="Change Profile Picture">
                </div>
                <img align="left" class="fb-image-lg" src="<?php echo $group_cover_src; ?>" alt="Profile image example">
            </div>
            <span id="page_photo">
                <div class="group-dp-change">
                    <i class="fa fa-camera fa-2x" id="group-dp-change"></i>
                    <input type="file" name="groupDp" id="group_profile_dp" class="custom-file-input" original-title="Change Profile Picture">
                </div>
                <img class="fb-image-profile thumbnail" align="left" src="<?php echo $group_dp_src; ?>">
            </span>
            <div class="fb-profile-text" style="background-color: #fff;">
                <div class="add_to_circles_button">
                    <button type="button" class="btn btn-default" style="font-weight: bold"><?php echo count($members); ?></button>
                </div>
                <h1 style="margin-top: -15px;padding-bottom: 5px">
                    <span class="page_name">
                        <?php echo $group['group_name']; ?>
                        <h5 style="color: darkred">
                            (<?php echo ucfirst($group['group_privacy']); ?>)
                        </h5>
                    </span>
                </h1>
            </div>
        </div>
    </div>
    <?php if (in_array($logged_user->guid, $members) || $entity->owner_guid == $logged_user->guid) { ?>
        <div class="col-md-12" style="background-color: #fff;padding: 5px 0 0 0;border-top: 1px solid #ddd;">
            <div class="col-md-4 post_box">
                <div class="row">
                    <div>
                        <div style="text-align: center;">
                            <div class="col-md-4">
                                <a href="<?php echo elgg_get_site_url() ?>ingynius_groups/members/?group_id=<?php echo $guid; ?>">
                                    <i class="fa fa-users fa-2x colors-color-hover" style="color:#444"></i><br>
                                    <span>Members</span>
                                </a>
                            </div>
                            <div class="col-md-4">
                                <a href="<?php echo elgg_get_site_url() ?>ingynius_groups/">
                                    <i class="fa fa-pagelines fa-2x colors-color-hover" style="color:#3b5998"></i><br>
                                    <span>Groups</span>
                                </a>
                            </div>
                            <div class="col-md-4">
                                <i class="fa fa-pencil fa-2x colors-color-hover" style="color:#00ff00"></i><br>
                                <span><a href="">Edit</a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8 post_box">
                <div class="row">
                    <div>
                        <div style="text-align: center;">
                            <div class="col-md-2" id="save_page_text" data-page_id="<?php echo $guid; ?>">
                                <i class="fa fa-font fa-2x colors-color-hover" style="color:#444"></i><br>
                                <span>Text</span>
                            </div>
                            <div class="col-md-2" id="save_page_photo" data-page_id="<?php echo $guid; ?>">
                                <i class="fa fa-camera fa-2x colors-color-hover" style="color:#d95e40"></i><br>
                                <span>Photo</span>
                            </div>
                            <div class="col-md-2" id="save_page_link" data-page_id="<?php echo $guid; ?>">
                                <i class="fa fa-link fa-2x colors-color-hover" style="color:#56bc8a"></i><br>
                                <span>Link</span>
                            </div>
                            <div class="col-md-2" id="save_page_audio" data-page_id="<?php echo $guid; ?>">
                                <i class="fa fa-volume-up fa-2x colors-color-hover" style="color:#529ecc"></i><br>
                                <span>Audio</span>
                            </div>
                            <div class="col-md-2" id="save_page_video" data-page_id="<?php echo $guid; ?>">
                                <i class="fa fa-film fa-2x colors-color-hover" style="color:#748089"></i><br>
                                <span>Video</span>
                            </div>
                            <div class="col-md-2" id="save_page_event" data-page_id="<?php echo $guid; ?>">
                                <i class="fa fa-calendar fa-2x colors-color-hover" style="color:#f2992e"></i><br>
                                <span>Event</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <?php if ($group['group_privacy'] == 'public') { ?>
        <div class="col-md-12" style="padding: 0px">
            <div id="masonry-container" class="js-masonry" data-masonry-options='{ "columnWidth": ".item", "itemSelector": ".item", "transitionDuration": "1.2s" }' style="padding-right: 0px !important;">
                <?php
                foreach ($entities as $entity) {
                    $subtype = get_subtype_from_id($entity->subtype);
                    $post_owner = get_entity($entity->owner_guid);
                    $user_circles_list = array();
                    $user_circles_value = 'circles_' . $post_owner->guid;
                    if ($post_owner->$user_circles_value) {
                        $user_circles_list = maybe_unserialize($post_owner->$user_circles_value);
                    }
                    $post = maybe_unserialize($entity->description);
                    $collection = get_access_collection($entity->access_id);
                    $post_location = $collection->name;
                    $owner_dp = 'user_dp_' . $post_owner->guid;
                    $likes_value = 'post_likes_' . $entity->guid;
                    $comments_value = 'post_comments_' . $entity->guid;
                    $ia = elgg_set_ignore_access(true);
                    $post_likes = ($entity->$likes_value) ? maybe_unserialize($entity->$likes_value) : array();
                    $post_comments = ($entity->$comments_value) ? maybe_unserialize($entity->$comments_value) : array();
                    $dp_src = ($post_owner->$owner_dp) ? elgg_get_site_url() . 'mod/time_theme/' . $post_owner->$owner_dp : elgg_get_site_url() . '_graphics/icons/user/defaultlarge.gif';
                    elgg_set_ignore_access($ia);
                    if (!user_can_view($user_circles_list, $post['content_privacy'], $post_owner->guid)) {
                        continue;
                    }
                    if ($subtype == 'photo_content_' . $guid) {
                        $line = 'added new photo';
                        include elgg_get_plugins_path() . 'time_theme/views/default/page/subtypes/photo_content.php';
                    } elseif ($subtype == 'text_content_' . $guid) {
                        $line = 'updated status';
                        include elgg_get_plugins_path() . 'time_theme/views/default/page/subtypes/text_content.php';
                    } elseif ($subtype == 'audio_content_' . $guid) {
                        $line = 'shared audio track';
                        include elgg_get_plugins_path() . 'time_theme/views/default/page/subtypes/audio_content.php';
                    } elseif ($subtype == 'video_content_' . $guid) {
                        $line = 'shared video';
                        include elgg_get_plugins_path() . 'time_theme/views/default/page/subtypes/video_content.php';
                    } elseif ($subtype == 'link_content_' . $guid) {
                        $line = 'shared link';
                        include elgg_get_plugins_path() . 'time_theme/views/default/page/subtypes/link_content.php';
                    } elseif ($subtype == 'event_content_' . $guid) {
                        $line = 'created event';
                        include elgg_get_plugins_path() . 'time_theme/views/default/page/subtypes/event_content.php';
                    } else {
                        $line = 'Text';
                    }
                }
                ?>
            </div>
        </div>
    <?php } ?>
    <?php
} else {
    forward(elgg_get_site_url() . 'company_pages/');
}
