<?php

// make sure only logged in users can see this page
gatekeeper();
$logged_user = elgg_get_logged_in_user_entity();
if (isset($_GET['id'])) {
    $collection_id = $_GET['id'];
    $collection = get_access_collection($collection_id);
    if ($logged_user->guid == $collection->owner_guid) {
        delete_access_collection($collection_id);
        system_message("Collection Deleted Successfully!");
        forward('collections');
    } else {
        register_error("Something Went Wrong!");
        forward('collections');
    }
}
// set the title
// for distributed plugins, be sure to use elgg_echo() for internationalization
$title = "Groups";

// start building the main column of the page
//$content = elgg_view_title($title);
// add the form to this section
$content .= elgg_view_form("ingynius_groups/list");

// optionally, add the content for the sidebar
$sidebar = "";

// layout the page
$body = elgg_view_layout('one_sidebar', array(
    'content' => $content,
        ));

// draw the page
echo elgg_view_page($title, $body);
