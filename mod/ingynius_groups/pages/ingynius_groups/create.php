<?php

// make sure only logged in users can see this page
gatekeeper();
$title = "Create Group";
$content .= elgg_view_form("ingynius_groups/create", array('id' => 'group_profile_pic_upload_form'));
$body = elgg_view_layout('one_sidebar', array(
    'content' => $content,
        ));
echo elgg_view_page($title, $body);
