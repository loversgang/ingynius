<?php

elgg_register_event_handler('init', 'system', 'ingynius_registration_init');

function ingynius_registration_init() {
    $action_path = elgg_get_plugins_path() . "ingynius_registration/actions/register.php";
    elgg_register_action("register", $action_path, 'public');
    elgg_register_page_handler('register', 'cutomregister_page_handler');
}

function cutomregister_page_handler() {
    $content = elgg_view_form('register');
    $params = array(
        'title' => '',
        'filter' => '',
        'content' => $content
    );

    $body = elgg_view_layout('one_column', $params);

    echo elgg_view_page('', $body);
    return TRUE;
}
