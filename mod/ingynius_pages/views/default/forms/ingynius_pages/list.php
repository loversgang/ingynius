<?php
include_once elgg_get_plugins_path() . 'time_theme/functions.php';
$logged_user = elgg_get_logged_in_user_entity();
$pages = elgg_get_entities(array(
    'type' => 'object',
    'subtype' => 'page_content',
    'owner_guid' => $logged_user->guid,
    'limit' => FALSE,
        ));
?>
<div class="row-fluid">
    <div class="col-md-12" style="background-color: #fff; margin-top: 10px">
        <h2 class="page-header" style="border:none;margin: 10px 0 20px 0">
            <span class="pull-right" style="margin-right: 15px;">
                <a href="<?php echo elgg_get_site_url(); ?>company_pages/create/" class="btn btn-success"><i class="fa fa-plus"></i> Create Page</a>
            </span>
            Pages
        </h2>
    </div>
    <div class="col-md-12">
        <div id="masonry-container" class="js-masonry" data-masonry-options='{ "columnWidth": ".item", "itemSelector": ".item", "transitionDuration": "1.2s" }' style="padding-right: 0px !important;">
            <?php
            foreach ($pages as $entity) {
                $post_owner = get_entity($entity->owner_guid);
                $post = maybe_unserialize($entity->description);
                $collection = get_access_collection($entity->access_id);
                $post_location = $collection->name;
                $owner_dp = 'user_dp_' . $post_owner->guid;
                $likes_value = 'post_likes_' . $entity->guid;
                $comments_value = 'post_comments_' . $entity->guid;
                $ia = elgg_set_ignore_access(true);
                $post_likes = ($entity->$likes_value) ? maybe_unserialize($entity->$likes_value) : array();
                $post_comments = ($entity->$comments_value) ? maybe_unserialize($entity->$comments_value) : array();
                $dp_src = ($post_owner->$owner_dp) ? elgg_get_site_url() . 'mod/time_theme/' . $post_owner->$owner_dp : elgg_get_site_url() . '_graphics/icons/user/defaultlarge.gif';
                elgg_set_ignore_access($ia);
                $line = ' created ';
                ?>
                <div class="item" data-type="media" id="post_<?php echo $entity->guid; ?>" data-entity="<?php echo $entity->guid; ?>">
                    <div class="row item_content">
                        <div class="col-md-2" style="width:65px;padding: 2px">
                            <img src="<?php echo $dp_src; ?>" class="img img-circle" style="height:35px"/>
                        </div>
                        <div class="col-md-8" style="margin-left: -20px;padding: 2px">
                            <i>
                                <b>
                                    <a href="<?php echo elgg_get_site_url() ?>profile/<?php echo $post_owner->username; ?>">
                                        <?php echo $post_owner->name; ?>
                                    </a>
                                </b>
                                <font style="color:#999">
                                <?php echo $line ?> <a href="<?php echo elgg_get_site_url() ?>company_pages/view/?page_id=<?php echo $entity->guid; ?>"><b><?php echo $post['page_name']; ?></b></a>
                                <br/>
                                <?php echo time_stamp($entity->time_created); ?>
                                </font>
                            </i>
                        </div>
                        <div class="col-md-12" style="margin-top:10px;padding: 0px;">
                            <div class="img-list">
                                <img class="img post_photo_img post_video_size" src="<?php echo elgg_get_site_url() ?>mod/time_theme/files/<?php echo $post['img_url']; ?>" />
                            </div>
                            <div class="col-md-12">
                                <div class="post_content">
                                    <?php echo substr($post['page_info'], 0, 200); ?>..
                                    <br/>
                                    <br/>
                                    <span class="location">
                                        <?php //echo $post['text_location'] ? $post['text_location'] . ' . ' : ''; ?><?php //echo $post_location; ?>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 options_row">
                            <div class="col-md-3">
                                <div class="pull-right">15</div>
                                <div class="pull-left">
                                    <i class="fa fa-share-alt"></i>
                                </div>
                            </div>
                            <div class="col-md-3 like_post">
                                <div class="pull-right post_like_count"><?php echo count($post_likes); ?></div>
                                <div class="pull-left">
                                    <i class="fa <?php echo in_array($logged_user->guid, $post_likes) ? 'fa-thumbs-up' : 'fa-thumbs-o-up'; ?>" data-owner_guid="<?php echo $entity->owner_guid; ?>" data-guid="<?php echo $entity->guid; ?>" data-post_like_count="<?php echo count($post_likes); ?>"></i>
                                </div>
                            </div>
                            <div class="col-md-3 comment_post" data-guid="<?php echo $entity->guid; ?>">
                                <div class="pull-right"><?php echo count($post_comments); ?></div>
                                <div class="pull-left">
                                    <i class="fa fa-comment"></i>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <i class="fa fa-pencil"></i>
                            </div>
                        </div>
                        <div class="col-md-12 comments_div">
                            <div class="input-group" style="margin-bottom: 5px">
                                <input type="text" class="form-control input-sm comment_post_data"/>
                                <div data-guid="<?php echo $entity->guid; ?>" style="cursor: pointer" class="submit_post_comment input-group-addon black-green-button colors-color"><i class="fa fa-check-circle-o"></i></div>
                            </div>
                            <div class="comments_data" data-guid="<?php echo $entity->guid; ?>"></div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>