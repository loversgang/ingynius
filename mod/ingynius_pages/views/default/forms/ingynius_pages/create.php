<?php
$logged_user = elgg_get_logged_in_user_entity();
$collections = get_user_access_collections($logged_user->guid);
$circles = elgg_get_entities(array(
    'type' => 'object',
    'subtype' => 'circle',
    'limit' => FALSE,
        ));
?>
<div class="row-fluid create_page_main"> 
    <div class="col-md-12" style="min-height: 800px;background-color: #fff; padding-top: 10px">
        <h2 class="page-header" style="border:none;margin: 10px 0 20px 0">
            <span class="pull-right" style="margin-right: 15px;">
                <a href="<?php echo elgg_get_site_url(); ?>company_pages/" class="btn btn-success"><i class="fa fa-list"></i> My Pages</a>
            </span>
            Create Page
        </h2>
        <div class="form">
            <label class="control-label">Page Name</label>
            <div class="form-group">
                <input class="form-control page_name_input" placeholder="Page Name"/>
            </div>
        </div>
        <div role="tabpanel">
            <ul class="nav nav-tabs" role="tablist" style="background: #eee;">
                <li role="presentation" class="active">
                    <a href="#about" aria-controls="about" role="tab" data-toggle="tab">
                        <i class="fa fa-info-circle"></i> About
                    </a>
                </li>
                <li role="presentation" class="">
                    <a href="#picture" aria-controls="picture" role="tab" data-toggle="tab">
                        <i class="fa fa-photo"></i> Profile Picture
                    </a>
                </li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content" style="padding: 10px 5px;">
                <div role="tabpanel" class="tab-pane fade active in" id="about">
                    <p>
                        <b>Tip:</b> Add a description and website to improve the ranking of your Page in search.<br/><br/>
                        Add a few sentences to tell people what your Page is about. This will help it show up in the right search results. You will be able to add more details later from your Page settings.<br/>
                        <br/>
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon border-radius black-green-button colors-color"><i class="fa fa-info"></i></div>
                                <textarea rows="3" class="form-control page_description_input" placeholder="*Tell People What Your Page Is About.."></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon border-radius black-green-button colors-color"><i class="fa fa-globe"></i></div>
                                <input class="form-control page_website_input" placeholder="Website (ex: your website, Twitter or other social media links)" value="http://"/>
                            </div>
                        </div>
                    </div>
                    <?php if ($collections) { ?>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label name="select_collection">Add To Collection</label>
                                <div class="input-group">
                                    <div class="input-group-addon border-radius black-green-button colors-color"><i class="fa fa-database"></i></div>
                                    <select id="collection" class="form-control border-radius" data-placeholder="Choose Circle" style="width: 100% !important">
                                        <?php foreach ($collections as $collection) { ?>
                                            <option value="<?php echo $collection->id; ?>"><?php echo $collection->name; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="col-md-12" style="margin-top:10px">
                        <div class="form-group">
                            <label class="control-label">Privacy</label>
                            <div class="input-group">
                                <div class="input-group-addon border-radius black-green-button colors-color"><i class="fa fa-share-alt"></i></div>
                                <select id="content_privacy" class="chosen form-control border-radius" multiple="true" data-placeholder="Choose Circle" style="width: 100% !important">
                                    <?php foreach ($circles as $circle) { ?>
                                        <option value="<?php echo $circle->guid ?>"><?php echo $circle->title ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <button type="button" class="btn btn-success pull-right profile_picture_tab">Next</button>
                    </p>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="picture">
                    <p>
                    <div class="col-md-12" style="padding: 0px" id="page_profile_picture_div">
                        <div class="col-md-4 preview_div">
                            <div id="timelineContainer">
                                <center>
                                    <div id="timelineProfilePic">
                                        <div id="show_upload_icon" style="padding:5px">
                                            <center>
                                                <div class="uploadFile timelineUploadBG">
                                                    <input type="file" name="userImage" id="page_profile_pic" class="custom-file-input" original-title="Change Cover Picture">
                                                </div>
                                                <div id="result_pic">
                                                    <img src="<?php echo elgg_get_site_url(); ?>images/noimage.jpg" style="width:100%;height: 156px">
                                                </div>
                                            </center>
                                        </div>
                                    </div>
                                </center>
                            </div>
                            <hr/>
                            <div id="preview-pane">
                                <div class="preview-container"></div>
                            </div>
                        </div>
                        <div class="col-md-8 image_preview_width">
                            <div class="profile_picture_thumbnail_crop"></div>
                        </div>
                    </div>
                    <div class="col-md-12" id="coordinates">
                        <input type="hidden" id="x-chord" name="x" />
                        <input type="hidden" id="y-chord" name="y" />
                        <input type="hidden" id="w-chord" name="w" />
                        <input type="hidden" id="h-chord" name="h" />
                        <button type="button" class="btn btn-success pull-right profile_picture_save">Save</button>
                    </div>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('#show_uupload_icon').hover(function () {
        $('.uploadFile').show();
    }, function () {
        $('.uploadFile').hide();
    });
</script>