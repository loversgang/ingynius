<?php

elgg_register_event_handler('init', 'system', 'ingynius_pages_init');

function ingynius_pages_init() {
    elgg_register_plugin_hook_handler('entity:url', 'object', 'pages_set_url');
    elgg_register_page_handler('company_pages', 'ingynius_pages_page_handler');
}

function ingynius_pages_page_handler($segments) {
    if (!$segments[0]) {
        include elgg_get_plugins_path() . 'ingynius_pages/pages/ingynius_pages/list.php';
        return true;
    }
    if ($segments[0] == 'list') {
        include elgg_get_plugins_path() . 'ingynius_pages/pages/ingynius_pages/list.php';
        return true;
    }
    if ($segments[0] == 'create') {
        include elgg_get_plugins_path() . 'ingynius_pages/pages/ingynius_pages/create.php';
        return true;
    }
    if ($segments[0] == 'view') {
        include elgg_get_plugins_path() . 'ingynius_pages/pages/ingynius_pages/view.php';
        return true;
    }
    return false;
}

elgg_register_action("ingynius_pages/save", elgg_get_plugins_path() . "ingynius_pages/actions/ingynius_pages/save.php");
elgg_register_action("ingynius_pages/save_upload", elgg_get_plugins_path() . "ingynius_pages/actions/ingynius_pages/save_upload.php");
