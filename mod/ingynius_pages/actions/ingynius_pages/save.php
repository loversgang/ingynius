<?php

// Get Logged In User Details
$logged_user = elgg_get_logged_in_user_entity();

// Create MetaData
$profile->mobile = get_input('mobile');
$profile->birth_date = get_input('birth_date');
$profile->birth_month = get_input('birth_month');
$profile->birth_year = get_input('birth_year');
$profile->gender = get_input('gender');

// for now make all tags public
$profile->access_id = ACCESS_PUBLIC;

// owner is logged in user
$profile->owner_guid = elgg_get_logged_in_user_guid();
$data = serialize((array) $profile);
create_metadata($logged_user->guid, 'data_'.$logged_user->guid, $data);
// save to database and get id of the new my_blog
$profile_guid = $logged_user->save();
pr($profile_guid);
exit;

// if the my_blog was saved, we want to display the new post
// otherwise, we want to register an error and forward back to the form
if ($profile_guid) {
    system_message("Your blog post was saved");
    forward($profile_guid->getURL());
} else {
    register_error("The blog post could not be saved");
    forward(REFERER); // REFERER is a global variable that defines the previous page
}