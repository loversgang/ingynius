<div class="item" data-type="text" id="post_<?php echo $entity->guid; ?>">
    <div class="row item_content">
        <div class="col-md-2" style="width:65px">
            <img src="<?php echo $dp_src; ?>" class="img img-circle" style="height:35px"/>
        </div>
        <div class="col-md-8" style="margin-left: -20px;">
            <i>
                <b>
                    <a href="<?php echo elgg_get_site_url() ?>profile/<?php echo $post_owner->username; ?>">
                        <?php echo $post_owner->name; ?>
                    </a>
                </b>
                <font style="color:#999">
                <?php echo $line ?>
                <br/>
                <?php echo time_stamp($entity->time_created); ?>
                </font>
            </i>
            <?php if ($entity->owner_guid == $logged_user->guid) { ?>
                <div class="actions_icon">
                    <i data-guid="<?php echo $entity->guid; ?>" data-type="event" class="fa fa-pencil edit_entity"></i>
                    <i data-guid="<?php echo $entity->guid; ?>" class="fa fa-trash delete_entity"></i>
                </div>
            <?php } ?>
        </div>
        <div class="col-md-12">
            <div class="post_content">
                <b>Event:</b> <?php echo $post['title']; ?><br/>
                <b>Event Date:</b> <?php echo $post['date']; ?><br/>
                <b>Location:</b> <span style="color: #999"><?php echo $post['location']; ?></span><br/><br/>
                <?php echo $post['text_content']; ?>
                <br/>
                <br/>
                <span class="location">
                    <?php echo $post['text_location'] ? $post['text_location'] . ' . ' : ''; ?><?php echo $post_location; ?>
                </span>
            </div>
        </div>
        <div class="col-md-12 options_row">
            <div class="col-md-3">
                <div class="pull-right">15</div>
                <div class="pull-left">
                    <i class="fa fa-share-alt"></i>
                </div>
            </div>
            <div class="col-md-3 like_post">
                <div class="pull-right post_like_count"><?php echo count($post_likes); ?></div>
                <div class="pull-left">
                    <i class="fa <?php echo in_array($logged_user->guid, $post_likes) ? 'fa-thumbs-up' : 'fa-thumbs-o-up'; ?>" data-owner_guid="<?php echo $entity->owner_guid; ?>" data-guid="<?php echo $entity->guid; ?>" data-post_like_count="<?php echo count($post_likes); ?>"></i>
                </div>
            </div>
            <div class="col-md-3 comment_post" data-guid="<?php echo $entity->guid; ?>">
                <div class="pull-right"><?php echo count($post_comments); ?></div>
                <div class="pull-left">
                    <i class="fa fa-comment"></i>
                </div>
            </div>
            <div class="col-md-3">
                <i class="fa fa-pencil"></i>
            </div>
        </div>
        <div class="col-md-12 comments_div">
            <div class="input-group" style="margin-bottom: 5px">
                <input type="text" class="form-control input-sm comment_post_data"/>
                <div data-guid="<?php echo $entity->guid; ?>" style="cursor: pointer" class="submit_post_comment input-group-addon black-green-button colors-color"><i class="fa fa-check-circle-o"></i></div>
            </div>
            <div class="comments_data" data-guid="<?php echo $entity->guid; ?>"></div>
        </div>
    </div>
</div>
<script>
    (function ($) {
        $(document).ready(function () {
            $('.comment_post').click();
        });
    })(jQuery);
</script>