<?php

include_once elgg_get_plugins_path() . 'time_theme/functions.php';
$logged_user = elgg_get_logged_in_user_entity();
$guid = get_input('guid');
if (!$guid) {
    forward(elgg_get_site_url());
}
$entity = get_entity($guid);
$subtype = get_subtype_from_id($entity->subtype);
$post_owner = get_entity($entity->owner_guid);
$user_circles_list = array();
$user_circles_value = 'circles_' . $post_owner->guid;
if ($post_owner->$user_circles_value) {
    $user_circles_list = maybe_unserialize($post_owner->$user_circles_value);
}
$post = maybe_unserialize($entity->description);
$collection = get_access_collection($entity->access_id);
$post_location = $collection->name;
$owner_dp = 'user_dp_' . $post_owner->guid;
$likes_value = 'post_likes_' . $entity->guid;
$comments_value = 'post_comments_' . $entity->guid;
$ia = elgg_set_ignore_access(true);
$post_likes = ($entity->$likes_value) ? maybe_unserialize($entity->$likes_value) : array();
$post_comments = ($entity->$comments_value) ? maybe_unserialize($entity->$comments_value) : array();
$dp_src = ($post_owner->$owner_dp) ? elgg_get_site_url() . 'mod/time_theme/' . $post_owner->$owner_dp : elgg_get_site_url() . '_graphics/icons/user/defaultlarge.gif';
elgg_set_ignore_access($ia);
if (user_can_view($user_circles_list, $post['content_privacy'], $post_owner->guid)) {
    if ($subtype == 'photo_content') {
        $line = 'added new photo';
        include 'subtypes/photo_content.php';
    } elseif ($subtype == 'text_content') {
        $line = 'updated status';
        include 'subtypes/text_content.php';
    } elseif ($subtype == 'audio_content') {
        $line = 'shared audio track';
        include 'subtypes/audio_content.php';
    } elseif ($subtype == 'video_content') {
        $line = 'shared video';
        include 'subtypes/video_content.php';
    } elseif ($subtype == 'link_content') {
        $line = 'shared link';
        include 'subtypes/link_content.php';
    } elseif ($subtype == 'event_content') {
        $line = 'created event';
        include 'subtypes/event_content.php';
    } else {
        $line = 'Text';
    }
}
?>