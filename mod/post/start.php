<?php

elgg_register_event_handler('init', 'system', 'posts_init');

function posts_init() {
    elgg_register_page_handler('post', 'posts_page_handler');
}

function posts_page_handler($segments) {
    if ($segments[0]) {
        set_input('guid', $segments[0]);
        include elgg_get_plugins_path() . 'post/pages/post/post.php';
        return true;
    }
    return false;
}
