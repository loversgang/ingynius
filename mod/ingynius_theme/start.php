<?php

function mytheme_init() {
    elgg_extend_view('elgg.css', 'ingynius_theme/css');
}

elgg_register_event_handler('init', 'system', 'mytheme_init');
